<!--Iniciar o Destruir session-->
<?php          
    
    session_start();   
    
    if(count($_SESSION)==0){
        session_destroy();                    
    //Si la session está iniciada, e intentas ir a la pagina principal, te redirige al indexUsuario
    }else if(count($_SESSION)>0){     
        //Si el usuario es administrador:                         
        if($_SESSION["admin"]=="1"){//Redirigir al index de administradores:
            //header("location:/vistas/indexAdmin.php");                
            header("location:/administracion");  
        }else{//Si no, redirigir al index del usuario
            //header("location:/vistas/indexUsuario.php");                
            header("location:/principal");  
        }        
    }
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <!--<meta name="viewpoint" content="width=device-width, initial-scale=1, syrink-to-fit=no">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FilmRate</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!--FontAwesomCSS-->
    <script src="https://kit.fontawesome.com/bd632f581b.js" crossorigin="anonymous"></script>
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sh....-->
    <!--SWEETALERT2-->
    <link rel="stylesheet" href="plugins/sweetalert2/sweetalert2.min.css">
 
    <!--Estilos-->
    <link rel="stylesheet" type="text/css" href="css/estilos.css?v=1.1"/><!--PRUEBAS, lo dejare asi por un tiempo, luego lo cambio-->
    <!--<link rel="stylesheet" type="text/css" href="css/estilos.css"/>-->
    <!--<meta http-equiv="cache-control" content="no-cache"/>ESTO NO HA FUNCIONADO-->
    
</head>
<body>
    <!--Llamo a las librerias---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="jquery/jquery.js"></script>
    <!--Popper-->   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <!--BOOTSTRAP-->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!--CDN de Bootstrap-->
    <!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    -->
    <!-- CDN de Vue -->
    <!--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>-->   
    <!-- <script src="plugins/vue.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

    <!-- CDN de Axios -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
    
    <!--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>-->      
    <!--SWEETALERT2-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>-->
    <script src="plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
     
    
    <!--LLAMADA AL CONTROLADOR (TIENE QUE IR DESPUES DE LAS LIBRERIAS)-->
    <script src="controlador/controladorIndex.js"></script>




    
    <?php 
        
        require "vistas/cabeceras/cabeceraPublica.php";
        
    ?>


     

        <div id="peliculaNoEncontrada" class="container text-dark shadow bg-light text-center"></div>

        <!--Div que mostrará información básica sobre una película-->

        <div id="appPelicula" class="container text-dark shadow bg-light text-left">           
            
            <h1 class="tituloPeliculaVue">{{tituloPelicula}}</h1>
            <img class="peliculaAMostrar"/><div class="contenedorNotasVotos"><div class="notaPeliculaAMostrarVue">{{notaPelicula}}</div><br><div class="numeroVotosAMostrarVue">{{numeroVotosPelicula}}<br>votos</div></div>
            <br>
            <button type="button" class="btn btn-lg colorAzul votarPeliculaPublica">Votar Película</button>
            <br>
            <br>
            <i><h5><strong class="tipoPeliculaVue">{{tipoPelicula}}</strong></h5></i>
            
            <p id='contenedorPais'><strong>Pais: </strong><span id="idfotopais"></span></p>
            <p><strong>Director/es: </strong> <span class="directorPeliculaVue">&nbsp;{{directorPelicula}}</span> </p>
            <p><strong>Año: </strong> <span class="fechaPeliculaVue">&nbsp;{{fechaPelicula}}</span> </p>
            <p><strong>Duración: </strong> <span class="duracionPeliculaVue">&nbsp;{{duracionPelicula}}</span> min </p>
            <p><strong>Guión: </strong> <span class="guionPeliculaVue">&nbsp;{{guionPelicula}}</span></p>
            <p><strong>Género: </strong> <span class="generoPeliculaVue">&nbsp;{{generoPelicula}}</span></p>
            <p><strong>Reparto: </strong> <span class="repartoPeliculaVue">&nbsp;{{repartoPelicula}}</span></p>
            <p><strong>Sinopsis: </strong> <span class="sinopsisPeliculaVue">&nbsp;{{sinopsisPelicula}}</span></p>
            
            <button type="button" class="btn btn-lg colorAzul botonVolverContenidoPublico"><i class="fas fa-arrow-left"></i></button>           
            
            <button type="button" class="btn btn-lg colorAzul criticaPeliculaPublica">Escribe Una Crítica</button>
            <button type="button" class="btn btn-lg colorAzul subir"><i class="fas fa-arrow-up"></i></button>           
        </div>



        

        <!--Div que mostrará 6 películas aleatorias-->
        <div id="contenidoPublico" class="container text-dark shadow bg-light text-center">            
            <!--<p class="h4">Algunas de nuestras películas</p>-->
                <div id="titulosPeliculas"></div>
                <!-- <button id="botonMostrarMasPublico" type="button" class="btn btn-lg colorAzul mostrarMasPeliculas"><i class="fas fa-plus"></i>&nbsp;&nbsp;Mostrar más</button> -->
                <button type="button" class="btn btn-lg colorAzul subir"><i class="fas fa-arrow-up"></i></button>           
        </div> 


       




        <!--Div del formulario de login-->
        <div id="divFormulariologin" class="container  text-dark  shadow bg-light">
        <div class="row">
                    <div class="col-md-12">
                        <!--<div class="well well-sm">-->
                      
                            <form class="form-horizontal text-center">
                            <img class="mb-4" src="favicon.ico"/>
                                <fieldset>
                                    
                                    <legend class="text-center header">Acceder</legend>
                                    <br>

                                    <!--Cuenta o email-->
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-7 mx-auto">
                                            <input id="cuentaOEmailLogin"  type="text" placeholder="Introduce tu nick o email" class="form-control">

                                            <!--<div class="text-danger"> Cuenta no existente </div> d-none-->

                                        </div>
                                    </div>
                                    <hr>
                                    <!--Contraseña-->
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fas fa-key"></i></span>
                                        <div class="col-md-7 mx-auto">
                                            <input id="passwordLogin"  type="password" placeholder="Introduce tu contraseña" class="form-control"><br><i id="ojo1" title="ocultar/mostrar contraseña" class="far fa-eye ojopasswd1"></i>
                                        </div>
                                    </div>
                                    <p class="textoSinCuenta"> ¿Aún no tienes cuenta? Regístrate </p>
                                    <!--<hr>-->
                                    <br>
                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <button id="botonLogin" type="button"  class="btn btn-lg">Continuar</button>
                                        </div>
                                    </div>                
                                </fieldset>
                            </form>
                        <!--</div>-->
                    </div>
            </div>
        </div>
        <!--Div del formulario de registro-->
        <div id="divFormularioregistrarse" class="container text-dark  shadow bg-light">
                <div class="row">
                    <div class="col-md-12">
                        <!--<div class="well well-sm">-->
                            <form class="form-horizontal text-center">
                                <img class="mb-4" src="favicon.ico"/>    
                                <fieldset>
                                    <legend class="text-center header">Bienvenido a FilmRate, crea una nueva cuenta.</legend>
                                    <br>

                                    <!--Nombre-->
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-7 mx-auto">
                                            <input id="nombreNuevoUsuario"  type="text" placeholder="Introduce tu nombre" class="form-control">
                                        </div>
                                    </div>
                                
                                    <!--Apellidos-->
                                    <div class="form-group">                            
                                        <div class="col-md-7 mx-auto">
                                            <input id="apellidoNuevoUsuario" type="text" placeholder="Introduce tus apellidos" class="form-control">
                                        </div>
                                    </div>
                                    <small class="text-muted"> Tu nombre y apellidos reales. </small>
                                    <hr>
                                    <!--Nick-->
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fas fa-user-circle"></i></span>
                                        <div class="col-md-7 mx-auto">
                                            <input id="nickNuevoUsuario"  type="text" placeholder="Introduce un nick" class="form-control">
                                        </div>                                        
                                    </div>
                                    <small class="text-muted"> Será el nombre que tendrá tu cuenta. </small>
                                    <hr>
                                    <!--Email-->
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="far fa-envelope"></i></span>
                                        <div class="col-md-7 mx-auto">
                                            <input id="emailNuevoUsuario"  type="email" placeholder="Introduce un correo" class="form-control">
                                        </div>                                        
                                    </div>
                                    <small class="text-muted"> Tu email. </small>
                                    <hr>
                                    <!--Contraseña-->
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fas fa-key"></i></span>                                        
                                        <div class="col-md-7 mx-auto">
                                            <input id="passwordNuevoUsuario1"  type="password" placeholder="Introduce una contraseña" class="form-control">
                                        </div>                                        
                                    </div>
                                    <div class="form-group">                                        
                                        <div class="col-md-7 mx-auto">
                                            <input id="passwordNuevoUsuario2"  type="password" placeholder="Vuelve a introducir la contraseña" class="form-control"><br><i id="ojo2" title="ocultar/mostrar contraseña" class="far fa-eye ojopasswd2"></i>
                                        </div>                                        
                                    </div>
                                    <small class="text-muted"> Debe tener 8 caracteres. </small>
                                    <br>
                                    <!--<hr>-->
                                    <br>
                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <button id="botonRegistrarse" type="button" class="btn btn-lg">Continuar</button>
                                        </div>
                                    </div>         

                                </fieldset>
                            </form>
                        <!--</div>-->
                    </div>
            </div>
        </div>        
  
        
        <?php
            require "vistas/footer/footer.php";
        ?> 
   
</body>

</html>