//Este es el controlador del fichero index para los usuarios que se han logeado.
$(document).ready(function(){

	//console.log(sessionStorage);	

	//Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);



    //Peticion para obtener todas las peliculas favoritas del usuario ordenadas por titulo:
    $.post("../modelo/modelo.php",{obtenerFavoritasUsuario:"true",idUsuarioFavoritas:sessionStorage.Id},function(respuestaFavoritasUsuario){

            //console.log(respuestaFavoritasUsuario);
            //Obtengo un array de ID que son los ID de las peliculas favoritas del usuario actual
            var arrayFavoritasUsuario = JSON.parse(respuestaFavoritasUsuario);            
            //console.log(arrayFavoritasUsuario);

            if(arrayFavoritasUsuario.length>0){

            //Por cada ID de pelicula favorita tengo que hacer una consulta para obtener la informacion completa de la pelicula
            for(var i = 0;i<arrayFavoritasUsuario.length;i++){
                //console.log(arrayFavoritasUsuario[i]);
                cadaIdFavoritas = arrayFavoritasUsuario[i][0];
                //console.log(cadaIdFavoritas);

                //Consulta para obtener informacion de cada pelicula favorita del usuario
                $.post("../modelo/modelo.php",{obtenerInfoFavorita:"true",idFavoritaObtenerInfo:cadaIdFavoritas},function(respuestaInformacionFavorita){

                    var arrayInfoCadaPeliculaFavorita = JSON.parse(respuestaInformacionFavorita);
                    var tituloFavorita =  arrayInfoCadaPeliculaFavorita[0][0];
                    var fotoFavorita =  arrayInfoCadaPeliculaFavorita[0][1];
                    var idFavorita = arrayInfoCadaPeliculaFavorita[0][2];

                    //console.log(tituloFavorita);
                    //console.log(fotoFavorita);

                    //Añado las peliculas al div
                    $("#listadoCompletoFavoritas").append(`
                    <div class='contenedorCadaFavorita'>
                        <div class='contenedorFilmFavoritas idFilmFavorita`+idFavorita+`'>
                            <img class='fotoFavorita' src='`+fotoFavorita+`'/>
                            <span class='tituloFavorita'><i>`+tituloFavorita+`</i></span>                        
                        </div>
                        <div class='divEstrella'>
                            <i class='fas fa-star estrellafavorita estrellaid`+idFavorita+`'></i>
                        </div>
                    </div>`);//Fin añadir peliculas

                    //Cuando hago click en el div de una pelicula
                    $(".contenedorFilmFavoritas").on("click",function(){
                            //Debo obtener su id
                            var idFilmPeliculaFavoritaPulsada = $(this).attr("class").split(" ")[1].split("idFilmFavorita")[1];
                            //console.log(idFilmPeliculaFavoritaPulsada);
                            window.location.href="/film/"+idFilmPeliculaFavoritaPulsada;    
                    });//Fin cuando hago click en el div de una pelicula

                    //Cuando pulso en la estrella, se debe eliminar la pelicula de la lista de favoritas y ocultar el div con la pelicula
                    $(".estrellafavorita").on("click",function(){


                            //Debo eliminar la pelicula de la lista de favoritas
                            var tituloPeliculaEliminar =this.parentNode.previousElementSibling.children[1].textContent;

                            Swal.fire({
                                title:"Deseas eliminar "+tituloPeliculaEliminar+" de tus favoritas?",
                                showCancelButton:true,
                                confirmButtonColor:'#3085d6',
                                cancelButtonColor:'#d33',
                                confirmButtonText: 'Si',
                                cancelButtonText: 'No',
                                
                            }).then((result)=>{			
                                if(result.value){	                                    			
                                    Swal.fire({
                                        title:tituloPeliculaEliminar+' eliminada de favoritas',
                                        icon:'success',
                                        showConfirmButton:false,
                                        timer:1200,
                                    });

                                    //Una vez se pulsa el boton de eliminar, tengo que hacer la consulta para borrarla de favoritas

                                    //Oculto el div completo si pulso en si
                                    $(this).hide();
                                    $(this.parentNode.parentNode).hide();                        
                                    


                                    //Obtengo el id de esa pelicula
                                    var idFilmPeliculaFavoritaEstrellaPulsada = $(this).attr("class").split(" ")[3].split("estrellaid")[1];
                                    //console.log(idFilmPeliculaFavoritaEstrellaPulsada);


                                    //Hago la consulta para eliminar la pelicula de las favoritas del usuario
                                    $.post("../modelo/modelo.php",{eliminarFavoritaListaFavoritas:"true",idUsuarioFavoritasEliminar:sessionStorage.Id,idPeliculaEliminarFavorita:idFilmPeliculaFavoritaEstrellaPulsada},function(respuestaFavoritaEliminada){
                                        //console.log(respuestaFavoritaEliminada);
                                    })//Fin consulta eliminar favorita
                                }
                            })		

                            
                        
                        
                        
                    });//Fin cuando pulso en la estrella
    


                });//Fin consulta para obtener informacion de cada pelicula favorita del usuario              

            }//Fin obtener informacion de cada pelicula favorita   
            
            }else{

                 //Añado las peliculas al div
                 $("#listadoCompletoFavoritas").append(`<div>No tiene ningun film añadido a favoritos.</div>`);

            }
            
    })//Fin peticion para obtener todas las peliculas favoritas del usuario

});