$(document).ready(function(){
    //console.log(sessionStorage);

       //Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);

    //Oculto el mensaje de usuario no encontrado:
    $("#infoNoExistente").hide();
    

    //Obtener de la url el id de la pelicula actual
    var nickBuscarInfo = obtenerIdURL();
    //console.log(idBuscarInfo);
    //console.log(nickBuscarInfo);
    
    //Si el nick no esta definido en la url
   /* if(nickBuscarInfo==undefined){
        //La informacion del usuario a buscar será la del usuario que estrá iniciado
        nickBuscarInfo=nickBuscarInfo;        
    }
*/
    //Si se intenta poner admin o administrador en la url:
    if(nickBuscarInfo=="admin"||nickBuscarInfo=="administrador"){
        window.location.replace("/perfil/"+nickBuscarInfo);
    }//Fin intentar poner administrador en la url


            


    //nickBuscarInfo = nickBuscarInfo.charAt(0).toUpperCase()+nickBuscarInfo.slice(1);
    //console.log("INFORMACION MIA ->"+nickBuscarInfo);


    //Comprobar si existe en la base de datos:
    $.post("../modelo/modelo.php",{existeEsteNick:nickBuscarInfo},function(respuestaExisteNick){
        //Si el usuario existe en la base de datos:
        if(respuestaExisteNick!="1"){   
            $("#infoNoExistente").show();
            $("#informacionUsuario").hide();
            $(".tablaInfoFilmRateUsuario").hide();
            $(".explicacionMinutosTotales").hide();
        }
    });//Fin comprobar si existe el usuario en la base de datos


    //Inserto en el titulo el nick y el nombre debajo:    
    $("#nickperfil").html("Perfil de "+nickBuscarInfo);
        
        //Tengo que hacer consultas para saber todos los datos que tendrá la tabla
        /*<td id='tablaMedia'>1</td>
        <td id='tablaVotos'>2</td>
        <td id='tablaCriticas'>3</td>
        <td id='tablaFavoritas'>4</td>*/
        

            //Peticion al post para saber la media de votos del usuario:
            $.post("../modelo/modelo.php",{mediaUsuarioInfoPerfil:nickBuscarInfo},function(respuestaInfoPerfilMedia){
                var notaMediaNick = parseFloat(respuestaInfoPerfilMedia).toFixed(2);     
                //Si la media es NaN la igualo a cero:
                if(isNaN(notaMediaNick)){
                    notaMediaNick=0;
                }    

                //Inserto en la tabla el resultado
                $("#tablaMedia").html("<i class='fa fa-bar-chart'></i><br>"+notaMediaNick);
            });//Fin peticion al post para saber la media de votos del usuario


            //Peticion al post para saber la cantidad de votos que ha hecho el usuario:
            $.post("../modelo/modelo.php",{votosUsuarioInfoPerfil:nickBuscarInfo},function(respuestaInfoPerfilVotos){
                var votosNick = respuestaInfoPerfilVotos;
                //Inserto en la tabla el resultado
                $("#tablaVotos").html("<i class='fa fa-film'></i><br>"+votosNick);
            });//Fin peticion al post para saber la cantidad de votos que ha hecho el usuario


            //Peticion al post para saber la cantidad de criticas que ha hecho el usuario:
            $.post("../modelo/modelo.php",{criticasUsuarioInfoPerfil:nickBuscarInfo},function(respuestaInfoPerfilCriticas){
                var criticasNick = respuestaInfoPerfilCriticas;
                //Inserto en la tabla el resultado
                $("#tablaCriticas").html("<i class='fa fa-pencil'></i><br>"+criticasNick);
            });//Fin peticion al post para saber la cantidad de votos que ha hecho el usuario

            
            //Peticion al post para saber la cantidad de peliculas favoritas que tiene el usuario:
            $.post("../modelo/modelo.php",{favoritasUsuarioInfoPerfil:nickBuscarInfo},function(respuestaInfoPerfilFavoritas){
                var favoritasNick = respuestaInfoPerfilFavoritas;
                //Inserto en la tabla el resultado
                $("#tablaFavoritas").html("<i class='fa fa-star'></i><br> "+favoritasNick);
            });//Fin peticion al post para saber la cantidad de peliculas favoritas que tiene el usuario

            //Peticion al post para saber la cantidad de horas que tiene el usuario consumidas en peliculas:
            $.post("../modelo/modelo.php",{horasUsuarioInfoPerfil:nickBuscarInfo},function(respuestaInfoPerfilMinutos){
                //console.log(respuestaInfoPerfilHoras);
                var minutos =respuestaInfoPerfilMinutos;
                //si el usuario no ha votado, no puede tener minutos
                if(minutos.length==0){
                    minutos = "0";
                }
                $("#tablaMinutosTotales").html("<i class='far fa-clock'></i><br>"+minutos);
            })//Fin peticion al post para saber la cantidad de horas que tiene el usuario

           

            //Consulta para obtener todos los datos de un usuario por nick
            $.post("../modelo/modelo.php",{nickUsuarioBuscarInfoAdmin:nickBuscarInfo,idAdmin:sessionStorage.Id},function(respuestaBuscarInfoUsuarioAdmin){
                
                var arrayInfoCompletaUsuario = JSON.parse(respuestaBuscarInfoUsuarioAdmin);
                //console.log(arrayInfoCompletaUsuario);

                //Recorro el array para imprimir los datos
                for(var i = 0 ; i<arrayInfoCompletaUsuario.length;i++){

                    var id = arrayInfoCompletaUsuario[i][0];
                    var nick = arrayInfoCompletaUsuario[i][1];
                    var email = arrayInfoCompletaUsuario[i][2];
                    var nombre = arrayInfoCompletaUsuario[i][3];
                    var apellidos = arrayInfoCompletaUsuario[i][4];
                    var tipo = arrayInfoCompletaUsuario[i][5];
                    var hash = arrayInfoCompletaUsuario[i][6];
                    var activa = arrayInfoCompletaUsuario[i][7];

                    $(".spanIdUsuario").text(id);
                    $(".spanNickUsuario").text(nick);
                    $(".spanEmailUsuario").text(email);
                    $(".spanNombreUsuario").text(nombre);
                    $(".spanApellidosUsuario").text(apellidos);
                    if(tipo==0){
                        $(".spanTipoUsuario").text("Usuario de FilmRate");
                    }else{
                        $(".spanTipoUsuario").text("Administrador de FilmRate");
                    }
                    $(".spanHashUsuario").text(hash);
                    if(activa==0){
                        $(".spanActivaUsuario").html("<span class='cuentaNoActivada'>Cuenta sin activar <i class='fas fa-times-circle'></i></span> <span class='botonActivarCuenta'>Activar Cuenta</span>");
                    }else{
                        $(".spanActivaUsuario").html("<span class='cuentaSiActivada'>Cuenta activada <i class='fas fa-check-circle'></i></span>");
                    }
                    

                }//Fin recorro el array para imprimir los datos


                //Si le doy al icono de editar perfil
                $(".configUser").on("click",function(){
                    //Oculto el icono y muestro las opciones de configuración
                    $(".configUser").hide();
                    $("#configuracionUsuario").show();
                    $(".botonCambiarPasswordUsuario").show();
                    //$(".guardarEdicionUsuario").show();
                    $(".oculto").show();
                    $(".cancelarEdicionUsuario").show();
               
                    var arrayDatosUsuarioEditar = [];


                    //Recorro el array para editar los datos
                    for(var z = 0 ; z<arrayInfoCompletaUsuario.length;z++){

                        var idEditar = arrayInfoCompletaUsuario[z][0];
                        var nickEditar = arrayInfoCompletaUsuario[z][1];
                        var emailEditar = arrayInfoCompletaUsuario[z][2];
                        var nombreEditar = arrayInfoCompletaUsuario[z][3];
                        var apellidosEditar = arrayInfoCompletaUsuario[z][4];
                        var tipoEditar = arrayInfoCompletaUsuario[z][5];
                        var hashEditar = arrayInfoCompletaUsuario[z][6];                     
                        

                        //$(".editar"+z).html("<input class='inputEditar'"+z+" value="+idEditar[z]+"></input>");


                    }//Fin recorrer el array para editar los datos

                    arrayDatosUsuarioEditar.push(idEditar);
                    arrayDatosUsuarioEditar.push(nickEditar);
                    arrayDatosUsuarioEditar.push(emailEditar);
                    arrayDatosUsuarioEditar.push(nombreEditar);
                    arrayDatosUsuarioEditar.push(apellidosEditar);
                    arrayDatosUsuarioEditar.push(tipoEditar);
                    arrayDatosUsuarioEditar.push(hashEditar);

                    //recorro el array cambiando los span por campos input text con el texto que habia en el span
                    for(var m = 1; m<5;m++){                        
                        $(".editar"+[m]).html("<input class='inputEditar"+m+"' value="+arrayDatosUsuarioEditar[m]+"></input>");
                    }//fin recorro el array cambiando los span por campos input text con el texto que habia en el span

                    
                    $(".editar5").html("<form class='selectorTipoUsuario'> <select name='tipousuario' id='tipousuario'><option class='opcion' value='0'>Usuario</option><option class='opcion' value='1'>Administrador</option></select></form>")

                   
                    //Cuando hago click en el boton de guardar cambios
                    $(".guardarEdicionUsuario").on("click",function(){

                        var idUsuarioAModificar = $(".editar0").text();
                        //console.log("USUARIO A EDITAR -> "+idUsuarioAModificar);
                        
                        //Tengo que meter los datos en un array para enviarlos al modelo
                        var arrayDatosModificarUsuario = [];

                        //var idEditar = $(".inputEditar0").val();
                        var nickEditar = $(".inputEditar1").val();
                        var emailEditar = $(".inputEditar2").val();
                        var nombreEditar = $(".inputEditar3").val();
                        var apellidosEditar = $(".inputEditar4").val();
                        var tipoUsuarioEditar = $("#tipousuario").val();
                        

                        //arrayDatosModificarUsuario.push(idEditar);
                        arrayDatosModificarUsuario.push(nickEditar);
                        arrayDatosModificarUsuario.push(emailEditar);
                        arrayDatosModificarUsuario.push(nombreEditar);
                        arrayDatosModificarUsuario.push(apellidosEditar);
                        arrayDatosModificarUsuario.push(tipoUsuarioEditar);

                        //console.log(arrayDatosModificarUsuario);

                        //Consulta para editar los datos del usuario en la base de datos
                        $.post("../modelo/modelo.php",{arrayDatosModificarUsuario:arrayDatosModificarUsuario,idAdmin:sessionStorage.Id,idUsuarioAModificar:idUsuarioAModificar},function(respuestaEditarUsuario){
                            //console.log(respuestaEditarUsuario);

                            //Si el usuario se ha modificado correctamente:
                            if(respuestaEditarUsuario=="USUARIOMODIFICADO"){
                                //Muestro un mensaje de sweetalert y recargo la pagina:                                            
                                Swal.fire({
                                    title:'Usuario modificado correctamente',
                                    icon:'success',
                                    showConfirmButton:false
                                });
                                setTimeout(recargarPagina,1700);
                                //Funcion para recargar pagina usandola en el location reload anterior
                                function recargarPagina(){
                                    window.location.href="/perfilUsuario/"+nickEditar;
                                }
                            }//Fin se el usuario se ha modificado correctamente


                            //Si el nick ya existe en la base de datos:
                            if(respuestaEditarUsuario=="NICKEXISTENTE"){
                                Swal.fire({
                                    title:'El nick ya existe',
                                    icon:'error',
                                    showConfirmButton:false,
                                    timer:1500,
                                });
                                $(".inputEditar1").val("");
                                $(".inputEditar1").focus();                               

                            }//Fin si el nick ya existe en la base de datos

                            //Si el nick ya existe en la base de datos:
                            if(respuestaEditarUsuario=="EMAILEXISTENTE"){
                                Swal.fire({
                                    title:'El email ya existe',
                                    icon:'error',
                                    showConfirmButton:false,
                                    timer:1500,
                                });
                                $(".inputEditar2").val("");
                                $(".inputEditar2").focus();                               

                            }//Fin si el nick ya existe en la base de datos


                        });//Fin consulta para editar los datos del usuario en la base de datos




                    });//Fin click en el boton guardar cambios

                    //Si pulsamos en el boton cancelar, recargo pagina simplemente
                    $(".cancelarEdicionUsuario").on("click",function(){
                        window.location.reload();
                    });//Fin pulsar en el boton cancelar

                    //Si se pulsa en un input y se escribe algo aparece el boton de guardar
                    for(var t = 1; t<5;t++){
                        $(".inputEditar"+t).keyup(function(){
                            $(".guardarEdicionUsuario").show();
                        });
                    }
                    $("option").on("click",function(){
                        $(".guardarEdicionUsuario").show();
                    })//Fin si se pulsa en input y se escribe algo aparece el boton guardar


                    //Si pulsamos en el boton "Cambiar Contraseña"
                   $(".botonCambiarPasswordUsuario").on("click",function(){
                        var idUsuarioCambiarPassword = $(".editar0").text();

                        //Tengo que pedir la contraseña actual para comprobar que el usuario actual quiere realmente cambiar su contraseña                
                        Swal.fire({
                            title:'Introduce la nueva contraseña',
                            input:'password',
                            showCancelButton:true,
                            cancelButtonText:'Cancelar',
                            confirmButtonText:'Aceptar'
                        }).then((resultado)=>{
                            if(resultado.value){
                                var nuevaPassword = resultado.value;
                                //Consulta para que el administrador cambie la contraseña del usuario
                                $.post("../modelo/modelo.php",{administradorCambiaPassword:"true",idAdmin:sessionStorage.Id,idUsuarioCambiarPassword:idUsuarioCambiarPassword,nuevaPasswordUsuario:nuevaPassword},function(respuestaCambiarPassword){
                                        //console.log(respuestaCambiarPassword);
                                        //Si se cambio correctamente
                                        if(respuestaCambiarPassword=="PASSWORDUSUARIOCAMBIADA"){
                                            //Muestro un mensaje de sweetalert y recargo la pagina:                                            
                                            Swal.fire({
                                                title:'Contraseña cambiada correctamente',
                                                icon:'success',
                                                showConfirmButton:false,
                                                timer:1500,
                                            });
                                        //Fin si se cambio correctamente
                                        }else{
                                            Swal.fire({
                                                title:'Ha habido un error al cambiar la contraseña',
                                                icon:'error',
                                                showConfirmButton:false,
                                                timer:1500,
                                            });
                                        }//Fin si no se cambia correctamente
                                });//Fin consulta cambiar contraseña del usuario
                            }
                        })//Fin pedir contraseña


                    });//Fin si pulsamos en el boton "Cambiar Contraseña"

                });//Fin click en el icono editar

                //Si se pulsa en el boton para activar un usuario que no tiene la cuenta activada
                $(".botonActivarCuenta").on("click",function(){
                    var idUsuarioActivar = $(".editar0").text();
                    //console.log(idUsuarioActivar);

                    //Consulta para activar una cuenta
                    $.post("../modelo/modelo.php",{activarCuenta:"true",idAdmin:sessionStorage.Id,idCuentaActivar:idUsuarioActivar},function(respuestaActivarUsuario){
                            console.log(respuestaActivarUsuario);
                            //Si se activa la cuenta
                            if(respuestaActivarUsuario=="CUENTAACTIVADA"){
                                
                                    //Muestro un mensaje de sweetalert y recargo la pagina:                                            
                                    Swal.fire({
                                        title:'Cuenta activada correctamente',
                                        icon:'success',
                                        showConfirmButton:false
                                    });
                                    setTimeout(recargarPagina,1700);
                                    //Funcion para recargar pagina usandola en el location reload anterior
                                    function recargarPagina(){
                                        window.location.reload();
                                    }
                             
                            }//Fin si se activa la cuenta
                    });//Fin consulta activar cuenta
                });//Fin si se pulsa en el boton para activar un usuario que no tiene la cuenta activada




            });//Fin consulta para obtener todos los datos de un usuario por nick
            
    
            //Al pulsar en "ELIMINAR USUARIO"  
            $(".eliminarUsuario").on("click",function(){
                var idUsuarioEliminar = $(".editar0").text();
                
                //Muestro mensaje de sweetalert
                Swal.fire({
                    title:'¿Deseas eliminar este usuario?',                    
                    showCancelButton:true,
                    confirmButtonText: 'Eliminar',
                    cancelButtonText: 'Cancelar',
                }).then((result)=>{       
                    //Si se pulsa en el boton "Eliminar"
                    if(result.value==true){
                        //Consulta al modelo para eliminar el usuario
                        $.post("../modelo/modelo.php",{idUsuarioEliminar:idUsuarioEliminar,idAdmin:sessionStorage.Id},function(respuestaEliminarUsuario){
                            //console.log(respuestaEliminarUsuario);
                            if(respuestaEliminarUsuario=="USUARIOELIMINADO"){
                                //Muestro un mensaje de sweetalert y recargo la pagina:                                            
                                Swal.fire({
                                    title:'Cuenta eliminada correctamente',
                                    icon:'success',
                                    showConfirmButton:false
                                });
                                setTimeout(recargarPagina,1700);
                                //Funcion para recargar pagina usandola en el location reload anterior
                                function recargarPagina(){
                                    window.location.href="/listaUsuarios";
                                }
                            }
                        });//Fin consulta al modelo para eliminar el usuario

                    }
                });
                //Fin mensaje de sweetalert








              

            })//Fin al pulsar en Eliminar Usuario
            

});//FIN DOCUMENT READY


//Funcion para obtener de la url el parametro nick:
function obtenerIdURL(){
    var stringUrl =window.location.href;
    var arrayUrl = stringUrl.split("/");
    var parametros =arrayUrl[arrayUrl.length-1]; 
    //console.log(parametros);
    return parametros;
};//Fin funcion obtener de la url el parametro;