//Este es el controlador del fichero index para los usuarios que se han logeado.
$(document).ready(function(){

	//console.log(sessionStorage);

	//Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);

  
    //Peticion para obtener todas las criticas del usuario
    $.post("../modelo/modelo.php",{obtenerCriticasUsuario:"true",idUsuarioListaCriticas:sessionStorage.Id},function(respuestaCriticasUsuario){

            //Array con todas las criticas del usuario
            arrayCriticasUsuario = JSON.parse(respuestaCriticasUsuario);
            //console.log(arrayCriticasUsuario);

            if(arrayCriticasUsuario.length>0){

                //Recorro el array para poder imprimir cada critica por separado
                for(var i = 0; i<arrayCriticasUsuario.length;i++){

                    idFilmCritica = arrayCriticasUsuario[i][0];
                    tituloFilmCritica = arrayCriticasUsuario[i][1];
                    fotoFilmCritica =arrayCriticasUsuario[i][2];
                    criticaUsuarioFilm = arrayCriticasUsuario[i][3];
                    fechaCriticaUsuario =arrayCriticasUsuario[i][4];

                    //Tengo que ir imprimiendo cada pelicula:
                    $("#contenedorCriticas").append(
                        `<div class='criticaDelUsuario idFilmCritica`+idFilmCritica+`'> 
                            <div style="display:flex">
                                <img class='fotoFilmCritica' src='`+fotoFilmCritica+`'/> 
                                <span class='cadaTituloCritica'>`+tituloFilmCritica+`</span>
                            </div>                      
                            <br>                    
                            <i class='textoCriticasUsuario'>`+'"'+criticaUsuarioFilm+'"'+`</i>
                            <div class='fechaCriticaUsuario'>`+fechaCriticaUsuario+`</div>
                        </div>`
                    );//Fin imprimir cada pelicula

                }//Fin recorro el array para poder imprimir cada critica por separado

            }else{
                $("#contenedorCriticas").append(`<div>No ha escrito críticas en films.</div>`);
            }

            //Al hacer click en cada pelicula debo redirigir a esa pelicula
            $(".criticaDelUsuario").on("click",function(){
                 //Obtengo el id de ese film
                var idFilmCriticaPulsada = $(this).attr("class").split(" ")[1].split("idFilmCritica")[1];			
                //Redirijo a la pagina del film:
                window.location.href="/film/"+idFilmCriticaPulsada;
                
            });//Fin al hacer click en cada pelicula debo redirigir a esa pelicula

    })//Fin peticion para obtener todas las criticas del usuario


});