//Este es el controlador del fichero index para los usuarios que se han logeado.
$(document).ready(function(){

	//console.log(sessionStorage);
	

	//Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);





    //Necesito saber el numero TOTAL de peliculas que existe en la base de datos:
    $.post("../modelo/modelo.php",{numeroTotalPeliculas:"true"},function(respuestaNumeroTotalPeliculas){
        //console.log(respuestaNumeroTotalPeliculas);
       
       
        //console.log(numeroPeliculas);

       
        //Peticion obtener todas las peliculas ordenadas por titulo
        $.post("../modelo/modelo.php",{obtenerTodasLasPeliculas:"true"},function(respuestaObtenerTodasLasPeliculas){
            //console.log(respuestaMostrarDiezPeliculas);
            var arrayTodasLasPeliculas = JSON.parse(respuestaObtenerTodasLasPeliculas);

            //ARRAY DE TODAS LAS PELICULAS:
            //console.log(arrayTodasLasPeliculas);

            for(var i=0;i<10;i++){
                var idFilmDiez = arrayTodasLasPeliculas[i][2];               
                var tituloFilmDiez = arrayTodasLasPeliculas[i][1];
                var fotoFilmDiez = arrayTodasLasPeliculas[i][0];

                //Añado las peliculas al div
                $("#listadoCompletoFilms").append(`
                <div class='contenedorFilmDiez idFilmTop`+idFilmDiez+`'>
                    <img class='fotoDiez' src='`+fotoFilmDiez+`'/>
                    <span class='tituloDiez'><i>`+tituloFilmDiez+`</i></span>                   
                </div>`);//Fin añadir peliculas
            }





            /*
            //Muestro 10 peliculas (resto por defecto a ese numero 10);
            numeroPeliculas = numeroPeliculas-10;

            //Por cada pelicula:
            for(var i=0;i<arrayDiezPeliculas.length;i++){

                var idFilmDiez = arrayDiezPeliculas[i][0];
                var notaFilmDiez = parseFloat(arrayDiezPeliculas[i][1]).toFixed(1);
                var tituloFilmDiez = arrayDiezPeliculas[i][2];
                var fotoFilmDiez = arrayDiezPeliculas[i][3];

                //Añado las peliculas al div
                $("#listadoCompletoFilms").append(`
				<div class='contenedorFilmDiez idFilmTop`+idFilmDiez+`'>
					<img class='fotoDiez' src='`+fotoFilmDiez+`'/>
					<span class='tituloDiez'><i>`+tituloFilmDiez+`</i></span>
					<span class='notaDiez'><div class='colorNotaDiez'>`+notaFilmDiez+`</div></span>
				</div>`);//Fin añadir peliculas
            }//Fin por cada pelicula
*/


            var numeroPeliculas = parseInt(respuestaNumeroTotalPeliculas); //48 en total  

            //var peliculasImprimidas = $(".contenedorFilmDiez").length; //10 inicialmente

          
            //Creo dos contadores, que se utilizarán para recorrer el array de peliculas cada vez que se pulsa el boton
            var contadorMin = 10;
            var contadorMax = 10;
            //Al pulsar el boton de mostrar diez mas
            $("#btnDiezMas").on("click",function(){    
                //Imcremento el contador máximo de peliculas de diez en diez mientras sea menor al numero total de peliculas
                if(contadorMax<numeroPeliculas){
                    contadorMax = contadorMax +10;
                }

                //Si el contador maximo es mayor que el numero de peliculas, debera ser igual que el numero de peliculas
                if(contadorMax>numeroPeliculas){
                    //contadorMax = Math.abs((contadorMax-numeroPeliculas)-10);                    
                    contadorMax = numeroPeliculas;
                }
                
                //Recorro el minimo y el maximo añadiendo las peliculas
                for(var m=contadorMin;m<contadorMax;m++){
                    //console.log(arrayTodasLasPeliculas[m][1]);                 
                
                    var idFilmDiezMas = arrayTodasLasPeliculas[m][2];               
                    var tituloFilmDiezMas = arrayTodasLasPeliculas[m][1];
                    var fotoFilmDiezMas = arrayTodasLasPeliculas[m][0];                
                
                    //Añado las peliculas al div
                    $("#listadoCompletoFilms").append(`
                    <div class='contenedorFilmDiez idFilmTop`+idFilmDiezMas+`'>
                        <img class='fotoDiez' src='`+fotoFilmDiezMas+`'/>
                        <span class='tituloDiez'><i>`+tituloFilmDiezMas+`</i></span>                   
                    </div>`);//Fin añadir peliculas


                }//Fin añadir peliculas
               
                //console.log("Contador Max (TotalMostradas) -> "+contadorMax);
                //console.log("Contador Min (MuestroDesde) -> "+contadorMin);
                
                //Si el numero maximo de peliculas a mostrar es el numero de peliculas total, oculto el boton de mostrar mas
                if(contadorMax==numeroPeliculas){
                    $("#btnDiezMas").hide();
                }//Fin ocultar boton mostrar mas:

                contadorMin = contadorMin+10;
                
                //Llamo a la funcion para redirigir a la pagina principal de la pelicula
                irPaginaFilm();

            })//Fin boton 10 mas           

            //Llamo a la funcion para redirigir a la pagina principal de la pelicula
            irPaginaFilm();

        });//Fin mostrar 10 peliculas

    });//Fin saber numero total de peliculas que existe en la base de datos



});//Fin document ready


//Funcion para enviar a la pagina de la pelicula al hacer click en una pelicula
function irPaginaFilm(){
    //Cuando hago click en el film, debo obtener el título:
    //$(".contenedorFilmDiez").on("click",function(){
    //    console.log($(this).children().text());
    //});

    //Cuando hago click en una pelicula
    $(".contenedorFilmDiez").on("click",function(){
        //Obtengo el id de ese film
        var idFilmTopTresPulsado = $(this).attr("class").split(" ")[1].split("idFilmTop")[1];			
        //Redirijo a la pagina del film:
        window.location.href="/film/"+idFilmTopTresPulsado;
    });//Fin al hacer click en una de las tres peliculas	

}//Fin funcion para enviar a la pagina de la pelicula al hacer click en un film