$(document).ready(function(){
    //console.log(sessionStorage);
	
    
    //Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);

    //Oculto el mensaje de usuario no encontrado:
    $("#infoNoExistente").hide();
    

    //Obtener de la url el id de la pelicula actual
    var nickBuscarInfo = obtenerIdURL();
    //console.log(idBuscarInfo);
    //console.log(nickBuscarInfo);
    
    //Si el nick no esta definido en la url
    if(nickBuscarInfo==undefined){
        //La informacion del usuario a buscar será la del usuario que estrá iniciado
        nickBuscarInfo=sessionStorage.Nick;        
    }

    //Si se intenta poner admin o administrador en la url:
    if(nickBuscarInfo=="admin"||nickBuscarInfo=="administrador"){
        window.location.replace("/perfil/"+sessionStorage.Nick);
    }//Fin intentar poner administrador en la url


    //Si la información del nick que busco es mia, podré editarla tambien:
    if(nickBuscarInfo===sessionStorage.Nick){
        nickBuscarInfo = nickBuscarInfo.charAt(0).toUpperCase()+nickBuscarInfo.slice(1);
        //console.log("INFORMACION MIA ->"+nickBuscarInfo);

        //Inserto en el titulo el nick y el nombre debajo:
        //$("#nickperfil").html("Perfil de "+nickBuscarInfo+"<i class='fas fa-user-cog configUser'></i>");
        $("#nickperfil").html("Perfil de "+nickBuscarInfo+"<i title='Ajustes' id='ajustesPerfil' class='fas fa-cog configUser'></i>");
        $("#nombreCompleto").html(sessionStorage.Apellidos+", "+sessionStorage.Nombre);
        
        //Tengo que hacer consultas para saber todos los datos que tendrá la tabla
        /*<td id='tablaMedia'>1</td>
        <td id='tablaVotos'>2</td>
        <td id='tablaCriticas'>3</td>
        <td id='tablaFavoritas'>4</td>*/
        

            //Peticion al post para saber la media de votos del usuario:
            $.post("../modelo/modelo.php",{mediaUsuarioInfoPerfil:sessionStorage.Nick},function(respuestaInfoPerfilMedia){
                var notaMediaNick = parseFloat(respuestaInfoPerfilMedia).toFixed(2);     
                //Si la media es NaN la igualo a cero:
                if(isNaN(notaMediaNick)){
                    notaMediaNick=0;
                }    

                //Inserto en la tabla el resultado
                $("#tablaMedia").html("<i class='fa fa-bar-chart'></i><br>"+notaMediaNick);
            });//Fin peticion al post para saber la media de votos del usuario


            //Peticion al post para saber la cantidad de votos que ha hecho el usuario:
            $.post("../modelo/modelo.php",{votosUsuarioInfoPerfil:sessionStorage.Nick},function(respuestaInfoPerfilVotos){
                var votosNick = respuestaInfoPerfilVotos;
                //Inserto en la tabla el resultado
                $("#tablaVotos").html("<i class='fa fa-film'></i><br>"+votosNick);
            });//Fin peticion al post para saber la cantidad de votos que ha hecho el usuario


            //Peticion al post para saber la cantidad de criticas que ha hecho el usuario:
            $.post("../modelo/modelo.php",{criticasUsuarioInfoPerfil:sessionStorage.Nick},function(respuestaInfoPerfilCriticas){
                var criticasNick = respuestaInfoPerfilCriticas;
                //Inserto en la tabla el resultado
                $("#tablaCriticas").html("<i class='fa fa-pencil'></i><br>"+criticasNick);
            });//Fin peticion al post para saber la cantidad de votos que ha hecho el usuario

            
            //Peticion al post para saber la cantidad de peliculas favoritas que tiene el usuario:
            $.post("../modelo/modelo.php",{favoritasUsuarioInfoPerfil:sessionStorage.Nick},function(respuestaInfoPerfilFavoritas){
                var favoritasNick = respuestaInfoPerfilFavoritas;
                //Inserto en la tabla el resultado
                $("#tablaFavoritas").html("<i class='fa fa-star'></i><br> "+favoritasNick);
            });//Fin peticion al post para saber la cantidad de peliculas favoritas que tiene el usuario

            //Peticion al post para saber la cantidad de horas que tiene el usuario consumidas en peliculas:
            $.post("../modelo/modelo.php",{horasUsuarioInfoPerfil:sessionStorage.Nick},function(respuestaInfoPerfilMinutos){
                //console.log(respuestaInfoPerfilHoras);
                var minutos =respuestaInfoPerfilMinutos;
                //si el usuario no ha votado, no puede tener minutos
                if(minutos.length==0){
                    minutos = "0";
                }
                $("#tablaMinutosTotales").html("<i class='far fa-clock'></i><br>"+minutos);
            })//Fin peticion al post para saber la cantidad de horas que tiene el usuario


            //Si le doy al icono de configuración de mi perfil
            $(".configUser").on("click",function(){
                //Oculto el icono y muestro las opciones de configuración
                $(".configUser").hide();
                $("#configuracionUsuario").show();
            });


            //OPCIONES DE CONFIGURACION
            //<div><p id='cambiarNombre'>Cambiar nombre</p></div>
            //<div><p id='cambiarApellidos'>Cambiar apellidos</p></div>
            //<div><p id='cambiarPassword'>Cambiar contraseña</p></div>                
            //<div><p id='eliminarCuenta'>Eliminar mi cuenta</p></div>

            //Si hago click en cambiar nombre:
            $("#cambiarNombre").on("click",function(){                
                //Muestro el input de sweetalert2
                Swal.fire({
                    title:'Introduce nuevo nombre',
                    input:'text',
                    showCancelButton:true,
                    cancelButtonText:'Cancelar'
                }).then((resultado)=>{
                    //console.log(resultado.length);
                    if(resultado.value){//Si se introduce un nombre                                                    
                        if(resultado.value.length!=0){//Si el nombre que se introduce es mayor a un caracter                            
                            //Paso la primera letra del nombre a mayusculas:
                            var nuevoNombre = resultado.value.charAt(0).toUpperCase()+resultado.value.slice(1);
                            //Hacer petición al post
                            $.post("../modelo/modelo.php",{nuevoNombreUsuario:nuevoNombre,nickUsuarioCambiarNombre:sessionStorage.Nick},function(respuestaCambiarNombre){                                
                                if(respuestaCambiarNombre=="NOMBRECAMBIADO"){
                                    //Inerto el nombre en el perfil del usuario:
                                    var arrayNombreMostrarPerfil = $("#nombreCompleto").html().split(",")
                                    arrayNombreMostrarPerfil[1]=nuevoNombre;
                                    stringNuevoNombre = arrayNombreMostrarPerfil.join(", ");
                                    $("#nombreCompleto").html(stringNuevoNombre);

                                    //Cambio el nombre en session sotrage:  
                                    sessionStorage.Nombre=stringNuevoNombre;                                   


                                    //Muestro el mensaje correcto:                                    
                                    Swal.fire({
                                        icon:'success',
                                        text:'Nombre cambiado con éxito',
                                        showConfirmButton:false,
                                        timer:1200
                                    })//Fin mensaje correcto
                                }//Fin se ha cambiado el nombre                                
                            })//Fin post cambiar nombre
                            
                        }
                    }
                })//Fin sweet alert2                
            });//Fin cambiar nombre



            //Si hago click en cambiar apellido:
            $("#cambiarApellidos").on("click",function(){                
                //Muestro el input de sweetalert2
                Swal.fire({
                    title:'Introduce nuevos apellidos',
                    input:'text',
                    showCancelButton:true,
                    cancelButtonText:'Cancelar'
                }).then((resultado)=>{
                    if(resultado.value){//Si se introduce un nombre                            
                        if(resultado.value.length!=0){//Si los apellidos que se introducen son mayores a un caracter
                            //Paso la primera letra del nombre a mayusculas:
                            var nuevosApellidos = resultado.value;
                            var arrayNuevosApellidos = nuevosApellidos.split(" ");
                            
                            //Paso todos los apellidos del array a mayusculas
                            for(var i=0;i<arrayNuevosApellidos.length;i++){                               
                                arrayNuevosApellidos[i]= arrayNuevosApellidos[i].charAt(0).toUpperCase()+arrayNuevosApellidos[i].slice(1);
                            }//Fin pasar apellidos a mayusculas                            
                            var stringApellidos = arrayNuevosApellidos.join(" ");
                            
                            
                            //Hacer petición al modelo para cambiar apellidos                            
                            $.post("../modelo/modelo.php",{nuevosApellidosUsuario:stringApellidos,nickUsuarioCambiarApellidos:sessionStorage.Nick},function(respuestaCambiarApellidos){                                     
                                //Si se cambian correctamente los apelidos
                                if(respuestaCambiarApellidos=="APELLIDOSCAMBIADOS"){    
                                    //Introduzco los apellidos en el html del perfil                                
                                    var arrayApellidosMostrarPerfil = $("#nombreCompleto").html().split(",")
                                    arrayApellidosMostrarPerfil[0]=stringApellidos;
                                    stringApellidos = arrayApellidosMostrarPerfil.join(", ");
                                    $("#nombreCompleto").html(stringApellidos);

                                    //Cambio los apellidos en session sotrage:  
                                    sessionStorage.Apellidos=stringApellidos; 
                                    
                                    //Muestro el mensaje correcto:                                    
                                    Swal.fire({
                                        icon:'success',
                                        text:'Nombre cambiado con éxito',
                                        showConfirmButton:false,
                                        timer:1200
                                    })//Fin mensaje correcto*/
                                }//Fin se ha cambiado el nombre                                
                            })//Fin post cambiar apellidos                            
                        }//Fin si los apellidos introducidos son mayores a un caraceter
                    }//Fin si se introduce algo
                })//Fin sweet alert2                
            });//Fin cambiar nombre



             //Si hago click en cambiar contraseña:
             $("#cambiarPassword").on("click",function(){  

                //Tengo que pedir la contraseña actual para comprobar que el usuario actual quiere realmente cambiar su contraseña                
                Swal.fire({
                    title:'Introduce tu contraseña',
                    input:'password',
                    showCancelButton:true,
                    cancelButtonText:'Cancelar'
                }).then((resultado)=>{                   
                    if(resultado.value) {
                        var passwordpedida = resultado.value;

                        //Peticion al modelo para comprobar atutenticidad del usuario
                        $.post("../modelo/modelo.php",{passwdUsuarioAutenticar:passwordpedida,nickUsuarioAutenticar:sessionStorage.Nick},function(respuestaAutenticacion){
                            //Si la autenticacion es correcta
                            if(respuestaAutenticacion=="1"){


                                            //Pedir contraseñas con sweetalert2
                                            Swal.fire({
                                                title:'Introduce la nueva contraseña',
                                                html:
                                                    '<input id="pass1" type="text" class="swal2-input"><br>'+
                                                    '<input id="pass2" type="text" class="swal2-input">',
                                                showCancelButton:true,
                                                cancelButtonText:'Cancelar',
                                                preConfirm:()=>{//Cuando se realiza una busqueda pulsando "Enter"
                                                    
                                                    var pass1 = document.getElementById("pass1").value.replace(/ /g,"");
                                                    var pass2 = document.getElementById("pass2").value.replace(/ /g,"");

                                                    //Si las contraseñas son iguales y tienen más de 8 caracteres
                                                    if(pass1.length>=8&&pass2.length>=8&&pass1===pass2&&pass2==pass1){
                                                        //Hacer petición al modelo para cambiar la contraseña
                                                        $.post("../modelo/modelo.php",{nuevaPasswordCambiar:pass1,nickUsuarioCambiarPassword:sessionStorage.Nick},function(respuestaCambiarPassword){
                                                            //console.log(respuestaCambiarPassword);

                                                            if(respuestaCambiarPassword=="PASSWORDCAMBIADA"){
                                                                Swal.fire({
                                                                    icon:'success',
                                                                    text:'Contraseña cambiada con éxito',
                                                                    showConfirmButton:false,
                                                                    timer:1200
                                                                })//Fin mensaje de error

                                                            }
                                                        });//Fin peticion al modelo para cambiar contraseña
                                                    }else{
                                                        //Muestro el mensaje de error:                                    
                                                        Swal.fire({
                                                            icon:'error',
                                                            text:'Las contraseñas deben ser iguales y tener más de 8 caracteres',
                                                            showConfirmButton:false,
                                                            timer:1500
                                                        })//Fin mensaje de error
                                                    }//Fin si las contraseñas son iguales y tienen más de 8 caracteres                



                                                }//Fin cuando se pulsa enter
                                            })//Fin pedir contraseñas con sweetalert2



                                            //Cuando se empieza escribir en los inputs debo eliminar los intentos de meter espacios en blanco:
                                            $("#pass1").keyup(function(evento){                    
                                                //Dificultar la introduccion de ESPACIOS EN BLANCO:
                                                $("#pass1").val($("#pass1").val().trim());
                                                $("#pass1").val($("#pass1").val().replace(/ /g,""));
                                            });//Fin escribir inputs
                                            //Cuando se empieza escribir en los inputs debo eliminar los intentos de meter espacios en blanco:
                                            $("#pass2").keyup(function(evento){                    
                                                //Dificultar la introduccion de ESPACIOS EN BLANCO:
                                                $("#pass2").val($("#pass2").val().trim());
                                                $("#pass2").val($("#pass2").val().replace(/ /g,""));
                                            });//Fin escribir inputs
                                

                            }else{
                                //Muestro el mensaje de error:                                    
                                Swal.fire({
                                    icon:'error',
                                    text:'Contraseña incorrecta',
                                    showConfirmButton:false,
                                    timer:1500
                                })//Fin mensaje de error
                            }//Si la autenticacion es correcta



                        });//Fin peticion comprobar autenticacion
                    }
                });//Fin pedir la contraseña actual para comprobar que el usuario actual quiere realmente cambiar su contraseña                
             });//Fin cambiar contraseña



             //Si hago click en eliminar mi cuenta:
             $("#eliminarCuenta").on("click",function(){ 
                 
                //Tengo que pedir la contraseña actual para comprobar que el usuario actual quiere realmente cambiar su contraseña                
                Swal.fire({
                title:'Introduce tu contraseña',
                input:'password',
                showCancelButton:true,
                cancelButtonText:'Cancelar'
                }).then((resultado)=>{                   
                    if(resultado.value) {
                        var passwordpedida = resultado.value;

                        //Peticion al modelo para comprobar atutenticidad del usuario
                        $.post("../modelo/modelo.php",{passwdUsuarioAutenticar:passwordpedida,nickUsuarioAutenticar:sessionStorage.Nick},function(respuestaAutenticacion){
                            
                            //Si la autenticacion es correcta
                            if(respuestaAutenticacion=="1"){
                                    //Preguntar al usuario si quiere eliminar su cuenta
                                    Swal.fire({
                                        title:'¿Deseas eliminar permanentemente tu cuenta?',
                                        text:'Esta acción no tiene vuelta atrás, piénsalo.',
                                        icon:'warning',
                                        showCancelButton:true,
                                        confirmButtonColor: '#d33',
                                        cancelButtonColor:  '#3085d6',
                                        confirmButtonText: 'Eliminar cuenta',
                                        cancelButtonText: 'Cancelar'
                                    }).then((result)=>{
                                        if(result.value){                                            
                                            //Hacer peticion al modelo para eliminar la cuenta:
                                            $.post("../modelo/modelo.php",{idEliminarCuenta:sessionStorage.Id},function(respuestaEliminarCuenta){

                                                
                                                if(respuestaEliminarCuenta=="CUENTAELIMINADA"){
                                                     //Muestro el mensaje de cuenta eliminada:                                    
                                                    Swal.fire({
                                                        icon:'success',
                                                        text:'Cuenta eliminada',
                                                        showConfirmButton:false,
                                                        timer:1500
                                                    })//Fin mensaje de error
                                                    setTimeout(cerrarSesion,1500);
                                                }
                                                function cerrarSesion(){
                                                    sessionStorage.clear();
                                                    window.location.replace("../modelo/logout.php");
                                                }	

                                            })//Fin peticion post eliminar cuenta                                           
                                        };
                                    })//Fin preguntar si quieres eliminar tu cuenta y eliminarla o no
                            }else{
                                //Muestro el mensaje de error:                                    
                                Swal.fire({
                                    icon:'error',
                                    text:'Contraseña incorrecta',
                                    showConfirmButton:false,
                                    timer:1500
                                })//Fin mensaje de error
                            }//Si la autenticacion es correcta

                        });//Fin post eliminar cueta
                    }
                })//Fin sweetalert2
             });//Fin eliminar cuenta


    }else{//Al ser de otro usuario, no podré hacer modificaciones
        //console.log("Inofrmacion de otro usuario ->"+nickBuscarInfo);


        //Muestro el boton para poder ver las estadisticas del usuario
        $(".oculto").show();
        //Si se hace click en el
        $("#botonBuscarEstadisticas").on("click",function(){
            
            window.location.replace("/estadisticas/"+nickBuscarInfo);                
    
        });//Fin click en boton ver estadisticas

        //Inserto en los botones el nick del usuario
        $('.nomUsuario').html(nickBuscarInfo);

            //Comprobar si existe en la base de datos:
            $.post("../modelo/modelo.php",{existeEsteNick:nickBuscarInfo},function(respuestaExisteNick){
                //Si el usuario existe en la base de datos:
                if(respuestaExisteNick=="1"){
                            //console.log("EXISTE");
                            //Inserto en el titulo el nick y el nombre debajo:
                            $("#nickperfil").html("Perfil de "+nickBuscarInfo);

                            //Tengo que saber su nombre y apellido:
                            $.post("../modelo/modelo.php",{saberNombreApellidosNick:nickBuscarInfo},function(respuestaNombreApellidos){
                                var arrayNombreApellidos = JSON.parse(respuestaNombreApellidos);
                                //console.log(arrayNombreApellidos[0]["nombre"])
                                $("#nombreCompleto").html(arrayNombreApellidos[0]["apellidos"]+", "+arrayNombreApellidos[0]["nombre"]);

                            });//Fin saber nombre y apellidos                            

                            //Peticion al post para saber la media de votos del usuario:
                            $.post("../modelo/modelo.php",{mediaUsuarioInfoPerfil:nickBuscarInfo},function(respuestaInfoPerfilMedia){
                                var notaMediaNick = parseFloat(respuestaInfoPerfilMedia).toFixed(2);     
                                //Si la media es NaN la igualo a cero:
                                if(isNaN(notaMediaNick)){
                                    notaMediaNick=0;
                                }    

                                //Inserto en la tabla el resultado
                                $("#tablaMedia").html("<i class='fa fa-bar-chart'></i><br>"+notaMediaNick);
                            });//Fin peticion al post para saber la media de votos del usuario


                            //Peticion al post para saber la cantidad de votos que ha hecho el usuario:
                            $.post("../modelo/modelo.php",{votosUsuarioInfoPerfil:nickBuscarInfo},function(respuestaInfoPerfilVotos){
                                var votosNick = respuestaInfoPerfilVotos;
                                //Inserto en la tabla el resultado
                                $("#tablaVotos").html("<i class='fa fa-film'></i><br>"+votosNick);
                            });//Fin peticion al post para saber la cantidad de votos que ha hecho el usuario


                            //Peticion al post para saber la cantidad de criticas que ha hecho el usuario:
                            $.post("../modelo/modelo.php",{criticasUsuarioInfoPerfil:nickBuscarInfo},function(respuestaInfoPerfilCriticas){
                                var criticasNick = respuestaInfoPerfilCriticas;
                                //Inserto en la tabla el resultado
                                $("#tablaCriticas").html("<i class='fa fa-pencil'></i><br>"+criticasNick);
                            });//Fin peticion al post para saber la cantidad de votos que ha hecho el usuario

                            
                            //Peticion al post para saber la cantidad de peliculas favoritas que tiene el usuario:
                            $.post("../modelo/modelo.php",{favoritasUsuarioInfoPerfil:nickBuscarInfo},function(respuestaInfoPerfilFavoritas){
                                var favoritasNick = respuestaInfoPerfilFavoritas;
                                //Inserto en la tabla el resultado
                                $("#tablaFavoritas").html("<i class='fa fa-star'></i><br>"+favoritasNick);
                            });//Fin peticion al post para saber la cantidad de peliculas favoritas que tiene el usuario

                            //Peticion al post para saber la cantidad de horas que tiene el usuario consumidas en peliculas:
                            $.post("../modelo/modelo.php",{horasUsuarioInfoPerfil:nickBuscarInfo},function(respuestaInfoPerfilMinutos){
                                //console.log(respuestaInfoPerfilHoras);
                                var minutos =respuestaInfoPerfilMinutos;
                                //si el usuario no ha votado, no puede tener minutos
                                if(minutos.length==0){
                                    minutos = 0;
                                }
                                $("#tablaMinutosTotales").html("<i class='far fa-clock'></i><br>"+minutos);
                            })//Fin peticion al post para saber la cantidad de horas que tiene el usuario

                }else{//Si no existe en la base de datos:
                    
                    $("#infoNoExistente").show();
                    $("#informacionUsuario").hide();
                    $(".tablaInfoFilmRateUsuario").hide();
                    $(".explicacionMinutosTotales").hide();
                }
            });//Fin comprobar existencia nick        

    }//Fin si la informacion que busco es mía o no.    


});//FIN DOCUMENT READY





//Funcion para obtener de la url el parametro id:
function obtenerIdURL(){
    var stringUrl =window.location.href;
    var arrayUrl = stringUrl.split("/");
    var parametros =arrayUrl[arrayUrl.length-1]; 
    //console.log(parametros);
    return parametros;
};//Fin funcion obtener de la url el parametro;