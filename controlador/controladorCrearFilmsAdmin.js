$(document).ready(function(){
    
    //console.log(sessionStorage);	

	//Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);

	//Si pulsamos en añadir manualmente
	$(".botonManualmente").on("click",function(){
		$(this).hide();
		$(".manualmente").show();
		$(".api").hide();
		$(".botonUsandoApi").show();



		//Para el pais ----------------------

				  //Cuando se pulsa en el botón para editar el país
				  $("#botonPais").on("click",function(){
					
					Swal.fire({                                    
						html:'<div class="tablaPaises"></div>',
						width:'70%',
						showConfirmButton:true,
					})

					//$(".tablaPaises").html("prueba");
					//../fotos/paises/us.png

					//Consulta para obtener la lista de países
					$.post("../modelo/modelo.php",{obtenerInfoPaisesFilms:"true"},function(respuestaPaisesFilm){

						var arrayPaises = JSON.parse(respuestaPaisesFilm);
						//console.log(arrayPaises);

						var arrayHTML = [];
						//Recorro los datos obtenidos de la consulta
						for(var i=0;i<arrayPaises.length;i++){
							var idpais = arrayPaises[i][0];
							var fotopais = arrayPaises[i][2];
							var nombrepais = arrayPaises[i][3];

							//Creo codigo HTML que insertaré en el mensaje
							var codigoHTML = 
							`<div class='cadaPais `+idpais+` `+nombrepais+`'>
								<div>
									<span>
										<img class='fotosPaisesEditarFilm' src="../fotos/paises/`+fotopais+`">
									</span>
									<span class='nombrepaiscastellano'>`+nombrepais+`</span>    
								</div>
								
								<div class='fotopais'></div>
							</div>`;

							
							arrayHTML.push(codigoHTML);
						}//Fin recorro los datos obtenidos de la consulta

						//Inserto el html
						$(".tablaPaises").html(arrayHTML)


						//Al hacer click en un pais de la lista
						$(".cadaPais").on("click",function(){
														
							//Obtengo el id del pais al que hice click:
							var idPaisSeleccionado = this.classList[1];			
						

							//LO INSERTO EN EL SPAN
							//console.log(this);
							$(".paisSeleccionado").text(idPaisSeleccionado);

							var nombrePaisSeleccionado = this.classList[2];
							

							//Oculto la tabla de paises y muestro un mensaje con el pais seleccionado
							$(".tablaPaises").html("<h4>Has seleccionado un país para este film.<h4>");

						})//Fin al hacer click en un pais de la lista

					});//Fin consulta para obtener la lista de países	

				});//Fin cuando se pulsa en el botón para editar el país


			//Fin para el pais ------------------

		

		//Al hacer click en "Crear Film"
		$(".botonCrearFilm").on("click",function(){

			//Obtengo los datos del formulario
			var titulo = $("#inputTitulo").val();


			//Quiero comprobar si el film existe en la base de datos
				$.post("../modelo/modelo.php",{consultaExisteFilm:"true",tituloFilmComprobarExiste:titulo},function(respuestaExisteFilm){
					//console.log("EXISTE EL FILM? "+respuestaExisteFilm);
					if(respuestaExisteFilm>=1){
						Swal.fire({
							title:'El film ya existe.',
							icon:'error',
							showConfirmButton:false,
							timer:1500,
						});
					}else{//SI EL FILM INTRODUCIDO NO EXISTE EN LA BASE DE DATOS:


						var tituloFinal = titulo.toLowerCase().replace(/\b[a-z]/g,function(letter){return letter.toUpperCase();})			
						var fecha = $("#inputFecha").val().split("-");
						var fecha = fecha[0];			
						var director = $("#inputDirector").val();
						var genero = $("#inputGenero").val();
						var tipo = $("#inputTipo").val();
						var sinopsis = $("#inputSinopsis").val();
						var titulofoto = titulo.toLowerCase().replace(/ /g,"");						
						var duracion = $("#inputDuracion").val();
						var guion = $("#inputGuion").val();
						var reparto = $("#inputReparto").val();
			
						var pais = $(".paisSeleccionado").text();
						
			
						//EVITO INSERCION DE SCRIPT
						regex = new RegExp(/<.*?>/g);
						rex1 = regex.test(tituloFinal);
						rex2 = regex.test(fecha);
						rex3 = regex.test(director);
						rex4 = regex.test(genero);
						rex5 = regex.test(tipo);
						rex6 = regex.test(sinopsis);
						rex7 = regex.test(titulofoto);
						rex8 = regex.test(duracion);
						rex9 = regex.test(guion);
						rex10 = regex.test(reparto);
						rex11 = regex.test(pais);
						//Si se intenta insertar codigo:
						if(rex1==true||rex2==true||rex3==true||rex4==true||rex5==true||rex6==true||rex7==true||rex8==true||rex9==true||rex10==true||rex11==true){
							Swal.fire({
								icon:'error',
								title:'ERROR',
								showConfirmButton:false,
								timer:1500
							})
						}else{//Si no se inserta codigo:
			
							//Para la foto-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
							var formData = new FormData();
							
							//Fin si no se carga foto
							if($("#image")[0].files[0]==undefined){
								//Muestro un mensaje de sweetalert y recargo la pagina:                                            
								Swal.fire({
									title:'No pueden haber campos vacíos',
									icon:'error',
									showConfirmButton:false,
									timer:1500
								});
							}else{//Si se carga una foto se crean las variables para poder subirla al servidor
			
								var files = $("#image")[0].files[0];
								var formatoFoto = $("#image")[0].files[0].name.split(".")[1];
			
								var nombreFotoSubirServidor = titulofoto+"."+formatoFoto;
								//console.log(nombreFotoSubirServidor);
			
								var ficheros = $("#image")[0].files[0];
			
							
								
								formData.append('fotos',ficheros);
								formData.append('nombrefoto',nombreFotoSubirServidor);
								//formData.append('prueba',nombreFotoSubirServidor);
			
										
								//console.log(formData.get(name));
			
								var fotoFilmFinal = "fotos/";
							}//Fin comprobacion cargar foto
			
							//REALIZAR COMPROBACIONES DE CAMPOS VACÍOS
							if(titulo.length==0||fecha.length!=4||director.length==0||genero.length==0||sinopsis.length==0||pais.length==0||duracion.length==0||guion.length==0||reparto.length==0||$("#image")[0].files[0]==undefined){
								//Muestro un mensaje de sweetalert y recargo la pagina:                                            
								Swal.fire({
									title:'No pueden haber campos vacíos',
									icon:'error',
									showConfirmButton:false,
									timer:1500
								});
								
							}else{//SI NO HAY CAMPOS VACIOS, HAGO LAS CONSULTAS PARA SUBIR LA FOTO AL SERVIDOR Y PARA METER LOS DATOS EN LA BASE DE DATOS
			
								//PETICION PARA SUBIR AL SERVIDOR LA FOTO (NO A LA BASE DE DATOS)
								$.ajax({
									url: '../modelo/modelo.php',
									type: 'post',
									data: formData,
									contentType:false,
									processData:false,
									success: function(response){
										//console.log(response);
										fotoFilmFinal=fotoFilmFinal+response;
										//console.log("LA FOTO DEL FILM EN LA BASE DE DATOS SERA-> "+fotoFilmFinal);					
			
			
										//TENGO QUE HACER UNA CONSULTA PARA CREAR EL FILM
										$.post("../modelo/modelo.php",{tipo:tipo,idAdmin:sessionStorage.Id,tituloFinal:tituloFinal,fecha:fecha,pais:pais,director:director,genero:genero,sinopsis:sinopsis,tipo:tipo,fotoFilmFinal:fotoFilmFinal,duracion:duracion,guion:guion,reparto:reparto},function(respuestaNuevoFilm){
											//console.log(respuestaNuevoFilm);
			
											if(respuestaNuevoFilm=="PELICULAAGREGADA"){
												//Muestro un mensaje de sweetalert y recargo la pagina:                                            
												Swal.fire({
													title:'Pelicula agregada con éxito.',
													icon:'success',
													showConfirmButton:false,
												});
												setTimeout(recargarPagina,1700);
												//Funcion para recargar pagina usandola en el location reload anterior
												function recargarPagina(){
													location.reload();
												}
											}
										})//Fin consulta crear film en base de datos
			
									}
								})//FIN PETICION PARA SUBIR AL SERVIDOR LA FOTO (NO A LA BASE DE DATOS)
			
			
							}//FIN COMPROBACIONES DE VARIOS CAMPOS
			
			
					
							//Fin para la foto-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------v
			
						}//Fin comprobar insercion de scripts




					}

				});//Fin post comprobar si existe el film



		});//Fin al hacer click en "Crear Film"



	})//Fin pulsar añadir manualmente


	//Para añadir utilizando la api, debo obtener el texto del film del campo
	//Al pulsar en el boton del buscador:
	$(".botonBuscadorApi").on("click",function(){
		
		//Debo obtener lo que escribimos en el campo:
		var tituloAgregar = $(".campoBuscadorApi").val().trim();

		//Si no se introduce nada en el buscador:
		if(tituloAgregar.length==0){
			Swal.fire({
				title:'Introduce el nombre de un film',
				icon:'error',
				showConfirmButton:false,
				timer:1500,
			});
		}else{//Si se introduce algo:
	


			//Comprobar si no es script:
			//EVITO INSERCION DE SCRIPT
			regex = new RegExp(/<.*?>/g);
			rexTitulo = regex.test(tituloAgregar);
			//Si es un script muestro error
			if(rexTitulo==true){
				Swal.fire({
					title:'ERROR',
					icon:'error',
					showConfirmButton:false,
					timer:1500,
				});
			}else{//Si no es un script


				//Quiero comprobar si el film existe en la base de datos
				$.post("../modelo/modelo.php",{consultaExisteFilm:"true",tituloFilmComprobarExiste:tituloAgregar},function(respuestaExisteFilm){
					//console.log("EXISTE EL FILM? "+respuestaExisteFilm);
					if(respuestaExisteFilm>=1){
						Swal.fire({
							title:'El film ya existe.',
							icon:'error',
							showConfirmButton:false,
							timer:1500,
						});
					}else{//SI EL FILM INTRODUCIDO NO EXISTE EN LA BASE DE DATOS:

						//Recojo lo que introduce el usuario
						var nombreFilm = $(".campoBuscadorApi").val();
						//Pregunto al usuario si desea añadir el film:
						Swal.fire({
							title: "¿Desea añadir '"+nombreFilm+"'?",
							showCancelButton: true,
							confirmButtonColor: "#769fe8",
							confirmButtonText: "Sí",
							cancelButtonText: "No",
						}).then((result) => {				
								

								//Si le da aceptar, la pelicula debe agregarse
								if(result.value==true){
									//Si acepta, aparecerá un mensaje con un icono de "loading" para que no tenga la sensación de que no hace nada la aplicación
									Swal.fire({									   
										title: 'Añadiendo film',
										showConfirmButton: false,
										html: `<i class="fa fa-circle-o-notch fa-spin" style="font-size:50px;color:#4063c5"></i>`,										
									});							
									//Fin mostrar mensaje


									//Tengo que cambiar los espacios en blancos del titlo por "+" para poder obtener su ID en filmaffinity
									tituloAgregar = tituloAgregar.replace(/ /g,"+");
									//console.log(tituloAgregar);

									//UTILIZO LA 1º API DE FILMAFFINITY-> Hago peticion al modelo para obtener el ID de filmaffinity
									$.post("../modelo/modelo.php",{tituloNuevoFilmApi:tituloAgregar},function(respuestaAgregarFilmApi){
										//Ejemplo del string al que se le saca el ID: "https://api-filmaffinity.herokuapp.com/api/busqueda/el+ultimo+samurai"
										var idFilmAffinity = JSON.parse(respuestaAgregarFilmApi)[0]["id"].split("/")[4].split("film")[1]; // -> Ejemplo: "267002" Es: "Watchmen"
										//console.log("PRIMERA API-> "+idFilmAffinity);
										//HAGO OTRA PETICION AL MODELO PARA OBTENER INFORMACIÓN DEL FILM UTILIZANDO LA 2º API:
										var peticionApi2ObtenerInfoFilm = {
											"async": true,
											"crossDomain": true,
											"url": "https://filmaffinity-unofficial.p.rapidapi.com/movie/detail/?id="+idFilmAffinity+"&lang=es",
											"method": "GET",
											"headers": {
												"x-rapidapi-host": "filmaffinity-unofficial.p.rapidapi.com",
												"x-rapidapi-key": "3726d73048msh953336ddc721accp18d23cjsn65ec6199963d"//Key de Rapid Api
											}
										}
									
										$.ajax(peticionApi2ObtenerInfoFilm).done(function (respuestaInfoApi2) {
											//console.log("SEGUNDA API -> "+respuestaInfoApi2)
											var director = respuestaInfoApi2.movie.director.toString();
										var reparto = respuestaInfoApi2.movie.actors.toString().replace(/'/g,"");
										var duracion = respuestaInfoApi2.movie.duration.split(" ")[0];
										var sinopsis = respuestaInfoApi2.movie.excerpt.replace("(FILMAFFINITY)","");
										var fecha =  respuestaInfoApi2.movie.year;
										var genero = respuestaInfoApi2.movie.genres.toString();
										var guion = respuestaInfoApi2.movie.director.toString(); //Le pondre el mismo guionista que el director
										var titulo = respuestaInfoApi2.movie.title;
										var tipo = "Pelicula";
										var pais = "US";
										//console.log(fecha);

										//FIN PETICION PARA OBTERNER INFO DEL FIN UTILIZANDO LA 2º API:

										//PETICION PARA OBTENER LA IMAGEN
										//TENGO QUE UTILIZAR LA API 3 PARA OBTENER EL ID DE LA IMAGEN DE IMDB:
										var tituloApi3 = tituloAgregar.replace(/\+/g,"%2520");
										//console.log(tituloApi3);
										var peticionObtenerIdImagenIMDB = {
											"async": true,
											"crossDomain": true,
											//"url": "https://imdb-internet-movie-database-unofficial.p.rapidapi.com/search/hasta%2520el%2520ultimo%2520hombre",
											"url": "https://imdb-internet-movie-database-unofficial.p.rapidapi.com/search/"+tituloApi3,
											"method": "GET",
											"headers": {
												"x-rapidapi-host": "imdb-internet-movie-database-unofficial.p.rapidapi.com",
												"x-rapidapi-key": "3726d73048msh953336ddc721accp18d23cjsn65ec6199963d"
											}
										}
										
										$.ajax(peticionObtenerIdImagenIMDB).done(function (response) {					
											var idIMDB = response.titles[0].id;
											//console.log("TERCERA API -> "+idIMDB);

											//Hacer peticion para obtener la imagen utilizando la api 4
											var settings = {
												"async": true,
												"crossDomain": true,
												"url": "https://imdb-internet-movie-database-unofficial.p.rapidapi.com/film/"+idIMDB,
												"method": "GET",
												"headers": {
													"x-rapidapi-host": "imdb-internet-movie-database-unofficial.p.rapidapi.com",
													"x-rapidapi-key": "3726d73048msh953336ddc721accp18d23cjsn65ec6199963d"
												}
											}
											
											$.ajax(settings).done(function (response) {						
												var URLImagen = response.poster; //ESTA IMAGEN TENGO QUE GUARDARLA EN EL SERVIDOR
												//console.log(URLImagen);

												//PETICION PARA AGREGAR LA PELICULA A LA BASE DE DATOS
												$.post("../modelo/modelo.php",{idAdmin:sessionStorage.Id,foto:URLImagen,director:director,reparto:reparto,duracion:duracion,sinopsis:sinopsis,fecha:fecha,genero:genero,guion:guion,titulo:titulo,tipo:tipo,pais:pais,agregadaApi:"true"},function(respuestaPeliculaAgregadaApi){
													//console.log(respuestaPeliculaAgregadaApi);
													if(respuestaPeliculaAgregadaApi=="PELICULAAGREGADAAPI"){
															//Muestro un mensaje de sweetalert y recargo la pagina:                                            
															Swal.fire({
																title:'Pelicula agregada con éxito.',
																icon:'success',
																showConfirmButton:false,
															});
															setTimeout(recargarPagina,1700);
															//Funcion para recargar pagina usandola en el location reload anterior
															function recargarPagina(){
																location.reload();
															}
													}
												});



											});
										});
										//FIN PETICION OBTENER ID DE LA IMAGEN
										});




									})//Fin peticion al modelo para obtener el ID de filmaffinity:

					/////////////////////////////////////////////////////// INSERTAR ESTE FRAGMENTO CUANDO LA 1º API VUELVA A FUNCIONAR ///////////////////////////////////////////////



					/*
										var settings = {
											"async": true,
											"crossDomain": true,
											"url": "https://filmaffinity-unofficial.p.rapidapi.com/movie/search/?lang=es&query=superman",
											"method": "GET",
											"headers": {
												"x-rapidapi-host": "filmaffinity-unofficial.p.rapidapi.com",
												"x-rapidapi-key": "f33cb97aacmshc1573b4692d643ep1eab37jsndb2a6c9af83b"
											}
										}

										$.ajax(settings).done(function (response) {
											console.log("ENTRA");
											console.log(response);
										});

					*/








					/*










									idFilmAffinity = 156313; //Comentar esta linea (a veces la api no funciona bien, este es un ejemplo con la pelicula "El protector")


									//HAGO OTRA PETICION PARA OBTENER INFORMACIÓN DEL FILM UTILIZANDO LA 2º API: 
									var settings = {
										"async": true,
										"crossDomain": true,
										"url": "https://filmaffinity-unofficial.p.rapidapi.com/movie/detail/?id="+idFilmAffinity+"&lang=es",
										"method": "GET",
										"headers": {
											"x-rapidapi-host": "filmaffinity-unofficial.p.rapidapi.com",
											"x-rapidapi-key": "f33cb97aacmshc1573b4692d643ep1eab37jsndb2a6c9af83b"//Key de Rapid Api OTRA KEY f33cb97aacmshc1573b4692d643ep1eab37jsndb2a6c9af83b
										}
									}
								
									$.ajax(settings).done(function (respuestaInfoApi2) {
										
										var director = respuestaInfoApi2.movie.director.toString();
										var reparto = respuestaInfoApi2.movie.actors.toString().replace(/'/g,"");
										var duracion = respuestaInfoApi2.movie.duration.split(" ")[0];
										var sinopsis = respuestaInfoApi2.movie.excerpt;
										var fecha =  respuestaInfoApi2.movie.year;
										var genero = respuestaInfoApi2.movie.genres.toString();
										var guion = respuestaInfoApi2.movie.director.toString(); //Le pondre el mismo guionista que el director
										var titulo = respuestaInfoApi2.movie.title;
										var tipo = "Pelicula";
										var pais = "US";
										console.log(fecha);

										//FIN PETICION PARA OBTERNER INFO DEL FIN UTILIZANDO LA 2º API:

										//PETICION PARA OBTENER LA IMAGEN
										//TENGO QUE UTILIZAR LA API 3 PARA OBTENER EL ID DE LA IMAGEN DE IMDB:
										var tituloApi3 = tituloAgregar.replace(/\+/g,"%2520");
										console.log(tituloApi3);
										var peticionObtenerIdImagenIMDB = {
											"async": true,
											"crossDomain": true,
											//"url": "https://imdb-internet-movie-database-unofficial.p.rapidapi.com/search/hasta%2520el%2520ultimo%2520hombre",
											"url": "https://imdb-internet-movie-database-unofficial.p.rapidapi.com/search/"+tituloApi3,
											"method": "GET",
											"headers": {
												"x-rapidapi-host": "imdb-internet-movie-database-unofficial.p.rapidapi.com",
												"x-rapidapi-key": "3726d73048msh953336ddc721accp18d23cjsn65ec6199963d"
											}
										}
										
										$.ajax(peticionObtenerIdImagenIMDB).done(function (response) {					
											var idIMDB = response.titles[0].id;

											//Hacer peticion para obtener la imagen utilizando la api 4
											var settings = {
												"async": true,
												"crossDomain": true,
												"url": "https://imdb-internet-movie-database-unofficial.p.rapidapi.com/film/"+idIMDB,
												"method": "GET",
												"headers": {
													"x-rapidapi-host": "imdb-internet-movie-database-unofficial.p.rapidapi.com",
													"x-rapidapi-key": "3726d73048msh953336ddc721accp18d23cjsn65ec6199963d"
												}
											}
											
											$.ajax(settings).done(function (response) {						
												var URLImagen = response.poster; //ESTA IMAGEN TENGO QUE GUARDARLA EN EL SERVIDOR
												//console.log(URLImagen);

												//PETICION PARA AGREGAR LA PELICULA A LA BASE DE DATOS
												$.post("../modelo/modelo.php",{idAdmin:sessionStorage.Id,foto:URLImagen,director:director,reparto:reparto,duracion:duracion,sinopsis:sinopsis,fecha:fecha,genero:genero,guion:guion,titulo:titulo,tipo:tipo,pais:pais,agregadaApi:"true"},function(respuestaPeliculaAgregadaApi){
													console.log(respuestaPeliculaAgregadaApi);
													if(respuestaPeliculaAgregadaApi=="PELICULAAGREGADAAPI"){
															//Muestro un mensaje de sweetalert y recargo la pagina:                                            
															Swal.fire({
																title:'Pelicula agregada con éxito.',
																icon:'success',
																showConfirmButton:false,
															});
															setTimeout(recargarPagina,1700);
															//Funcion para recargar pagina usandola en el location reload anterior
															function recargarPagina(){
																location.reload();
															}
													}
												});



											});
										});
										//FIN PETICION OBTENER ID DE LA IMAGEN


									});						

									*/

					////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


								}//Fin si el usuario le da a aceptar
								
						});
										
					}
				});//Fin comprobar si el film existe en la base de datos



			}//Fin comprobar insecion de scripts

		}//Fin si no se introduce nada en el buscador

	});//Fin al pulsar en el boton del buscador



	//Si pulsamos en usar api
	$(".botonUsandoApi").on("click",function(){
		$(this).hide();
		$(".api").show();
		$(".manualmente").hide();
		$(".botonManualmente").show();
	})//Fin pulsar usar api

});//Fin document ready
