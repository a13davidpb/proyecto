 //Si se pulsa en cerrar sesión------------------------
 $("#cerrarsesion").on("click",function(){
     cerrarNavbar();
     Swal.fire({
         title: '¿Deseas cerrar la sesión?',
         icon: 'question',
         showCancelButton:true,
         confirmButtonColor:'#3085d6',
         cancelButtonColor:'#d33',
         confirmButtonText: 'Si',
         cancelButtonText: 'No'
     }).then((result)=>{			
         if(result.value){				
             Swal.fire({
                 title:'Sesión cerrada',
                 icon:'success',
                 showConfirmButton:false
             });
             setTimeout(cerrarSesion,1500);
         }
     })		
     function cerrarSesion(){
         sessionStorage.clear();
         window.location.replace("/modelo/logout.php");
     }		
 });//Fin cerrar la sesion----------------------------- 
 
 //Cuando se hace click en el icono "home":
 $("#home").on("click",function(){
     //window.location.replace("/");
     window.location.href="/";
 });//Fin hacer click en home.
 
 
 
 //Cuando se hace click en el icono del "perfil"
 $("#irPerfil").on("click",function(){
    //window.location.replace("/vistas/perfil.php?nick="+sessionStorage.Nick);
    //window.location.href="perfil.php?nick="+sessionStorage.Nick;
    //window.location.href="/perfil/"+sessionStorage.Nick;
 })//Fin al hacer click en perfil





//Cuando se hace click en el icono de lista de films
$("#listafilmsadmin").on("click",function(){
    window.location.href="/listaFilmsAdmin";
});//Fin click lista de films

//Cuando se hace click en el icono de lista de usuarios
$("#usuariosadmin").on("click",function(){
    window.location.href="/listaUsuarios";
});//Fin click lista de usuarios

//Cuando se hace click en el icono de añadir usuario
$("#nuevousuario").on("click",function(){
    window.location.href="/crearNuevoUsuario";
});//Fin click añadir usuario

//Cuando se hace click en el icono de añadir film
$("#nuevofilm").on("click",function(){
    window.location.href="/nuevoFilm";
})//Fin click añadir film


//Funcion para el boton de scroll hacia arriba (Con anmacion, usar en el boton para subir):
function scrollTop(){
    $("html, body").animate({scrollTop:0},'fast');
    //$("html,body").scrollTop(0);
}//Fin funcion scroll up


//Funcion para el boton de scroll hacia arriba inmediato (Sin animación, util para "cambios de páginas" desde el navbar):
function scrollTopFast(){
    $("html,body").scrollTop(0);
}//Fin funcion scroll up


//Funcion que cierra la navbar (Cuando tiene el boton desplegar en pantallas pequeñas)
function cerrarNavbar(){ 
    $('.navbar-collapse').collapse('hide');
}//Fin funcion cerrar navbar
