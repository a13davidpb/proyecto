//Este es el controlador del fichero index para los usuarios que se han logeado.
$(document).ready(function(){

	//console.log(sessionStorage);
	

	//Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);


	//Imprimo el mensaje de bienvenida
	$(".mensajeBienvenida").html("Bienvenido "+sessionStorage.Nick[0].toUpperCase()+sessionStorage.Nick.substring(1));


	//Consulta para obtener las 3 películas con mejor nota media:
	$.post("../modelo/modelo.php",{topTresMejorVotadas:"true"},function(respuestaTopTresMejorVotadas){
		
		var arrayTresMejores = JSON.parse(respuestaTopTresMejorVotadas);
		//console.log(arrayTresMejores);

		//Por cada pelicula del array:
		for(var i=0;i<arrayTresMejores.length;i++){
			var idFilmTopTres = arrayTresMejores[i][0];
			var notaFilmTopTres = parseFloat(arrayTresMejores[i][1]).toFixed(1);
			var tituloFilmTopTres = arrayTresMejores[i][2];
			var fotoFilmTopTres = arrayTresMejores[i][3];
			
			//console.log(fotoFilmTopTres);
			//console.log(tituloFilmTopTres);
			//console.log(notaFilmTopTres);

			//Añado las peliculas
			$("#topTresMejores").append(`
				<div class='contenedorFilmTopTres idFilmTop`+idFilmTopTres+`'>
					<img class='fotoTopTres' src='`+fotoFilmTopTres+`'/>
					<span class='tituloTopTres'><i>`+tituloFilmTopTres+`</i></span>
					<span class='notaTopTres'><div class='colorNota'>`+notaFilmTopTres+`</div></span>
				</div>`
			);//Fin peliculas añadidas
		}//Fin por cada pelicula del top

		//Cuando hago click en una pelicula
		$(".contenedorFilmTopTres").on("click",function(){
			//Obtengo el id de ese film
			var idFilmTopTresPulsado = $(this).attr("class").split(" ")[1].split("idFilmTop")[1];			
			//Redirijo a la pagina del film:
			window.location.href="/film/"+idFilmTopTresPulsado;
		});//Fin al hacer click en una de las tres peliculas	

	});//Fin consulta para obtener las 3 películas con mejor nota media



	//Consulta para obtener las 3 películas peor valoradas:
	$.post("../modelo/modelo.php",{topTresPeorVotadas:"true"},function(respuestaTopTresMejorVotadas){
		
		var arrayTresMejores = JSON.parse(respuestaTopTresMejorVotadas);
		//console.log(arrayTresMejores);

		//Por cada pelicula del array:
		for(var i=0;i<arrayTresMejores.length;i++){
			var idFilmTopTres = arrayTresMejores[i][0];
			var notaFilmTopTres = parseFloat(arrayTresMejores[i][1]).toFixed(1);
			var tituloFilmTopTres = arrayTresMejores[i][2];
			var fotoFilmTopTres = arrayTresMejores[i][3];
			
			//console.log(fotoFilmTopTres);
			//console.log(tituloFilmTopTres);
			//console.log(notaFilmTopTres);

			//Añado las peliculas
			$("#topTresPeores").append(`
				<div class='contenedorFilmTopTres idFilmTop`+idFilmTopTres+`'>
					<img class='fotoTopTres' src='`+fotoFilmTopTres+`'/>
					<span class='tituloTopTres'><i>`+tituloFilmTopTres+`</i></span>
					<span class='notaTopTres'><div class='colorNota'>`+notaFilmTopTres+`</div></span>
				</div>`
			);//Fin peliculas añadidas
		}//Fin por cada pelicula del top

		//Cuando hago click en una pelicula
		$(".contenedorFilmTopTres").on("click",function(){
			//Obtengo el id de ese film
			var idFilmTopTresPulsado = $(this).attr("class").split(" ")[1].split("idFilmTop")[1];			
			//Redirijo a la pagina del film:
			window.location.href="/film/"+idFilmTopTresPulsado;
		});//Fin al hacer click en una de las tres peliculas	

	});//Fin consulta para obtener las 3 películas peor valoradas

	//Creare un Top de 3 mejores películas por genero, aparecerá un genero de los siguientes aleatoriamente:
	var generos = ["Trhiller","Drama","Bélico","Mafia","Ciencia Ficción","Acción","Crimen","Basado en hechos reales","Policíaco","Intriga","Drama psicológico","Drogas"];
	var numeroAleatorio=Math.floor(Math.random()*generos.length);
	var tipoTopMostrar = generos[numeroAleatorio];

	//Inserto el nombre del tipo del top en el h3
	$(".variarTipoPelicula").html("Top 3 mejores del genero: "+tipoTopMostrar);

	//Consulta para obtener 3 mejores peliculas de un genero:
	$.post("../modelo/modelo.php",{topTresMejoresGenero:"true",genero:tipoTopMostrar},function(respuestaTopTresMejoresGenero){
		//console.log(respuestaTopTresMejoresGenero);
		var arrayTresMejoresGenero = JSON.parse(respuestaTopTresMejoresGenero);

		//Por cada pelicula del top
		for(var i=0;i<arrayTresMejoresGenero.length;i++){
			var idFilmTopTresGenero = arrayTresMejoresGenero[i][0];
			var notaFilmTopTresGenero = parseFloat(arrayTresMejoresGenero[i][1]).toFixed(1);
			var tituloFilmTopTresGenero = arrayTresMejoresGenero[i][2];
			var fotoFilmTopTresGenero = arrayTresMejoresGenero[i][3];


			//Añado las peliculas
			$("#topTresTipoPelicula").append(`
				<div class='contenedorFilmTopTres idFilmTop`+idFilmTopTresGenero+`'>
					<img class='fotoTopTres' src='`+fotoFilmTopTresGenero+`'/>
					<span class='tituloTopTres'><i>`+tituloFilmTopTresGenero+`</i></span>
					<span class='notaTopTres'><div class='colorNota'>`+notaFilmTopTresGenero+`</div></span>
				</div>`);//Fin añadir peliculas
		}//Fin por cada pelicula del top

		//Cuando hago click en una pelicula
		$(".contenedorFilmTopTres").on("click",function(){
			//Obtengo el id de ese film
			var idFilmTopTresPulsado = $(this).attr("class").split(" ")[1].split("idFilmTop")[1];			
			//Redirijo a la pagina del film:
			window.location.href="/film/"+idFilmTopTresPulsado;
		});//Fin al hacer click en una de las tres peliculas	

	})//Fin post para obtener 3 mejores peliculas por genero

	








});//FIN DOCUMENT READY
//Funcion que cierra la navbar (Cuando tiene el boton desplegar en pantallas pequeñas)
function cerrarNavbar(){ 
    $('.navbar-collapse').collapse('hide');
}//Fin funcion cerrar navbar