//Este es el controlador del fichero index.html público que se muestra como página incial.
$(document).ready(function(){
    scrollTop();
    $.ajaxSetup({cache:false});  
    $("#menuDesplegablePeliculaBuscadaIndex").html("");

    //Al cargar la pagina oculto los divs del index menos el div con las peliculas
    $("#divFormularioregistrarse").hide();
    $("#divFormulariologin").hide();
    $("#appPelicula").hide();
    $("#peliculaNoEncontrada").hide();


    //Al hacer click en "iniciarsesion" se muestra el formulario de inicio de sesion
    //y se oculta el resto de esta manera evito pantallas de carga
    $("#irainicio").on("click",function(){
        vaciarCamposTexto();
        cerrarNavbar();
        scrollTop();
        $("#peliculaNoEncontrada").hide();        
        $("#divFormulariologin").hide();
        $("#divFormularioregistrarse").hide();
        $("#contenidoPublico").show();
    })

    //Al hacer click en "iniciar sesion" se muestra el formulario de inicio de sesion
    //y se oculta el resto de esta manera evito pantallas de carga
    $("#iniciarsesion").on("click",function(){
        vaciarCamposTexto();
        cerrarNavbar();
        scrollTop();
        $("#contenidoPublico").hide();
        $("#peliculaNoEncontrada").hide();
        $("#divFormularioregistrarse").hide();
        $("#divFormulariologin").show();
    });

    //Al hacer click en "registrase" se muestra el formulario de inicio de sesion
    //y se oculta el resto de esta manera evito pantallas de carga
    $("#registrarse").on("click",function(){
        vaciarCamposTexto();
        cerrarNavbar();
        scrollTop();
        $("#peliculaNoEncontrada").hide();
        $("#contenidoPublico").hide();
        $("#divFormulariologin").hide();
        $("#divFormularioregistrarse").show();
    });

    /*EJEMPLO DE LLAMADA AL MODELO
        $.post("modelo/modelo.php",function(respuesta){
            console.log(respuesta);
        })
    */

    //Cuando se hace click en los campos input dentro de la pagina, la navbar se debe cerrar
    $("form input").on("click",function(){
        cerrarNavbar();
    }); 
   
   
    //Muestro 6 peliculas aleatorias:
    mostrarPeliculasAleatorias(6);


    //Cuando se hace click en el boton buscar(lupa) del buscador. 
    $("#botonBuscador").on("click",function(){
        $("#peliculaNoEncontrada").hide();
        cerrarNavbar();
        var textoBuscar = $("#campoTextoBuscador").val();
        //console.log(textoBuscar);

        //Solo realizo busquedas si se introduce algo:
        if(textoBuscar.length>0){
            //Realizo la busqueda de la pelicula por su titulo por titulo:
            buscarPorTituloSinRegistro(textoBuscar);
        }//Fin si se introduce algo
        //Vacio el campo de texto:
        $("#campoTextoBuscador").val("");
        //igualarStringsVue();
    });//Fin hacer click en el boton buscar (lupa)


    //Cuando se realiza una busqueda pulsando "Enter"
    $("#campoTextoBuscador").keyup(function(evento){
        //Pulsar Enter
        if(evento.which==13){
            $("#peliculaNoEncontrada").hide();           
            cerrarNavbar();
            var textoBuscar = $("#campoTextoBuscador").val();
            //console.log(textoBuscar);
    
            //Solo realizo busquedas si se introduce algo:
            if(textoBuscar.length>0){
                //Realizo la busqueda de la pelicula por su titulo por titulo: (Noto una diferencia, pulsando "Enter" se ven las variables Vue {{}})
                buscarPorTituloSinRegistro(textoBuscar);
            }//Fin si se introduce algo
            //Vacio el campo de texto:
            $("#campoTextoBuscador").val("");
            //igualarStringsVue();
        }//Fin pulsar Enter
        
    })//Fin cuando se realiza busqueda pulsando "Enter"





    //Cuando se hace click en el input del buscador
    $("#campoTextoBuscador").on("click",function(){
            
            //Lo vacío:$("#campoTextoBuscador").val("");     
            $(this).val("");
            
            //Oculto las sugerencias anteriores al pulsar en la caja de texto
            //$("#menuDesplegablePeliculaBuscadaIndex").hide();
            //Si pulso en cualquier sitio del body, oculto las sugerencias
            $("body").on("click",function(){
                $("#menuDesplegablePeliculaBuscadaIndex").hide();
                
            })

            //Cada vez que se pulsa una tecla debe ocurrir una sugerencia: 
            $("#campoTextoBuscador").keyup(function(){
                var nombrePeliculaBuscar =  $.trim($(this).val());
                //console.log(nombrePeliculaBuscar);

                //Si lo del campo de texto es menor a tres caracteres, oculto el campo.
                if(nombrePeliculaBuscar.length<2){
                    $("#menuDesplegablePeliculaBuscadaIndex").hide();
                }

                //Si lo del campo de texto es mayor o igual a tres caracteres realizo la busqueda de sugerencias
                if(nombrePeliculaBuscar.length>=2){

                    $.post("modelo/modelo.php",{nombrePeliculaSugerir:nombrePeliculaBuscar},function(peliculaSugeridaJSON){
                        //console.log(peliculaSugeridaJSON);
                        //Muestro el menu desplegable
                        $("#menuDesplegablePeliculaBuscadaIndex").show();
                        //Vacio el contenido del menu desplegable (Por si se realizó una busqueda previa)
                        $("#menuDesplegablePeliculaBuscadaIndex").html("");

                        //Creo un array de las peliculas sugeridas
                        var arrayPeliculasSugeridas = JSON.parse(peliculaSugeridaJSON);

                        //console.log(arrayPeliculasSugeridas);
                        //Si no hay coincidencias, se oculta el desplegable:
                        if(arrayPeliculasSugeridas.length==0){
                            $("#menuDesplegablePeliculaBuscadaIndex").hide();
                        }

                        //Por cada pelicula sugerida inserto un html en las sugerencias:
                        //Añado los titulos de las peliculas:
                        //<div class="dropdown-item"><img class="fotoBusqueda" src="fotos/thewire.jpg"/>The Wire</div>
                        for(var i = 0; i<arrayPeliculasSugeridas.length;i++){
                            $("#menuDesplegablePeliculaBuscadaIndex").append('<div class="dropdown-item id-'+arrayPeliculasSugeridas[i]["idfilm"]+'"><img class="fotoBusqueda" src="'+arrayPeliculasSugeridas[i]["fotoportada"]+'"/>'+arrayPeliculasSugeridas[i]["titulo"]+'</div><br>');
                        }   

                        //Por cada elemento de las sugerencias:
                        $("#menuDesplegablePeliculaBuscadaIndex div").each(function(){
                            //Cuando pulso en el elemento
                            $(this).click(function(){
                                
                                //Vacio el campo de texto delbuscador
                                $("#campoTextoBuscador").val("");
                                $("#divFormulariologin").hide();//Tuve que añadir esto al añadir la funcionalidad de registrarse para votar o comentar   
                                $("#divFormularioregistrarse").hide();//Tuve que añadir esto al añadir la funcionalidad de darle al enlace para registrarse si no tienes cuenta

                                //igualarStringsVue();//

                                $("#peliculaNoEncontrada").hide();
                                /*Mostrar un div con información de esa pelicula*/
                                cerrarNavbar();
                                scrollTop();            
                             
                                //Oculto y vacio el menu desplegable 
                                //$("#menuDesplegablePeliculaBuscadaIndex").show();//Estaba en show(), no se si me habia equivocado.
                                $("#menuDesplegablePeliculaBuscadaIndex").hide(); 
                                $("#menuDesplegablePeliculaBuscadaIndex").html("");

                                //console.log($(this).text());
                                var tituloSeleccionadoMenu = $(this).text();

                                //Realizo la busqueda de la pelicula por su titulo por titulo:
                                buscarPorTituloSinRegistro(tituloSeleccionadoMenu);

                                
                                

                                
                            });//Fin al pulsar elemento
                        });//Fin por cada elemento de las sugerencias:


                        
                        //$("#menuDesplegablePeliculaBuscadaIndex").append('<div class="dropdown-item">'+peliculaSugeridaJSON+'</div><br>');
                        
                    });//Fin busqueda de sugerencias
                }//Fin si el campo es mayor o igual a 3

                //Vacio el menuDesplegable;
                $("#menuDesplegablePeliculaBuscadaIndex").html("");
                //Vacio la variable del nombre:
                nombrePeliculaBuscar="";
            })//Fin sugerencias
        

    });//Fin click input buscador


 /*LOGIN------------------------------------------------------------------------------*/
    //LOGIN
    //Usando el boton:
    $("#botonLogin").on("click",function(){         
        iniciarSesion();
    });
    //Pulsando enter:
    $("#passwordLogin,#cuentaOEmailLogin").keyup(function(evento){
        if(evento.which==13){
            iniciarSesion();
        };
    });
    //FIN LOGIN
/*FIN LOGIN------------------------------------------------------------------------------*/



/*REGISTRO----------------------------------------------------------------------------*/
   
    //Si se pulsa en el ojo, ver contenido campos contraseña (En login):
    $(".ojopasswd1").on("click",function(){
        var campo3 = document.getElementById("passwordLogin");        
        if(campo3.type=="password"){

            campo3.type= "text";
        }else{   
            campo3.type="password";
        }
        
        if( $(".ojopasswd1").hasClass("fa-eye")){
            $(".ojopasswd1").removeClass("fa-eye");
            $(".ojopasswd1").addClass("fa-eye-slash");
        }else{
            $(".ojopasswd1").removeClass("fa-eye-slash");
            $(".ojopasswd1").addClass("fa-eye");
        }

    })//Fin si se pulsa en el ojo
    //Si se pulsa en el ojo, ver contenido campos contraseña (En registro):
    $(".ojopasswd2").on("click",function(){
        var campo1 = document.getElementById("passwordNuevoUsuario1");
        var campo2 = document.getElementById("passwordNuevoUsuario2");        
        if(campo1.type==="password"&&campo2.type==="password"){
            campo1.type= "text";
            campo2.type= "text";            
        }else{            
            campo1.type= "password";
            campo2.type= "password";   
        }
        
        if( $(".ojopasswd2").hasClass("fa-eye")){
            $(".ojopasswd2").removeClass("fa-eye");
            $(".ojopasswd2").addClass("fa-eye-slash");
        }else{
            $(".ojopasswd2").removeClass("fa-eye-slash");
            $(".ojopasswd2").addClass("fa-eye");
        }
    })//Fin si se pulsa en el ojo

    //BOTON REGISTRARSE:
    $("#botonRegistrarse").on("click",function(){
       registrarse();
    });//FIN BOTON REGISTRARSE

//COLORES DE LOS IMPUT DEL FORMULARIO:
    //CAMPO NOMBRE #############################################################################################################
    $("#nombreNuevoUsuario").keyup(function(){    
        
            var nombreIntroducido=$("#nombreNuevoUsuario").val();
            //Compruebo que el nombre no sea un email:
            if(esEmail(nombreIntroducido)!=true&&tieneNumerosString(nombreIntroducido)!=true){
                $("#nombreNuevoUsuario").removeClass("is-invalid");                
                $("#nombreNuevoUsuario").addClass("is-valid");
                //Si lo introducido en el campo esta vacio, elimino el color verde:
                if($.trim($(this).val()).length==0){
                    $("#nombreNuevoUsuario").removeClass("is-valid"); 
                }//Fin si no esta vacio

            }else{//Si es un email, lo pongo en rojo:
                $("#nombreNuevoUsuario").addClass("is-invalid");
            }//Fin si es un email
    })//FIN CAMPO NOMBRE
    //CAMPO APELLIDOS #############################################################################################################
    $("#apellidoNuevoUsuario").keyup(function(){
        var apellidoIntroducido = $("#apellidoNuevoUsuario").val();
        //Compruebo que el apellidos no sea un email:
        if(esEmail(apellidoIntroducido)!=true&&tieneNumerosString(apellidoIntroducido)!=true){
            $("#apellidoNuevoUsuario").removeClass("is-invalid"); 
            $("#apellidoNuevoUsuario").addClass("is-valid");
            //Si lo introducido en el campo esta vacio, elimino el color verde:
            if($.trim($(this).val()).length==0){
                $("#apellidoNuevoUsuario").removeClass("is-valid"); 
            }//Fin if
        }else{
            $("#apellidoNuevoUsuario").addClass("is-invalid");
        }//Fin si es un email      
    })//FIN CAMPO APELLIDOS

    //CAMPO NICK #############################################################################################################
    $("#nickNuevoUsuario").keyup(function(){
        
        //Lo introducido:
        var nickIntroducido = $.trim($(this).val());

        //Elimino la clase invalido
        $("#nickNuevoUsuario").removeClass("is-invalid"); 

        //Hago la peticion post aqui, asi no utilizo la funcion existeNickDB() ya que se tendria que usar de modo asincrono.
        $.post("modelo/modelo.php",{existeEsteNick:nickIntroducido},function(respuesta){            
            if(respuesta!=1&&esEmail(nickIntroducido)!=true&&nickIntroducido.length>0){//Existe
                $("#nickNuevoUsuario").addClass("is-valid"); 
            }else if(nickIntroducido.length==0){
                $("#nickNuevoUsuario").removeClass("is-valid"); 
                $("#nickNuevoUsuario").removeClass("is-invalid"); 
            }else{//No existe
                $("#nickNuevoUsuario").removeClass("is-valid"); 
                $("#nickNuevoUsuario").addClass("is-invalid"); 
            }//Fin 
        })//Fin post

/*
        //Si no existe en la base de datos y no es un email
        if(existeNickDB(nickIntroducido)==false&&esEmail(nickIntroducido)==false){ 
            $("#nickNuevoUsuario").addClass("is-valid"); 
        }else{
            $("#nickNuevoUsuario").removeClass("is-valid"); 
            $("#nickNuevoUsuario").addClass("is-invalid"); 
        }//Fin si no existe o no es email        
*/
    })//FIN CAMPO NICK

    //CAMPO EMAIL #############################################################################################################
    $("#emailNuevoUsuario").keyup(function(){
        //Lo introducido:
        var correoIntroducido = $.trim($(this).val());

        //Si es un email
        if(esEmail(correoIntroducido)==true){
            //Elimino la clase roja:
            $("#emailNuevoUsuario").removeClass("is-invalid");
            

            //Hago la peticion post aqui, asi no utilizo la funcion existeCorreoDB() ya que se tendria que usar de modo asincrono.
            $.post("modelo/modelo.php",{existeEsteCorreo:correoIntroducido},function(respuesta){

                if(respuesta!=1&&esEmail(correoIntroducido)==true&correoIntroducido.length>0){
                    //Añado clase verde
                    $("#emailNuevoUsuario").addClass("is-valid"); 
                }else if(correoIntroducido.length==0){
                    $("#emailNuevoUsuario").removeClass("is-valid"); 
                    $("#emailNuevoUsuario").removeClass("is-invalid"); 
                }else{
                    //Elimino clase verde
                    $("#emailNuevoUsuario").removeClass("is-valid"); 
                    //Añado clase roja
                    $("#emailNuevoUsuario").addClass("is-invalid");
                }
            });//Fin post para comprobar si existe el correo


        }else{//Si no es un email
            //Elimino clase verde
            $("#emailNuevoUsuario").removeClass("is-valid"); 
            //Añado clase roja
            $("#emailNuevoUsuario").removeClass("is-invalid"); 
        }//Fin si es un email        

    });//FIN CAMPO EMAIL

    //CAMPO PASSWORD 1 #############################################################################################################
    $("#passwordNuevoUsuario1").keyup(function(){

        //Si lo introducido es mayor a 8
        if($.trim($(this).val()).length>=8){
            //Añado clase verde
            $("#passwordNuevoUsuario1").addClass("is-valid");
        }else if($.trim($(this).val()).length<8){//Si es menor a 8
            //Elimino clase verde
            $("#passwordNuevoUsuario1").removeClass("is-valid");
        }
        

        //Dificultar la introduccion de ESPACIOS EN BLANCO:
        $("#passwordNuevoUsuario1").val($("#passwordNuevoUsuario1").val().trim());
        $("#passwordNuevoUsuario1").val($("#passwordNuevoUsuario1").val().replace(/ /g,""));
    });//FIN CAMPO PASSWORD 1   
    

    //CAMPO PASSWORD 2 #############################################################################################################
    $("#passwordNuevoUsuario2").keyup(function(){

        //Elimino clase roja
        $("#passwordNuevoUsuario2").removeClass("is-invalid");
        //Elimino clase verde
        $("#passwordNuevoUsuario2").removeClass("is-valid");
     
        //Si lo introducido es del mismo tamaño que el campo de contraseña1
        if($.trim($(this).val().length==$("#passwordNuevoUsuario1").val().length)){
            //Si lo introducido es exactamente igual que la contraseña1
            if($.trim($(this).val())===$("#passwordNuevoUsuario1").val()){
                //Añado clase verde:
                $("#passwordNuevoUsuario2").addClass("is-valid");
            }

        }else{//Si no elimino clase roja y clase verde (Esto puede dar conflictos, ya que es un poco lioso)
            $("#passwordNuevoUsuario2").removeClass("is-invalid");
            $("#passwordNuevoUsuario2").removeClass("is-valid");
        } 
       
        //Dificultar la introduccion de ESPACIOS EN BLANCO:
        $("#passwordNuevoUsuario2").val($("#passwordNuevoUsuario2").val().trim());        
        $("#passwordNuevoUsuario2").val($("#passwordNuevoUsuario2").val().replace(/ /g,""));
    });// FIN CAMPO PASSWORD 2


     //ELIMINAR ESPACIOS EN BLANCO (Esto sirve para que las contraseñas no contengan espacios)
     //Me aseguro de eliminar los espacios en blanco de los campos de contraseñas
     $("#passwordNuevoUsuario1").focusout(function(){

        var textoIntroducido = document.getElementById("passwordNuevoUsuario1").value;
        var arrayTexto = textoIntroducido.split(" ");
        var textoSinEspacios = arrayTexto.join("");
        $("#passwordNuevoUsuario1").val(textoSinEspacios);    
    })//Fin eliminar campos en blanco de los campos de contraseña
     //Me aseguro de eliminar los espacios en blanco de los campos de contraseñas
     $("#passwordNuevoUsuario2").focusout(function(){
        var textoIntroducido = document.getElementById("passwordNuevoUsuario2").value;
        var arrayTexto = textoIntroducido.split(" ");
        var textoSinEspacios = arrayTexto.join("");
        $("#passwordNuevoUsuario2").val(textoSinEspacios);    
    })//Fin eliminar campos en blanco de los campos de contraseña



//FIN COLORES FORMULARIO

    


/*FIN REGISTRO----------------------------------------------------------------------------*/





    //Texto que pregunta si no tienes cuenta:
    $(".textoSinCuenta").on("click",function(){
        vaciarCamposTexto();
        cerrarNavbar();
        scrollTop();
        $("#peliculaNoEncontrada").hide();
        $("#contenidoPublico").hide();
        $("#divFormulariologin").hide();
        $("#divFormularioregistrarse").show();
    })

    
    //Boton para volver a cargar peliculas
    $("#botonMostrarMasPublico").on("click",function(){
        //location.reload();//Creo que es mejor una consulta nueva.
        mostrarPeliculasAleatorias(3);          
    })//Fin boton recargar peliculas

   
    ///CONFIRMACION DE TERMINOS Y CONDICIONES

    //console.log("FUNCIONA"); 
    //Mensaje para aceptar terminos y condinciones de uso
    //console.log("Recarga: "+sessionStorage.getItem("terminos"));

    if(sessionStorage.getItem("terminos")==null){

        Swal.fire({
            
            title: "Su privacidad es importante",
            html: `
            <div style='text-align:left'>
                En FilmRate almacenamos o accedemos a información en un dispositivo, tales como cookies, y procesamos datos personales, tales como identificadores únicos e información estándar enviada por un dispositivo, para anuncios y contenido personalizados, medición de anuncios y del contenido e información sobre el público, así como para desarrollar y mejorar productos.
                <br>
                Con su permiso, nosotros podemos utilizar datos de localización geográfica precisa e identificación mediante las características de dispositivos. Puede hacer clic para otorgarnos su consentimiento para que llevemos a cabo el procesamiento previamente descrito. Tenga en cuenta que algún procesamiento de sus datos personales puede no requerir de su consentimiento, pero usted tiene el derecho de rechazar tal procesamiento. 
                Sus preferencias se aplicarán solo a este sitio web. Puede cambiar sus preferencias en cualquier momento entrando de nuevo en este sitio web o visitando nuestra política de privacidad.        
                <br><br><em><a href='https://www.boe.es/buscar/act.php?id=BOE-A-2018-16673'>Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales (LOPDPGDD)</em></a>
            </div>
            `,        
            showCancelButton:false,        
            confirmButtonColor: "#3342FF",
            confirmButtonText: "Acepto",       
            
            allowOutsideClick:false,
            allowEscapeKey:false,
            allowEnterKey:false,
            imageUrl:'../favicon.ico',
            width:'95%',
            padding:'25px',
        }).then((result) => {
                if(result){
                    //console.log("ACEPTADOS TERMINOS Y CONDICIONES");
                    sessionStorage.setItem("terminos",true);

                    //console.log(sessionStorage.getItem("terminos"));
                }
        });//Fin Mensaje para aceptar terminos y condinciones de uso     

    }
  

    //console.log(sessionStorage);
    //Siempre, en esta pagina, me aseguro de borrar el sessionStorage.    
    if(sessionStorage.getItem("terminos")!=null){
        sessionStorage.clear();
        sessionStorage.setItem("terminos",true);
    }else{
        sessionStorage.clear();
    }
    
    //FIN CONFIRMACION DE TERMINOS Y CONDICIONES

})//Fin document ready###############################################################################################################################################################################################################################

//Funcion para el boton de scroll hacia arriba (Con anmacion, usar en el boton para subir):
function scrollTop(){
    $("html, body").animate({scrollTop:0},'fast');
    //$("html,body").scrollTop(0);
}//Fin funcion scroll up


//Funcion para el boton de scroll hacia arriba inmediato (Sin animación, util para "cambios de páginas" desde el navbar):
function scrollTopFast(){
    $("html,body").scrollTop(0);
}//Fin funcion scroll up


//Funcion que cierra la navbar (Cuando tiene el boton desplegar en pantallas pequeñas)
function cerrarNavbar(){ 
    $('.navbar-collapse').collapse('hide');
}//Fin funcion cerrar navbar

function vaciarCamposTexto(){
    eliminarTicksVerdesRojos();
    $("#campoTextoBuscador").val("");
    $("input").val("");
}

//Funcion que pone los strings de Vue {{}} en su estado original
function igualarStringsVue(){
    $(".tituloPeliculaVue").text("{{tituloPelicula}}");
    $(".directorPeliculaVue").text("{{directorPelicula}}");
    $(".fechaPeliculaVue").text("{{fechaPelicula}}");
    $(".duracionPeliculaVue").text("{{duracionPelicula}}");
    $(".guionPeliculaVue").text("{{guionPelicula}}");
    $(".generoPeliculaVue").text("{{generoPelicula}}");
    $(".tipoPeliculaVue").text("{{tipoPelicula}}");
    $(".repartoPeliculaVue").text("{{repartoPelicula}}");
    $(".sinopsisPeliculaVue").text("{{sinopsisPelicula}}");
    $(".notaPeliculaAMostrarVue").text("{{notaPelicula}}");
    $(".numeroVotosAMostrarVue").html("{{numeroVotosPelicula}}<br>votos");
    
}//Fin Funcion que pone los strings de Vue {{}} en su estado original

//Funcion que se le pasa el titulo de una pelicula y realiza una consulta al modelo, luego la muestra en el div con Vue
function buscarPorTituloSinRegistro(titulo){
            
            //Realizo una consulta por título
            $.post("modelo/modelo.php",{busquedaPorTitulo:titulo},function(peliculaBuscadaJSON){   

                //console.log(peliculaBuscadaJSON);
                //console.log(PELICULA);
                var arrayPeliculaEncontrada = JSON.parse(peliculaBuscadaJSON);        
                //console.log( arrayPeliculaEncontrada[0]["titulo"]);
                
           
                  

                    //Si se encuentra la pelicula:
                    if(arrayPeliculaEncontrada["status"]!="error"){
                             //Consulta de la nota media de la pelicula seleccionada:
                            $.post("modelo/modelo.php",{obtenerNotaPeliculaPorId:arrayPeliculaEncontrada[0]["idfilm"]},function(notasPelicula){
                                //console.log(notaMediaPelicula);
                                
                                   //Obtengo el JSON con la nota media y el numero de votos
                                    var arrayNotasPelicula=JSON.parse(notasPelicula);

                                    //console.log(arrayNotasPelicula);

                                    var notaPelicula = arrayNotasPelicula[0].toFixed(1);
                                    var votosPelicula = arrayNotasPelicula[1];



                                $("#contenidoPublico").hide();
                                

                                //Aqui creo las variables que obtendran del JSON la informacion de la pelicula (Es lo que le pasare a vue)
                                tituloEncontrada = arrayPeliculaEncontrada[0]["titulo"];
                                directorEncontrada = arrayPeliculaEncontrada[0]["director"];
                                fechaEncontrada = arrayPeliculaEncontrada[0]["fechaestreno"];
                                fechaEncontrada=fechaEncontrada.split("-");
                                fechaEncontrada=fechaEncontrada[0];
                                duracionEncontrada = arrayPeliculaEncontrada[0]["duracion"];
                                guionEncontrada = arrayPeliculaEncontrada[0]["guion"];
                                generoEncontrada = arrayPeliculaEncontrada[0]["genero"];
                                tipoEncontrada = arrayPeliculaEncontrada[0]["tipo"];
                                repartoEncontrada = arrayPeliculaEncontrada[0]["reparto"];
                                sinopsisEncontrada = arrayPeliculaEncontrada[0]["sinopsis"];
                                idPaisEncontrada = arrayPeliculaEncontrada[0]["pais"];

                                portadaEncontrada = arrayPeliculaEncontrada[0]["fotoportada"];

                                //Le añado la foto
                                $(".peliculaAMostrar").attr("src",portadaEncontrada);


                                //Insertare la bandera del pais
                                //console.log(paisFilmEncontrado);
                                //Consulta al modelo para saber el pais del film:
                                $.post("modelo/modelo.php",{idPais:idPaisEncontrada},function(respuestaPais){                                               
                                    //console.log(respuestaPais);
                                    if(JSON.parse(respuestaPais)["status"]!="SINPAIS"){//Si se recibe un pais:
                                        $("#idfotopais").html("<img src='fotos/paises/"+JSON.parse(respuestaPais)[0][0]+"'/><span id='nombrePais'>"+JSON.parse(respuestaPais)[0][1]+"</span>");
                                    }else{//Si no se recibe informacion del pais, es decir, si no tenemos ese pais en la base de datos:
                                        //Oculto la informacion del pais de la pelicula
                                        $("#contenedorPais").hide();
                                    }
                                });//Fin consulta para saber el pais del film



                                //Utilizo Vue para mostrar información de la pelicula:
                                var appPelicula = new Vue({
                                    el:"#appPelicula",
                                    data:{
                                        tituloPelicula:tituloEncontrada,
                                        directorPelicula:directorEncontrada,
                                        fechaPelicula:fechaEncontrada,
                                        duracionPelicula:duracionEncontrada,
                                        guionPelicula:guionEncontrada,
                                        generoPelicula:generoEncontrada,
                                        tipoPelicula:tipoEncontrada,
                                        repartoPelicula:repartoEncontrada,
                                        sinopsisPelicula:sinopsisEncontrada,
                                        notaPelicula:notaPelicula,                                        
                                        numeroVotosPelicula:votosPelicula
                                    }
                                })//FIN VUE

                                //Muestro el div de VUE (Si hago esto antes de declarar las variables de Vue, por unos milisegundos se puede apreciar {{variables}} en el navegador)
                                $("#appPelicula").show();

                                //En este punto al darle a inicio:
                                $("#irainicio").on("click",function(){  
                                    //scrollTop();                  
                                    $("#appPelicula").hide();
                                    $("#peliculaNoEncontrada").hide();
                                    $("#contenidoPublico").show();
                                    igualarStringsVue();
                                })//Fin darle a inicio
                                //En este punto al darle a login:
                                $("#iniciarsesion").on("click",function(){                    
                                    //scrollTop();
                                    $("#appPelicula").hide();
                                    $("#peliculaNoEncontrada").hide();
                                    $("#iniciarsesion").show();
                                    igualarStringsVue();
                                })//Fin darle a inicio
                                //En este punto al darle a registrarse:
                                $("#registrarse").on("click",function(){                    
                                    //scrollTop();
                                    $("#appPelicula").hide();
                                    $("#peliculaNoEncontrada").hide();
                                    $("#registrarse").show();
                                    igualarStringsVue();
                                })//Fin darle a inicio

                                //Al darle al boton volver al contenido publico
                                $(".botonVolverContenidoPublico").on("click",function(){
                                    //Oculto las peliculas, y muestro informacion sobre la pelicula pulsada
                                    cerrarNavbar();
                                    //scrollTop();
                                    $("#peliculaNoEncontrada").hide();
                                    $("#contenidoPublico").show();
                                    $("#appPelicula").hide();

                                    //NO HAGO location.reload();
                                    //Vuelvo a igualar los strings de Vue (ESTO ES IMPORTANTE, EVITO HACER UN LOCATION RELOAD)                    
                                    igualarStringsVue();
                                });//Fin darle boton volver contenido publico              

                                //Cuando un usuario sin registrarse quiere votar la película o escribir una critica
                                $(".votarPeliculaPublica,.criticaPeliculaPublica").on("click",function(){
                                    scrollTopFast();
                                    //Oculto el resto de divs y muestro el de iniciar sesion
                                    $("#divFormularioregistrarse").hide();                   
                                    $("#appPelicula").hide();
                                    $("#peliculaNoEncontrada").hide();
                                    $("#divFormulariologin").show(); 
                                    
                                    Swal.fire({
                                        icon:'info',
                                        title:'Inicia Sesion',
                                        text:'Para valorar o hacer críticas debes iniciar sesion.'
                                    })
                                });//Fin cuando usuario sin registrarse quiere votar la pelicula

                                

                                //Llamo la funcion para que funcione el boton de scroll en la info de cada pelicula
                                $(".subir").on("click",function(){
                                    scrollTop();
                                });                                 
                        });//Fin consulta nota media 
                        igualarStringsVue();      
                }else{//Si no se encuentra la pelicula:
                            //METER AQUI MEJOR UN DIV QUE INDIQUE QUE NO SE HA ENCONTRADO LA BUSQUEDA:
                            console.log("NO SE HA ENCONTRADO PELICULA");
                            vaciarCamposTexto();
                            $("#contenidoPublico").hide();

                            //Oculto el resto de divs y muestro el de pelicula no encontrada
                            $("#divFormularioregistrarse").hide();
                            $("#divFormulariologin").hide();
                            $("#appPelicula").hide();
                            $("#peliculaNoEncontrada").show();
                            
                            $("#peliculaNoEncontrada").html("<h4><i class='fas fa-search'></i><br><br>No se ha encontrado la película con el título '"+titulo+"'<br><br><button type='button' class='btn btn-lg colorAzul botonVolverContenidoPublico'><i class='fas fa-arrow-left'></i></button><br><br></h4>");

                            //Al darle al boton volver al contenido publico
                            $(".botonVolverContenidoPublico").on("click",function(){
                                //Oculto las peliculas, y muestro informacion sobre la pelicula pulsada
                                cerrarNavbar();
                                scrollTop();
                                $("#peliculaNoEncontrada").hide();
                                $("#contenidoPublico").show();
                                $("#appPelicula").hide();

                                //NO HAGO location.reload();
                                //Vuelvo a igualar los strings de Vue (ESTO ES IMPORTANTE, EVITO HACER UN LOCATION RELOAD)                    
                                igualarStringsVue();
                            });//Fin darle boton volver contenido publico
                            
                }//Fin if

              
        });//Fin consulta por titulo
};//Fin funcion que se le pasa el titulo de una pelicula y realiza una consulta al modelo, luego la muestra en el div con Vue


//Funcion para iniciar sesion
function iniciarSesion(){
    var nickEmail = $("#cuentaOEmailLogin").val().toLowerCase();
    var password = $("#passwordLogin").val();

    //Si hay campos vacios:
    if(nickEmail.length==0||password.length==0){
        //Vacio todo:
        vaciarCamposTexto();
        //Muestro mensaje:
        Swal.fire({
            icon:'error',
            title: 'Oops...',
            text: 'No pueden haber campos vacios',                    
        });//Fin mensaje
    }else{
        //CONSULTA AL MODELO SOBRE EL USUARIO:
        $.post("modelo/modelo.php",{cuentaLogin:nickEmail,passwordLogin:password},function(respuesta){
            //console.log(respuesta);


            //SI EL LOGIN DA ERROR:
            if(respuesta=="ERRORLOGIN"){
                //Vacio todo:
                vaciarCamposTexto();
                //Muestro mensaje:
                Swal.fire({
                    icon:'error',
                    title: 'Oops...',
                    text: 'Usuario o contraseña erroneos',                    
                });//Fin mensaje
            }else if(respuesta=="CUENTASINACTIVAR"){//Si la cuenta está sin activar:
                //console.log("TENGO QUE ACTIVARLA");

                 Swal.fire({//Utilizo sweetalert2 para pedir el codigo con un input
                    title: 'Introduce el codigo de activación',
                    text: 'El código de activación se ha enviado al correo',
                    html:
                      '<input id="swal-input1" class="swal2-input">',                    
                    focusConfirm: false,
                    preConfirm: () => {
                        var codigo = document.getElementById('swal-input1').value;
                        //Hago una peticion al modelo para comprobar el codigo con el que hay en la base de datos:
                        $.post("modelo/modelo.php",{cuentaParaActivar:nickEmail,codigoActivacion:codigo},function(respuesta){
                                if(respuesta=="CODIGOACTIVACIONERRONEO"){
                                    //SI ELCODIGO DE ACTIVACION ES ERRONEO
                                    Swal.fire({
                                        icon:'error',
                                        title:'Codigo de activacion erroneo'
                                    })
                                }else{
                                    //Mensaje de activacion de la cuenta:
                                    Swal.fire({
                                        icon:'success',
                                        title:'Se ha activado la cuenta, ya puedes acceder.'
                                    })
                                }
                        });                      
                        
                        
                    }
                  })//Fin pedir codigo de activacion de cuenta                

            }else{
                
                respuesta = JSON.parse(respuesta);
                //console.log(respuesta);//["id","nick","email","nombre","apellidos","admin"]

                //Asigno en sessionStorage las variables para poder trabajar con los datos del usuario registrado desde javascript
                sessionStorage.Id=respuesta[0];
                sessionStorage.Nick=respuesta[1];
                sessionStorage.Email=respuesta[2];
                sessionStorage.Nombre=respuesta[3];
                sessionStorage.Apellidos=respuesta[4];
                sessionStorage.Admin=respuesta[5];
                
                //console.log(sessionStorage);

                //Si es administrador
                if(respuesta[5]=="1"&&sessionStorage.Admin=="1"){
                    //window.location.replace("/vistas/indexAdmin.php");
                    window.location.replace("/administracion");
                }else{
                    //window.location.replace("/vistas/indexUsuario.php");
                    window.location.replace("/principal");
                }                

                //Redirigir a la pagina correspondiente:
                //window.location.replace("/vistas/"+respuesta);
                //console.log(respuesta);
            }//Fin si el login da error
            
        });//FIN CONSULTA USUARIO
    }//Fin si no hay campos vacios

}//Fin funcion iniciar sesion



//Funcion para registrarse en la web
function registrarse(){
   $("#nombreNuevoUsuario").removeClass("is-invalid");
   $("#apellidoNuevoUsuario").removeClass("is-invalid");

   $("#nickNuevoUsuario").removeClass("is-invalid");
   $("#emailNuevoUsuario").removeClass("is-invalid");

   $("#passwordNuevoUsuario1").removeClass("is-invalid");
   $("#passwordNuevoUsuario2").removeClass("is-invalid");   


   //$("#passwordNuevoUsuario2").removeClass("is-valid");


    var nombreNuevo = $("#nombreNuevoUsuario").val();
    var apellidosNuevo = $("#apellidoNuevoUsuario").val();

    var nickNuevo = $("#nickNuevoUsuario").val().toLowerCase();
    var correoNuevo = $("#emailNuevoUsuario").val().toLowerCase();

    var passwordNuevoUno = $("#passwordNuevoUsuario1").val();
    var passwordNuevoDos = $("#passwordNuevoUsuario2").val();

    /*
    console.log(nombreNuevo);
    console.log(apellidosNuevo);
    console.log(nickNuevo);
    console.log(correoNuevo);
    console.log(passwordNuevoUno);
    console.log(passwordNuevoDos);
    */



    $.post("modelo/modelo.php",{existeEsteCorreo:correoNuevo},function(respuestaCorreo){

        $.post("modelo/modelo.php",{existeEsteNick:nickNuevo},function(respuestaNick){


        /*var camposLlenos = false;*/
        var compNom = false ;
        var compApe = false ;
        var compNick =false ;
        var compEmail =false;
        var compPasswd1 = false;
        var compPasswd2 = false;

                
            if(respuestaCorreo!=1 && respuestaNick!=1){

                //Comprobacion del NOMBRE#######################################################
                if(nombreNuevo.length>0&&esEmail(nombreNuevo)==false&&tieneNumerosString(nombreNuevo)==false){
                    compNom=true;
                }else if(nombreNuevo.length==0||esEmail(nombreNuevo)==true||tieneNumerosString(nombreNuevo)==true){
                    mensajeCamposVacios();
                    $("#nombreNuevoUsuario").addClass("is-invalid"); 
                    compNom=false
                }
                 //Comprobacion de los APELLIDOS#######################################################
                if(apellidosNuevo.length>0&&esEmail(apellidosNuevo)==false&&tieneNumerosString(apellidosNuevo)==false){
                    compApe=true;
                }else if(apellidosNuevo.length==0||esEmail(apellidosNuevo)==true||tieneNumerosString(apellidosNuevo)==true){
                    mensajeCamposVacios();
                    $("#apellidoNuevoUsuario").addClass("is-invalid"); 
                    compApe=false;
                }
                //Comprobacion del NICK#######################################################
                if(nickNuevo.length>0){
                    compNick=true;
                    if(esEmail(nickNuevo)){
                        $("#nickNuevoUsuario").val("");
                        Swal.fire({
                            icon:'error',
                            title: 'Oops...',
                            text: 'Nick ya registrado o erroneo',                    
                        });//Fin mensaje
                        $("#nickNuevoUsuario").removeClass("is-valid");
                        $("#nickNuevoUsuario").removeClass("is-invalid");
                        compNick = false;
                    }else{
                        compNick=true;
                    }

                }else if(nickNuevo.length==0){
                    mensajeCamposVacios();
                    $("#nickNuevoUsuario").addClass("is-invalid"); 
                    compNick=false;
                }

                //Comprobacion del EMAIL#######################################################
                if(correoNuevo.length>0){
                    compEmail=true;


                    if(esEmail(correoNuevo)!=true){
                        $("#emailNuevoUsuario").val("");
                        //$("#emailNuevoUsuario").focus();
                        Swal.fire({
                            icon:'error',
                            title: 'Oops...',
                            text: 'Email ya registrado o erroneo',                    
                        });//Fin mensaje            
                        $("#emailNuevoUsuario").removeClass("is-valid");          
                        $("#emailNuevoUsuario").removeClass("is-invalid");
                        compEmail=false;             
                    }else{
                        compEmail=true;
                    }

                }else if(correoNuevo.length==0){
                    mensajeCamposVacios();
                    $("#emailNuevoUsuario").addClass("is-invalid"); 
                    compEmail=false;
                }

                //Comprobacion de la CONTRAEÑA 1#######################################################
                if(passwordNuevoUno.length>0){
                    compPasswd1=true;

                    if(passwordNuevoUno.length<8){
                        $("#passwordNuevoUsuario1").val("");
            
                        Swal.fire({
                            icon:'error',
                            title: 'Oops...',
                            text: 'Las contraseñas deben de tener un mínimo de 8 caracteres.',                    
                        });//Fin mensaje

                        compPasswd1 = false;
                    }else{
                        compPasswd1 = true;
                    }
                }else if(passwordNuevoUno.length==0){
                    mensajeCamposVacios();
                    $("#passwordNuevoUsuario1").addClass("is-invalid"); 
                    compPasswd1=false;
                }


                //Comprobacion de la CONTRAEÑA 2#######################################################
                if(passwordNuevoDos.length>0){
                    compPasswd2=true;
                    if(passwordNuevoDos.length<8){
                        $("#passwordNuevoUsuario1").val("");
                
                        Swal.fire({
                            icon:'error',
                            title: 'Oops...',
                            text: 'Las contraseñas deben de tener un mínimo de 8 caracteres.',                    
                        });
                        compPasswd2 = false;
                    }else{
                        compPasswd2 = true;
                    }
                }else if(passwordNuevoDos.length==0){
                    mensajeCamposVacios();
                    $("#passwordNuevoUsuario2").addClass("is-invalid"); 
                    compPasswd2=false;
                }

                //Comprobacion de ambas CONTRAEÑAS#######################################################   
                if(passwordNuevoDos!==passwordNuevoUno){
                    $("#passwordNuevoUsuario1").val("");
                    $("#passwordNuevoUsuario2").val("");
                    Swal.fire({
                        icon:'error',
                        title: 'Oops...',
                        text: 'Las contraseñas deben de ser iguales.',                    
                    });//Fin mensaje
                    $("#passwordNuevoUsuario1").removeClass("is-valid");
                    $("#passwordNuevoUsuario2").removeClass("is-valid");
                    $("#passwordNuevoUsuario1").removeClass("is-invalid");
                    $("#passwordNuevoUsuario2").removeClass("is-invalid");        
                    compPasswd2 = false;
                }else{
                    compPasswd2 = true;
                } 



            }else if(respuestaCorreo==1){
                $("#emailNuevoUsuario").val(""); 
                Swal.fire({
                    icon:'error',
                    title: 'Oops...',
                    text: 'Email ya registrado o erroneo',                    
                });//Fin mensaje
                compEmail=false;
            }else if(respuestaNick==1){
                $("#nickNuevoUsuario").val("");
                Swal.fire({
                    icon:'error',
                    title: 'Oops...',
                    text: 'Nick ya registrado o erroneo',                    
                });//Fin mensaje
                compNick=false;
            }
            /*
            console.log("COMPROBACIONES");
            console.log("Nombre?:"+compNom);
            console.log("Apellido?:"+compApe);
            console.log("Nick?:"+compNick);
            console.log("Email?:"+compEmail);
            console.log("Password1?:"+compPasswd1);
            console.log("Password2?:"+compPasswd2);*/

            //Si todas las comprobaciones están bien, registro el usuario
            if(compNom==true && compApe ==true && compNick==true && compEmail==true && compPasswd1==true && compPasswd2==true){
                //console.log("REGISTRAR"); //HACER AQUI LLAMADA AL MODELO   
                
                /*console.log("REGISTRAR----------------");
                console.log("NOMBRE:"+nombreNuevo);
                console.log("APELLIDOS:"+apellidosNuevo);
                console.log("NICK:"+nickNuevo);
                console.log("EMAIL:"+correoNuevo);
                console.log("CONTRASEÑA:"+passwordNuevoUno);
                */

                //Envio los datos al modelo para que este los ingrese en la base de datos.
                $.post("modelo/modelo.php",
                {   registrarNombre:nombreNuevo,                
                    registrarApellidos:apellidosNuevo,
                    registrarNick:nickNuevo.toLowerCase(),
                    registrarEmail:correoNuevo,
                    registrarPassword:passwordNuevoUno
                },function(respuesta){
                    //console.log(respuesta);
                    if(respuesta=="USUARIOCREADO"){
                        //Muestro un mensaje
                        Swal.fire({
                            icon:'success',
                            title:'Se te ha enviado un correo, si no lo encuentras busca en spam.',
                            showConfirmButton: false,
                            timer:3000
                        })
                        //Redirigir a la página de login:
                        $("#peliculaNoEncotrada").hide();
                        $("#appPelicula").hide();
                        $("#contenidoPublico").hide();
                        $("#divFormularioregistrarse").hide();
                        $("#divFormulariologin").show();  
                    }
                });//Fin enviar datos al modelo para registrarlos


                



            }//Fin registrar usuario si todas las comprobaciones van bien
        });//Fin POST Comprobar si existe Nick
    });//Fin POST Comprobar si existe Correo



   //Funcion de mensaje para campos vacios:
   function mensajeCamposVacios(){
        Swal.fire({
            icon:'error',
            title: 'Oops...',
            text: 'Campos vacíos o erróneos',                    
        });//Fin mensaje
    }//Fin funcion para campos vacios


}//Fin funcion registrarse


//Funcion para comprobar si un string es email
function esEmail(correo){
    var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return reg.test(correo);
}//Fin comprobar correo


//Funcion que elimina todos los ticks verdes del formulario de registro:
function eliminarTicksVerdesRojos(){
    //Elimino los ticks verdes
    $("#nombreNuevoUsuario").removeClass("is-valid");
    $("#apellidoNuevoUsuario").removeClass("is-valid");
    $("#nickNuevoUsuario").removeClass("is-valid");
    $("#emailNuevoUsuario").removeClass("is-valid");
    $("#nickNuevoUsuario").removeClass("is-valid");
    $("#passwordNuevoUsuario1").removeClass("is-valid");
    $("#passwordNuevoUsuario2").removeClass("is-valid");

    //Elimino los ticks rojos
    $("#nombreNuevoUsuario").removeClass("is-invalid");
    $("#apellidoNuevoUsuario").removeClass("is-invalid");
    $("#nickNuevoUsuario").removeClass("is-invalid");
    $("#emailNuevoUsuario").removeClass("is-invalid");
    $("#nickNuevoUsuario").removeClass("is-invalid");
    $("#passwordNuevoUsuario1").removeClass("is-invalid");
    $("#passwordNuevoUsuario2").removeClass("is-invalid");
}//Fin funcioneliminar ticks verdes



//Funcion que muestra en el div de peliculas el numero de peliculas aleatorias que se le indique. (No lo repite de cada vez que se invoca a la funcion)
function mostrarPeliculasAleatorias(numeroPeliculas){

  //Variable para pasarle al modelo y que entre en el isset
  var consultaPeliculasPaginaPrincipal="true";

  /*Quiero pasarle el listado de peliculas para mostrarlas al publico*/
  $.post("modelo/modelo.php",{consultaPeliculasPaginaPrincipal:consultaPeliculasPaginaPrincipal},function(JSONPeliculas){

      //console.log(JSONPeliculas);

      //Paso el JSON a un array:
      var arrayPeliculas = JSON.parse(JSONPeliculas);
      
      /*Quiero mostrar 8 peliculas aleatorias en la pagina principal*/
      var numeroPeliculasMostrar = numeroPeliculas; //Este sera el numero de peliculas que mostrara la pagina principal

      //Creo un array de numeros con el numero de peliculas de la base de datos.
      var arrayCantidadPeliculas=[];
      for(var z=0;z<arrayPeliculas.length;z++){
          arrayCantidadPeliculas.push(z);            
      }        

      //Desordeno aleatoriamente ese array:
      arrayCantidadPeliculas = arrayCantidadPeliculas.sort(function(){return Math.random()-0.5});
      //console.log(arrayCantidadPeliculas);

      //Creo un array que almacenará 6 numeros aleatorios del array de numeros de peliculas:
      var arraySeisPeliculas=[];    
      for(var u=0;u<numeroPeliculasMostrar;u++){
          arraySeisPeliculas.push(arrayCantidadPeliculas[u]);
      }

      //console.log(arraySeisPeliculas);

      //Inserto las fotos en el div
      for(var i=0;i<numeroPeliculasMostrar;i++){
          //$("#titulosPeliculas").append("<img class='img-responsive fotoPortadaPublica' src='"+arrayPeliculas[arraySeisPeliculas[i]]["fotoportada"]+"'/>");            
          //$("#titulosPeliculas").append("<img class='fotoPortadaPublica' id='"+arrayPeliculas[arraySeisPeliculas[i]]["idfilm"]+"' src='"+arrayPeliculas[arraySeisPeliculas[i]]["fotoportada"]+"'/>");//ESTO ES COMO ANTES, SIN EL TITULO ENCIMA DE CADA PELICULA 
          $("#titulosPeliculas").append("<div id='fotoCompleta' class='contenedor tituloPortadaPublica'><img class='fotoPortadaPublica' id='"+arrayPeliculas[arraySeisPeliculas[i]]["idfilm"]+"' src='"+arrayPeliculas[arraySeisPeliculas[i]]["fotoportada"]+"'/><div class='centrado'>"+arrayPeliculas[arraySeisPeliculas[i]]["titulo"]+"</div></div>");            
      }//Fin insertar peliculas en div
    
      //console.log(arrayPeliculas);
      //Al hacer click en cada una de las fotos del index inicial:
      $(".fotoPortadaPublica, .tituloPortadaPublica").on("click",function(){
          /*Mostrar un div con información de esa pelicula*/
          cerrarNavbar();
          scrollTop();            
          $("#contenidoPublico").hide();
          
          //console.log($(this).children().attr("id"));
          
          //Lo siguiente lo hago ya que al añadir un div con el titulo de la pelicula, al hacer click en el div, tengo que buscar tambien esa pelicula por el id.
          if(this.id=="fotoCompleta"){
              idObtenerPeliculaPublica=$(this).children().attr("id");                
          }else{
              //TENGO QUE HACER UNA CONSULTA PARA OBTENER INFORMACION DE LA PELICULA SELECCIONADA
              idObtenerPeliculaPublica=this.id;
          }
        
          
          //Consulta de pelicula por id:
          $.post("modelo/modelo.php",{idPeliculaPublica:idObtenerPeliculaPublica},function(peliculaJSON){


             
              //Consulta de la nota media de la pelicula seleccionada:
              $.post("modelo/modelo.php",{obtenerNotaPeliculaPorId:idObtenerPeliculaPublica},function(notasPelicula){
                  
                  //Obtengo el JSON con la nota media y el numero de votos
                  var arrayNotasPelicula=JSON.parse(notasPelicula);

                  //console.log(arrayNotasPelicula);

                  var notaPelicula = arrayNotasPelicula[0].toFixed(1);
                  var votosPelicula = arrayNotasPelicula[1];

                  var titulo="";
                  //console.log(PELICULA);
                  var arrayPELICULA = JSON.parse(peliculaJSON);
              
                  //console.log(arrayPELICULA[0]["titulo"])
                  //Aqui creo las variables que obtendran del JSON la informacion de la pelicula (Es lo que le pasare a vue)
                  titulo = arrayPELICULA[0]["titulo"];
                  director = arrayPELICULA[0]["director"];
                  fecha = arrayPELICULA[0]["fechaestreno"];
                  fecha=fecha.split("-");
                  fecha=fecha[0];
                  duracion = arrayPELICULA[0]["duracion"];
                  guion = arrayPELICULA[0]["guion"];
                  genero = arrayPELICULA[0]["genero"];
                  tipo = arrayPELICULA[0]["tipo"];
                  reparto = arrayPELICULA[0]["reparto"];
                  sinopsis = arrayPELICULA[0]["sinopsis"];

                  portada = arrayPELICULA[0]["fotoportada"];
                  idpais = arrayPELICULA[0]["pais"];

                  //Le añado la foto
                  $(".peliculaAMostrar").attr("src",portada);
                  

                //Insertare la bandera del pais
                //console.log(paisFilmEncontrado);
                //Consulta al modelo para saber el pais del film:
                $.post("modelo/modelo.php",{idPais:idpais},function(respuestaPais){                                               
                    //console.log(respuestaPais);
                    if(JSON.parse(respuestaPais)["status"]!="SINPAIS"){//Si se recibe un pais:
                        $("#idfotopais").html("<img src='fotos/paises/"+JSON.parse(respuestaPais)[0][0]+"'/><span id='nombrePais'>"+JSON.parse(respuestaPais)[0][1]+"</span>");
                    }else{//Si no se recibe informacion del pais, es decir, si no tenemos ese pais en la base de datos:
                        //Oculto la informacion del pais de la pelicula
                        $("#contenedorPais").hide();
                    }
                });//Fin consulta para saber el pais del film

              

              //Utilizo Vue para mostrar información de la pelicula:
              var appPelicula = new Vue({
                      el:"#appPelicula",
                      data:{
                          tituloPelicula:titulo,
                          directorPelicula:director,
                          fechaPelicula:fecha,
                          duracionPelicula:duracion,
                          guionPelicula:guion,
                          generoPelicula:genero,
                          tipoPelicula:tipo,
                          repartoPelicula:reparto,
                          sinopsisPelicula:sinopsis,
                          notaPelicula:notaPelicula,
                          numeroVotosPelicula:votosPelicula
                  
                      }
                  })//FIN VUE

                  //Muestro el div de VUE (Si hago esto antes de declarar las variables de Vue, por unos milisegundos se puede apreciar {{variables}} en el navegador)
                  $("#appPelicula").show();


                  //En este punto al darle a inicio:
                  $("#irainicio").on("click",function(){  
                      scrollTop();             
                      $("#peliculaNoEncontrada").hide();     
                      $("#appPelicula").hide();
                      $("#contenidoPublico").show();
                      igualarStringsVue();
                  })//Fin darle a inicio
                  //En este punto al darle a login:
                  $("#iniciarsesion").on("click",function(){                    
                      scrollTop();
                      $("#peliculaNoEncontrada").hide();
                      $("#appPelicula").hide();
                      $("#iniciarsesion").show();
                      igualarStringsVue();
                  })//Fin darle a inicio
                  //En este punto al darle a registrarse:
                  $("#registrarse").on("click",function(){                    
                      scrollTop();
                      $("#peliculaNoEncontrada").hide();
                      $("#appPelicula").hide();
                      $("#registrarse").show();
                      igualarStringsVue();
                  })//Fin darle a inicio


                  //Cuando un usuario sin registrarse quiere votar la película o escribir una critica
                  $(".votarPeliculaPublica,.criticaPeliculaPublica").on("click",function(){
                      scrollTopFast();
                      //Oculto el resto de divs y muestro el de iniciar sesion
                      $("#divFormularioregistrarse").hide();                   
                      $("#appPelicula").hide();
                      $("#peliculaNoEncontrada").hide();
                      $("#divFormulariologin").show();  
                      Swal.fire({
                          icon:'info',
                          title:'Inicia Sesion',
                          text:'Para valorar o hacer críticas debes iniciar sesion.'
                      })
                  });//Fin cuando usuario sin registrarse quiere votar la pelicula
  
                  
                  

                  //Al darle al boton volver al contenido publico
                  $(".botonVolverContenidoPublico").on("click",function(){
                      //Oculto las peliculas, y muestro informacion sobre la pelicula pulsada
                      cerrarNavbar();
                      scrollTop();
                      $("#peliculaNoEncontrada").hide();
                      $("#contenidoPublico").show();
                      $("#appPelicula").hide();

                      //NO HAGO location.reload();
                      //Vuelvo a igualar los strings de Vue (ESTO ES IMPORTANTE, EVITO HACER UN LOCATION RELOAD)                    
                      igualarStringsVue();
                  });//Fin darle boton volver contenido publico              

                  //Llamo la funcion para que funcione el boton de scroll en la info de cada pelicula
                  $(".subir").on("click",function(){
                      scrollTop();
                  });             
              });//FIN POST BUSCAR NOTA DE PELICULA POR ID
          });//FIN POST BUSCAR PELICULA POR ID INFORMACION PELICULA PULSADA
      });//Fin al hacer click en una foto

      //Llamo la funcion para que funcione el boton de scroll en el index principal
      $(".subir").on("click",function(){
      scrollTop();
      });
      

  }); //Fin Post listado de peliculas   


}//Fin funcion que muestra las peliculas en la pagina principal


//Funcion que dice si un string tiene numeros devuelve true or false
function tieneNumerosString(string){   
    if(
        string.includes("1")==true ||
        string.includes("2")==true ||
        string.includes("3")==true ||
        string.includes("4")==true ||
        string.includes("5")==true ||
        string.includes("6")==true ||
        string.includes("7")==true ||
        string.includes("8")==true ||
        string.includes("9")==true 
    ){
        return true;
    }else{
        return false;
    }

}//Fin funcion que dice si un string tiene numeros