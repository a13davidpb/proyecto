$(document).ready(function(){
    
    //console.log(sessionStorage);

	//Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);

	//Cuando se hace click en el boton de Crear Usuario
	$(".botonCrearUsuario").on("click",function(){
		
		//Debo recoger los datos del formulario
		var nick = $("#inputNick").val();
		var email = $("#inputEmail").val();
		var password = $("#inputPassword").val();
		var nombre = $("#inputNombre").val();
		var apellidos = $("#inputApellidos").val();

		//CONTROLAR LA INSERCION DE SCRIPTS:
		regex = new RegExp(/<.*?>/g);
		rex1 = regex.test(nick);
		rex2 = regex.test(email);
		rex3 = regex.test(password);
		rex4 = regex.test(nombre);
		rex5 = regex.test(apellidos);

		//Si se intenta insertar codigo:
		if(rex1==true||rex2==true||rex3==true||rex4==true||rex5==true){
			Swal.fire({
				icon:'error',
				title:'ERROR',
				showConfirmButton:false,
				timer:1500
			})
		}else{//Si no se inserta codigo:



				var tipoUsuario = $("input:radio[name=tipoUsuario]:checked").val();

				var cuentaActivada = $("input:checkbox[name=activada]:checked").val();
				if(cuentaActivada == undefined){
					cuentaActivada="0";
				}

				var testearEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

				//DEBO COMPROBAR QUE NO HAY CAMPOS VACIOS
				if(nick.length==0||email.length==0||password.length==0||nombre.length==0||apellidos.length==0){
					//Muestro un mensaje de sweetalert:                                            
					Swal.fire({
						title:'No puede haber campos vacíos',
						icon:'error',
						showConfirmButton:false,
						timer:1500
					});
				}else if(testearEmail.test(email)!=true){
					//Muestro un mensaje de sweetalert:                                            
					Swal.fire({
						title:'El texto introducido en el campo Email no es valido',
						icon:'error',
						showConfirmButton:false,
						timer:1500
					});
				}else{//SI NO HAY CAMPOS VACIOS
					//Consulta al modelo para que el administrador cree un usuario
					$.post("../modelo/modelo.php",{crearUsuarioAdmin:"true",crearNick:nick,crearEmail:email,crearPassword:password,crearNombre:nombre,crearApellidos:apellidos,tipoUsuario:tipoUsuario,cuentaActivada:cuentaActivada,idAdmin:sessionStorage.Id},function(respuestaCrearUsuarioAdmin){

						//console.log(respuestaCrearUsuarioAdmin);
						if(respuestaCrearUsuarioAdmin=="NICKEXISTENTE"){//SI EL NICK YA EXISTE:
						//Muestro un mensaje de sweetalert:                                            
							Swal.fire({
								title:'El nick ya existe',
								icon:'error',
								showConfirmButton:false,
								timer:1500
							});
							$("#inputNick").val("");
							$("#inputNick").focus();
						}else if(respuestaCrearUsuarioAdmin=="EMAILEXISTENTE"){//SI EL EMAIL YA EXISTE:
							//Muestro un mensaje de sweetalert:                                            
							Swal.fire({
								title:'El email ya existe',
								icon:'error',
								showConfirmButton:false,
								timer:1500
							});
							$("#inputEmail").val("");
							$("#inputEmail").focus();
						}else if(respuestaCrearUsuarioAdmin=="USUARIOCREADO"){//SE CREA EL USUARIO CORRECTAMENTE:
							//Muestro un mensaje de sweetalert:                                            
							Swal.fire({
								title:'Usuario creado correctamente.',
								icon:'success',
								showConfirmButton:false,
								timer:1500
							});
							$("#inputNick").val("");
							$("#inputEmail").val("");
							$("#inputPassword").val("");
							$("#inputNombre").val("");
							$("#inputApellidos").val("");

						}


					});//Fin consulta admin crear usuario


				}//FIN COMPROBACION CAMPOS VACIOS

				
		}//Fin comprobacion isercion de scrips


		
		

	});//Fin boton Crear Usuario


});//Fin document ready
