$(document).ready(function(){
    //console.log(sessionStorage);

    
    //Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);


//Obtener de la url el id de la pelicula actual
var nickBuscarInfo = obtenerIdURL();
//console.log(idBuscarInfo);
//console.log(nickBuscarInfo);

//Si el nick no esta definido en la url
if(nickBuscarInfo==undefined){
    //La informacion del usuario a buscar será la del usuario que estrá iniciado
    nickBuscarInfo=sessionStorage.Nick;        
}
/*
//Buscador por nombre de usuario
$("#botonBuscarEstadisticas").on("click",function(){

    //Obtengo el texto del input
    var textoNickBuscar =$("#textoBuscarEstadisticas").val();

    if(textoNickBuscar.length>0){
        window.location.href="/estadisticas/"+textoNickBuscar;
    }
    

})//Fin buscador por nombre de usuario*/




//NECESITO CONSULTAR EL ID DEL NICK
$.post("../modelo/modelo.php",{nickUsuarioSaberId:nickBuscarInfo},function(respuestaIdUsuario){
    //console.log(respuestaIdUsuario);
    if(respuestaIdUsuario.length>0&&respuestaIdUsuario!="ERROR"){//Si el nick que se busca esta permitido por el servidor, y no es una cuenta de administrador:
            
        idBuscarInfo=respuestaIdUsuario;  


        //########################################################### GRAFICA 1 ###############################################################################################

        //Necesito saber las ultimas 10 notas de los usuarios
        $.post("../modelo/modelo.php",{saberDiezUltimasNotasUsuarios:"true",idUsuarioIgnorar:idBuscarInfo},function(respuestaDiezUltimasNotasUsuarios){
                
                //console.log(respuestaDiezUltimasNotasUsuarios);
                var arrayObjetosNotasUsuarios = JSON.parse(respuestaDiezUltimasNotasUsuarios);
                //console.log(arrayObjetosNotasUsuarios);
                var arrayDiezNotasUsuarios = [];//Este sera el array de notas que mostraré en la gráfica
                for(var i=0; i<arrayObjetosNotasUsuarios.length;i++){
                    arrayDiezNotasUsuarios.push(arrayObjetosNotasUsuarios[i][0]);
                }
                //console.log(arrayDiezNotasUsuarios);




                //Necesito saber las ultimas 10 notas del usuario que tiene la sesion iniciada
                //Necesito saber las ultimas 10 notas de los usuarios
                $.post("../modelo/modelo.php",{saberDiezUltimasNotasUsuario:"true",idUsuarioDiezNotas:idBuscarInfo},function(respuestaDiezUltimasNotasUsuario){
                            //console.log(respuestaDiezUltimasNotasUsuario);
                            //console.log(respuestaDiezUltimasNotasUsuario);                       

                                    var arrayObjetosMisNotas = JSON.parse(respuestaDiezUltimasNotasUsuario);
                                    //console.log(arrayObjetosMisNotas);
                                    var arrayDiezNotas = [];//Este sera el array de notas que mostraré en la gráfica
                                    for(var i=0; i<arrayObjetosMisNotas.length;i++){
                                        arrayDiezNotas.push(arrayObjetosMisNotas[i][0]);
                                    }
                                    //console.log(arrayDiezNotas);
                                    //console.log(arrayDiezNotas.reverse());

                                    //En este punto tengo dos arrays, el de las notas de todos los usuarios y el de las notas del usuario, pero tengo que darles la vuelta:
                                    arrayDiezNotasUsuarios = arrayDiezNotasUsuarios.reverse();
                                    arrayDiezNotas = arrayDiezNotas.reverse();    


                                    //INSERTO LA GRAFICA---------------------------------------------------------------------              
                                    var a = new Chart(document.getElementById("chartUltimosDiezVotosUsuarios"), {
                                        
                                        type: 'bar',

                                        
                                        data: {
                                        labels: ["1º", "2º", "3º", "4º","5º","6º","7º","8º","9º","10º"],
                                        //labels: ["", "", "", "","","","","","",""],
                                        datasets: [{
                                            label: "Votos de usuarios",
                                            
                                            type: "line",
                                            borderColor: "#8e5ea2",
                                            data: arrayDiezNotasUsuarios,
                                            fill: false
                                            }, {
                                            label: "Tus votos",
                                            type: "line",
                                            borderColor: "#3e95cd",
                                            data: arrayDiezNotas,
                                            fill: false
                                            }
                                        ]
                                        },
                                        options: {
                                            maintainAspectRatio:false,//Esto es para poder redimensionarla con estilos
                                            title: {
                                                display: true,
                                                //text: 'Ultimos 10 votos'
                                            },
                                            legend: { display: false },//Esto es para que se vea la informacion
                                            scales: {
                                                    yAxes: [{                        
                                                        ticks: {
                                                            suggestedMin: 1,
                                                            suggestedMax: 10,
                                                            stepSize:1,
                                                        
                                                        }
                                                    }]
                                            },
                                        
                                        }
                                    });
                                    //FIN GRAFICA----------------------------------------------------------------------------        

                        
                });//Fin post saber ultimas 10 notas usuario


        })//Fin post saber las ultimas 10 notas de los usuarios



        //########################################################### FIN GRAFICA 1 ###############################################################################################
        //########################################################### GRAFICA 2 ###############################################################################################



        
        



        //Necesito una consulta que me devuelva un array de notas medias por genero
        $.post("../modelo/modelo.php",{notaMediaPorGenero:"true"},function(respuestaNotaMediaGeneral){
            //console.log(JSON.parse(respuestaNotaMediaGeneral));
            var arrayNotasMediasGenerales = JSON.parse(respuestaNotaMediaGeneral);
        
        
        
            
        
            //NECESITO UNA CONSULTA QUE OBTENGA LAS NOTAS MEDIAS DEL USUARIO POR GENERO 
            $.post("../modelo/modelo.php",{notaMediaPorGeneroUsuario:"true",idUsuarioNotaMediaPorGenero:idBuscarInfo},function(respuestaNotaMediaUsuarioPorGenero){
                //console.log(respuestaNotaMediaUsuarioPorGenero);
                var arrayNotasMediasUsuario = JSON.parse(respuestaNotaMediaUsuarioPorGenero);

            
                
                var arrayColores=["#3e95cd", "#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd"];            
                /*
                for(var i=0;i<arrayNotasMediasUsuario.length;i++){
                    if(arrayNotasMediasUsuario[i]==0){  
                        arrayNotasMediasUsuario[i]=5;
                        arrayColores[i]="#ffffff00";
                    }
                }*/

                            //Inserto la grafica
                            new Chart(document.getElementById("chartNotaMediaPorGenero"), {
                                type: 'bar',
                                data: {
                                labels: ["Thriller", "Drama", "Bélico", "Mafia", "C.Ficción","Acción"],
                                datasets: [
                                    {
                                    //label: "Population (millions)",
                                    //backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                                    backgroundColor: arrayColores,
                                    //data: [5.49,5,6.5,9,2,7],
                                    //label: "Population (millions)",
                                    //backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                                    data: arrayNotasMediasUsuario            
                                    },
                        
                                    {
                                        //label: "Africa",
                                        backgroundColor: ["#8e5ea2", "#8e5ea2","#8e5ea2","#8e5ea2","#8e5ea2","#8e5ea2"],
                                        data: arrayNotasMediasGenerales,
                                    }, 
                                ]
                                },
                                
                                options: {
                                    maintainAspectRatio:false,//Esto es para poder redimensionarla con estilos
                                legend: { display: false },
                                title: {
                                    display: true,
                                    //text: 'Predicted world population (millions) in 2050'
                                }, scales: {
                                    yAxes: [{                        
                                        ticks: {
                                            suggestedMin: 1,
                                            suggestedMax: 10,
                                            stepSize:1,
                                        
                                        }
                                    }]
                            },
                                }
                            });//Fin grafica



            });//Fin consulta que obtiene las notas medias del usuario por genero   
        });//Fin consulta que devuelve array de notas medias por genero

        //########################################################### FIN GRAFICA 2 ###############################################################################################
        //########################################################### GRAFICA 3 ###############################################################################################
       
        

        //Necesito una consulta, para obtener todos los paises y sus abrevituras (codigo de pais):
        $.post("../modelo/modelo.php",{saberCodigosNombresPaises:"true"},function(respuestaCodigosNombresPaises){
                //console.log(respuestaCodigosNombresPaises);
                var arrayObjetosCodigosNombresPaises = JSON.parse(respuestaCodigosNombresPaises);
                //console.log(arrayObjetosCodigosNombresPaises);

                //Creo un array mas simple
                var arraySimple = [];
                for(var i=0;i<arrayObjetosCodigosNombresPaises.length;i++){
                    //console.log(arrayObjetosCodigosNombresPaises[i][0]);                
                    arraySimple[arrayObjetosCodigosNombresPaises[i][0]] = arrayObjetosCodigosNombresPaises[i][1];
                }
                //De esta manera puedo imprimir el nombre de un pais asi:
                //console.log(arraySimple["US"]); //Devuelve -> Estados Unidos
                //console.log(arraySimple);
                
                //Necesito una consulta que me devuelva la cantidad de países por genero:
                $.post("../modelo/modelo.php",{saberCantidadPeliculasPorPais:"true"},function(respuestaCantidadPeliculasPorPais){

                        //console.log(respuestaCantidadPeliculasPorPais);
                        var arrayCantidadPeliculasPorPais = JSON.parse(respuestaCantidadPeliculasPorPais);
                        //console.log(arrayCantidadPeliculasPorPais);

                        //Creo un objeto mas simple cuya clave es el codigo del pais
                        var objetoSimplePaisesCantidad = [];
                        var paisesPeliculas = [];
                        var cantidadPeliculas = [];
                        var arrayColores = [];
                        for(var z=0;z<4;z++){
                            objetoSimplePaisesCantidad.push(new Array(arraySimple[arrayCantidadPeliculasPorPais[z][1]]+","+arrayCantidadPeliculasPorPais[z][0]));                               
                            objetoSimplePaisesCantidad[z]=objetoSimplePaisesCantidad[z][0].split(",");

                            paisesPeliculas.push(arraySimple[arrayCantidadPeliculasPorPais[z][1]]);
                           
                            cantidadPeliculas.push(arrayCantidadPeliculasPorPais[z][0]);

                            var randomColor = "#"+Math.floor(Math.random()*16777215).toString(16);

                            arrayColores.push(randomColor);

                        }

                        //console.log(objetoSimplePaisesCantidad);
                        //console.log(paisesPeliculas);
                        //console.log(cantidadPeliculas);                        
                        //console.log(arrayColores);




                        //IMPRIMIR GRAFICA
                        new Chart(document.getElementById("chartCantidadPeliculasPaises"), {
                            type: 'doughnut',
                            data: {
                              labels:paisesPeliculas,
                              datasets: [{
                                //label: "Population (millions)",
                                backgroundColor: ["#3e95cd","#8e5ea2","#3ecd76","#db7f17"],
                                data: cantidadPeliculas
                              }]
                            },
                            options: {
                                maintainAspectRatio:false,//Esto es para poder redimensionarla con estilos
                                legend: { display: true },
                              title: {
                                display: true,
                                //text: 'Predicted world population (millions) in 2050'
                              }
                            }
                        });



                        //FIN IMPRIMIR GRAFICA



                })//Fin consulta que devuelve la cantidad de paises por genero:

        })//Fin obtener paises y abreviaturas
        //########################################################### FIN GRAFICA 3 ###############################################################################################
        //###########################################################  GRAFICA 4 ###############################################################################################




        //Necesito una consulta para saber la nota media general de los usuarios
        $.post("../modelo/modelo.php",{notaMediaGeneralFilmRate:"true"},function(respuestaNotasMediasGenerales){
            //console.log(respuestaNotasMediasGenerales);
            var mediaGeneral = parseFloat(respuestaNotasMediasGenerales).toFixed(3);
            //console.log(mediaGeneral);

            //Necesito una consulta para saber la nota media general del usuario
            $.post("../modelo/modelo.php",{nickUsuarioMediaGeneral:nickBuscarInfo},function(respuestaNotasMediasGenerales){

                var mediaUsuario = parseFloat(respuestaNotasMediasGenerales).toFixed(3);
                //console.log(mediaUsuario);


                //IMPRIMIR GRAFICA

                new Chart(document.getElementById("chartNotaMediaGeneral"), {
                    type: 'horizontalBar',
                    data: {
                      labels: ["", ""],
                      datasets: [
                        {
                          //label: "Population (millions)",
                          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                          data: [mediaUsuario,mediaGeneral]
                        }
                      ]
                    },
                    options: {
                        maintainAspectRatio:false,//Esto es para poder redimensionarla con estilos
                      legend: { display: false },
                      title: {
                        display: false,
                        //text: 'Predicted world population (millions) in 2050'
                      },scales: {
                        xAxes: [{                        
                            ticks: {
                                suggestedMin: 1,
                                suggestedMax: 10,
                                stepSize:1,
                            
                            }
                        }]
                },
                    }
                });
                




                //FIN IMPRIMIR GRAFICA

            });//Fin consulta nota media general del usuario
        })//Fin consulta notas medias generales
        //########################################################### FIN GRAFICA 4 ###############################################################################################
        //########################################################### GRAFICA 5 ###############################################################################################






        //Obtener los 5 usuarios y el numero de votos que tienen:
        $.post("../modelo/modelo.php",{obtenerCincoUsuariosConMasVotos:"true"},function(respuestaCincoUsuariosConMasVotos){
                //console.log(respuestaCincoUsuariosConMasVotos);
                var objetoRespuesta = JSON.parse(respuestaCincoUsuariosConMasVotos);
                //console.log(objetoRespuesta);

                var arrayNicks = [];
                for(var i=0;i<objetoRespuesta.length;i++){
                    arrayNicks.push(objetoRespuesta[i][2]);
                }
                var arrayCantidadVotos = [];
                for(var i=0;i<objetoRespuesta.length;i++){
                    arrayCantidadVotos.push(objetoRespuesta[i][1]);
                }

                //console.log(arrayNicks);
                //console.log(arrayCantidadVotos);

                //Inserto en cada span de colores, el nick del usuario
                for(var u=0;u<arrayNicks.length;u++){
                    //$(".us"+u).html("<i class='fas fa-user-alt'></i> "+arrayNicks[u]);
                    $(".us"+u).html(arrayNicks[u]);
                    $(".us"+u).show();
                }//Fin insertar el nick del usuario

                //Al hacer click en un usuario ver su perfil:
                $(".usuarioMasVotos").on("click",function(){

                    var nickClick = $(this).html().toLowerCase();

                    window.location.href="/perfil/"+nickClick;

                })//Fin hacer click en un usuario

              
                // Bar chart
                new Chart(document.getElementById("chartCincoUsuariosConMasVotos"), {
                    type: 'bar',
                    data: {
                    //labels: arrayNicks,
                    labels: ["","","","",""],
                    datasets: [
                        {
                        //label: "Population (millions)",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#dd973b","#c45850"],
                        data: arrayCantidadVotos
                        }
                    ]
                    },
                    options: {
                        maintainAspectRatio:false,//Esto es para poder redimensionarla con estilos
                    legend: { display: false },
                    title: {
                        display: false,
                    // text: 'Predicted world population (millions) in 2050'
                    },scales: {
                        yAxes: [{                        
                            ticks: {
                                suggestedMin:1,
                                stepSize:10,                            
                            }
                        }]
                },
                    }
                });





        });//Fin obtener los 5 usuarios y el numero de votos que tienen:



        //########################################################### FIN GRAFICA 5 ###############################################################################################



        //Inserto en la informacion de las estadisticas el nick del usuario:
        $(".tusVotos").html(nickBuscarInfo[0].toUpperCase()+nickBuscarInfo.substring(1));

        //Si se hace click en el nombre de usuario, se va al perfil
        $(".tusVotos").on("click",function(){

            var nombreUsuarioIrPerfil = $(".tusVotos").html().toLowerCase();

            window.location.href="/perfil/"+nombreUsuarioIrPerfil;

        });//Fin click nombre de usuario



    }else if(respuestaIdUsuario=="ERROR"){//Si el nick que se busca no esta permitido por el servidor, y no es una cuenta de administrador:
        //Voy a las estadisticas de mi usuario
        window.location.replace("/estadisticas/"+sessionStorage.Nick);
    }else{//Si se intenta poner en la URL el nick de un usuario que no existe:
        //Voy a las estadisticas de mi usuario
        window.location.replace("/estadisticas/"+sessionStorage.Nick);
    }

});//FIN CONSULTAR EL ID DEL NICK QUE SE VAN A MOSTRAR LAS ESTADISTICAS

});//FIN DOCUMENT READY
//Funcion para obtener de la url el parametro id:
function obtenerIdURL(){
    var stringUrl =window.location.href;
    var arrayUrl = stringUrl.split("/");
    var parametros =arrayUrl[arrayUrl.length-1]; 
    //console.log(parametros);
    return parametros;
};//Fin funcion obtener de la url el parametro;