//Este es el controlador del fichero index para los usuarios que se han logeado.
$(document).ready(function(){

	//console.log(sessionStorage);	

	//Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);

    
    //Peticion para obtener todas las peliculas votadas por el usuario
    $.post("../modelo/modelo.php",{obtenerVotosUsuario:"true",idUsuarioVotos:sessionStorage.Id},function(respuestaVotosUsuario){

        var arrayVotosUsuario = JSON.parse(respuestaVotosUsuario);
        //console.log(arrayVotosUsuario);

        if(arrayVotosUsuario.length>0){
            //Tengo que recorrer el array para imprimir el contenido
            for(var i=0;i<arrayVotosUsuario.length;i++){

                cadaIdFilmVoto = arrayVotosUsuario[i][0];
                cadaNotaFilmVoto = arrayVotosUsuario[i][1];
                cadaTituloFilmVoto = arrayVotosUsuario[i][2];
                cadaFotoFilmVoto = arrayVotosUsuario[i][3];    

                //Añado las peliculas
                $("#listadoCompletoVotos").append(`
                <div class='contenedorVotosUsuario idFilmVotoUsuario`+cadaIdFilmVoto+`'>
                    <img class='fotoVotosUsuario' src='`+cadaFotoFilmVoto+`'/>
                    <span class='tituloVotosUsuario'><i>`+cadaTituloFilmVoto+`</i></span>
                    <span class='notaVotosUsuario'><div class='colorVotosUsuario'>`+cadaNotaFilmVoto+`</div></span>
                </div>`
                );//Fin peliculas añadidas

            }//Fin for recorrer array para imprimir contenido
            
        }else{

            //Añado las peliculas al div
            $("#listadoCompletoVotos").append(`<div>No ha votado ningún film.</div>`);
            $("#ordenarVotacionesNombreDESC").hide();
            $("#ordenarVotacionesNotaDESC").hide();
        }

        $(".contenedorVotosUsuario").on("click",function(){
            //Obtengo el id de ese film
            var idFilmVotoPulsado = $(this).attr("class").split(" ")[1].split("idFilmVotoUsuario")[1];			
            //Redirijo a la pagina del film:
            window.location.href="/film/"+idFilmVotoPulsado;
        });


      


    });//Fin peticion para obterner todas las peliculas votadas por el usuario



    //Al hacer click en el boton "Ordenar por nombre ASC"
    $("#ordenarVotacionesNombreDESC").on("click",function(){     
        //Para ordenar o desordenar   
        if($("#ordenarVotacionesNombreDESC").text()=="Ordenar por nombre DESC"){

                //Cuando se pulsa el boton borro el contenido:
                $("#listadoCompletoVotos div").remove();
                //Cambio el texto del boton:
                $("#ordenarVotacionesNombreDESC").text("Ordenar por nombre ASC");

                //Vuelvo a hacer la consulta:
                //Peticion para obtener todas las peliculas votadas por el usuario de forma DESCENDENTE
                $.post("../modelo/modelo.php",{obtenerVotosDESCUsuario:"true",idUsuarioVotos:sessionStorage.Id},function(respuestaVotosDESCUsuario){

                    var arrayVotosDESCUsuario = JSON.parse(respuestaVotosDESCUsuario);
                            
                    //Tengo que recorrer el array para imprimir el contenido
                    for(var i=0;i<arrayVotosDESCUsuario.length;i++){

                        cadaIdFilmVoto = arrayVotosDESCUsuario[i][0];
                        cadaNotaFilmVoto = arrayVotosDESCUsuario[i][1];
                        cadaTituloFilmVoto = arrayVotosDESCUsuario[i][2];
                        cadaFotoFilmVoto = arrayVotosDESCUsuario[i][3];    

                        //Añado las peliculas
                        $("#listadoCompletoVotos").append(`
                        <div class='contenedorVotosUsuario idFilmVotoUsuario`+cadaIdFilmVoto+`'>
                            <img class='fotoVotosUsuario' src='`+cadaFotoFilmVoto+`'/>
                            <span class='tituloVotosUsuario'><i>`+cadaTituloFilmVoto+`</i></span>
                            <span class='notaVotosUsuario'><div class='colorVotosUsuario'>`+cadaNotaFilmVoto+`</div></span>
                        </div>`
                        );//Fin peliculas añadidas

                    }//Fin for recorrer array para imprimir contenido

                    $(".contenedorVotosUsuario").on("click",function(){
                        //Obtengo el id de ese film
                        var idFilmVotoPulsado = $(this).attr("class").split(" ")[1].split("idFilmVotoUsuario")[1];			
                        //Redirijo a la pagina del film:
                        window.location.href="/film/"+idFilmVotoPulsado;
                    });


                });//Fin peticion para obtener todas las peliculas votadas por el usuario de forma DESCENDENTE

        }else{
                //Cuando se pulsa el boton borro el contenido:
                $("#listadoCompletoVotos div").remove();
                //Cambio el texto del boton:
                $("#ordenarVotacionesNombreDESC").text("Ordenar por nombre DESC");
                //Peticion para obtener todas las peliculas votadas por el usuario
                $.post("../modelo/modelo.php",{obtenerVotosUsuario:"true",idUsuarioVotos:sessionStorage.Id},function(respuestaVotosUsuario){

                    var arrayVotosUsuario = JSON.parse(respuestaVotosUsuario);
                    //console.log(arrayVotosUsuario);


                    //Tengo que recorrer el array para imprimir el contenido
                    for(var i=0;i<arrayVotosUsuario.length;i++){

                        cadaIdFilmVoto = arrayVotosUsuario[i][0];
                        cadaNotaFilmVoto = arrayVotosUsuario[i][1];
                        cadaTituloFilmVoto = arrayVotosUsuario[i][2];
                        cadaFotoFilmVoto = arrayVotosUsuario[i][3];    

                        //Añado las peliculas
                        $("#listadoCompletoVotos").append(`
                        <div class='contenedorVotosUsuario idFilmVotoUsuario`+cadaIdFilmVoto+`'>
                            <img class='fotoVotosUsuario' src='`+cadaFotoFilmVoto+`'/>
                            <span class='tituloVotosUsuario'><i>`+cadaTituloFilmVoto+`</i></span>
                            <span class='notaVotosUsuario'><div class='colorVotosUsuario'>`+cadaNotaFilmVoto+`</div></span>
                        </div>`
                        );//Fin peliculas añadidas

                    }//Fin for recorrer array para imprimir contenido

                    $(".contenedorVotosUsuario").on("click",function(){
                        //Obtengo el id de ese film
                        var idFilmVotoPulsado = $(this).attr("class").split(" ")[1].split("idFilmVotoUsuario")[1];			
                        //Redirijo a la pagina del film:
                        window.location.href="/film/"+idFilmVotoPulsado;
                    });

                });//Fin peticion para obterner todas las peliculas votadas por el usuario

        }
    });//Fin al hacer click en el boton "Ordenar por nombre DESC"

    //Al hacer click en el boton "Ordenar por nota DESC"
    $("#ordenarVotacionesNotaDESC").on("click",function(){
        //Para ordenar o desordenar   
        if($("#ordenarVotacionesNotaDESC").text()=="Ordenar por nota DESC"){

                //Cuando se pulsa el boton borro el contenido:
                $("#listadoCompletoVotos div").remove();
                //Cambio el texto del boton:
                $("#ordenarVotacionesNotaDESC").text("Ordenar por nota ASC");


                //Vuelvo a hacer la consulta:
                //Peticion para obtener todas las peliculas votadas por el usuario ordenadas de forma DESCENDENTE por nota
                $.post("../modelo/modelo.php",{obtenerVotosNotaDESCUsuario:"true",idUsuarioVotos:sessionStorage.Id},function(respuestaVotosNotaDESCUsuario){

                    var arrayVotosNotaDESCUsuario = JSON.parse(respuestaVotosNotaDESCUsuario);
                            
                    //Tengo que recorrer el array para imprimir el contenido
                    for(var i=0;i<arrayVotosNotaDESCUsuario.length;i++){

                        cadaIdFilmVoto = arrayVotosNotaDESCUsuario[i][0];
                        cadaNotaFilmVoto = arrayVotosNotaDESCUsuario[i][1];
                        cadaTituloFilmVoto = arrayVotosNotaDESCUsuario[i][2];
                        cadaFotoFilmVoto = arrayVotosNotaDESCUsuario[i][3];    

                        //Añado las peliculas
                        $("#listadoCompletoVotos").append(`
                        <div class='contenedorVotosUsuario idFilmVotoUsuario`+cadaIdFilmVoto+`'>
                            <img class='fotoVotosUsuario' src='`+cadaFotoFilmVoto+`'/>
                            <span class='tituloVotosUsuario'><i>`+cadaTituloFilmVoto+`</i></span>
                            <span class='notaVotosUsuario'><div class='colorVotosUsuario'>`+cadaNotaFilmVoto+`</div></span>
                        </div>`
                        );//Fin peliculas añadidas

                    }//Fin for recorrer array para imprimir contenido

                    $(".contenedorVotosUsuario").on("click",function(){
                        //Obtengo el id de ese film
                        var idFilmVotoPulsado = $(this).attr("class").split(" ")[1].split("idFilmVotoUsuario")[1];			
                        //Redirijo a la pagina del film:
                        window.location.href="/film/"+idFilmVotoPulsado;
                    });

                });//Fin peticion para obtener todas las peliculas votadas por el usuario ordenadas de forma DESCENDENTE por nota

        }else{
             //Cuando se pulsa el boton borro el contenido:
             $("#listadoCompletoVotos div").remove();
             //Cambio el texto del boton:
             $("#ordenarVotacionesNotaDESC").text("Ordenar por nota DESC");

              //Vuelvo a hacer la consulta:
                //Peticion para obtener todas las peliculas votadas por el usuario ordenadas de forma DESCENDENTE por nota
                $.post("../modelo/modelo.php",{obtenerVotosNotaASCUsuario:"true",idUsuarioVotos:sessionStorage.Id},function(respuestaVotosNotaASCUsuario){

                    var arrayVotosNotaASCUsuario = JSON.parse(respuestaVotosNotaASCUsuario);
                            
                    //Tengo que recorrer el array para imprimir el contenido
                    for(var i=0;i<arrayVotosNotaASCUsuario.length;i++){

                        cadaIdFilmVoto = arrayVotosNotaASCUsuario[i][0];
                        cadaNotaFilmVoto = arrayVotosNotaASCUsuario[i][1];
                        cadaTituloFilmVoto = arrayVotosNotaASCUsuario[i][2];
                        cadaFotoFilmVoto = arrayVotosNotaASCUsuario[i][3];    

                        //Añado las peliculas
                        $("#listadoCompletoVotos").append(`
                        <div class='contenedorVotosUsuario idFilmVotoUsuario`+cadaIdFilmVoto+`'>
                            <img class='fotoVotosUsuario' src='`+cadaFotoFilmVoto+`'/>
                            <span class='tituloVotosUsuario'><i>`+cadaTituloFilmVoto+`</i></span>
                            <span class='notaVotosUsuario'><div class='colorVotosUsuario'>`+cadaNotaFilmVoto+`</div></span>
                        </div>`
                        );//Fin peliculas añadidas

                    }//Fin for recorrer array para imprimir contenido

                    $(".contenedorVotosUsuario").on("click",function(){
                        //Obtengo el id de ese film
                        var idFilmVotoPulsado = $(this).attr("class").split(" ")[1].split("idFilmVotoUsuario")[1];			
                        //Redirijo a la pagina del film:
                        window.location.href="/film/"+idFilmVotoPulsado;
                    });

                });//Fin peticion para obtener todas las peliculas votadas por el usuario ordenadas de forma DESCENDENTE por nota



        }

    });//Fin al hacer click en el boton "Oordenar por nota DESC"






});