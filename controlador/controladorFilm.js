$(document).ready(function(){
    ////console.log(sessionStorage);
    
    //Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);



    //Obtener de la url el id de la pelicula actual
    var idBuscarInfo = obtenerIdURL();
    //console.log(idBuscarInfo);
    ////console.log(idBuscarInfo);
    if(isNaN(parseInt(idBuscarInfo))){
        idBuscarInfo = -1;
    };


    //Si se intenta acceder solo a "film.php" sin pasarle ningun parámetro
    if(idBuscarInfo==undefined){
        $("#infoFilm").html("<h4><i class='fas fa-exclamation-triangle'></i> A esta página solo se puede acceder si se ha realizado la busqueda de una película <i class='fas fa-exclamation-triangle'></i></h4>")
    }else{
        


        //Tengo que comprobar si el id introducido existe o no:
        $.post("../modelo/modelo.php",{idBuscarInfoExiste:idBuscarInfo},function(numeroFilmsEncontradosJSON){
            var arrayNumeroFilms = JSON.parse(numeroFilmsEncontradosJSON);
            var numeroFilmsEncontrados = arrayNumeroFilms[0][0];
            
            //Si se ha encontrado una película con el ID que aparece en la URL
            if(numeroFilmsEncontrados==1){

                        //tengo que hacer una consulta al modelo para obtener toda la información de la pelicula a partir del id
                        $.post("../modelo/modelo.php",{idBuscarInfo:idBuscarInfo},function(infoFilmJSON){
                            var arrayInfoFilm = JSON.parse(infoFilmJSON);
                            var objetoFilm = arrayInfoFilm[0];
                            ////console.log("INFORMACION DE LA PELICULA");
                            ////console.log(objetoFilm);

                            //Información del film:
                            var idFilmEncontrado = objetoFilm["idfilm"];
                            var tituloFilmEncontrado = objetoFilm["titulo"];
                            var fechaFilmEncontrado = objetoFilm["fechaestreno"].split("-")[0];
                            var paisFilmEncontrado = objetoFilm["pais"];
                            var directorFilmEncontrado = objetoFilm["director"];
                            var generoFilmEncontrado = objetoFilm["genero"];
                            var sinopsisFilmEncontrado = objetoFilm["sinopsis"];
                            var tipoFilmEncontrado = objetoFilm["tipo"];
                            var fotoFilmEncontrado = objetoFilm["fotoportada"];
                            var duracionFilmEncontrado = objetoFilm["duracion"];
                            var guionFilmEncontrado = objetoFilm["guion"];
                            var repartoFilmEncontrado = objetoFilm["reparto"];


                            /*
                            //Consulta para obtener todos los votos de una pelicula            
                            $.post("../modelo/modelo.php",{idFilmVotos:idFilmEncontrado},function(votosJSON){
                                var arrayVotos =JSON.parse(votosJSON);
                                var objetoVotos = arrayVotos;
                                //console.log("TODOS LOS VOTOS DE LA PELICULA");
                                //console.log(objetoVotos);
                            })//Fin consulta para obtener todos los votos de una pelicula
                            */


                            //CONSULTAS SOBRE INFORMACION DEL USUARIO LOGUEADO EN EL FILM------------------------------------------------------------------------------------
                            //Tengo que buscar los votos del usuario actual en esa pelicula
                            $.post("../modelo/modelo.php",{idFilmVotoUsuario:idFilmEncontrado,idUsuarioVotos:sessionStorage.Id},function(votoUsuarioJSON){

                                var arrayVotoUsuarioFilm = JSON.parse(votoUsuarioJSON);
                                var objetoVotoUsuario = arrayVotoUsuarioFilm[0];
                                ////console.log("INFORMACION DEL VOTO DEL USUARIO");
                                ////console.log(objetoVotoUsuario);

                                //Hacer una consulta para ver as criticas del usuario actual en esta pelicula
                                $.post("../modelo/modelo.php",{idFilmCriticaUsuario:idFilmEncontrado,idUsuarioCriticas:sessionStorage.Id},function(criticaUsuarioJSON){
                                    
                                    var arrayCriticaUsuarioFilm = JSON.parse(criticaUsuarioJSON);
                                    var objetoCriticaUsuario = arrayCriticaUsuarioFilm[0];
                                    ////console.log("INFORMACION DE LA CRITICA DEL USUARIO");
                                    ////console.log(objetoCriticaUsuario);      
                                    


                                    //Consulta para obtener la nota media de una pelicula y el numero de votos
                                    $.post("../modelo/modelo.php",{obtenerNotaPeliculaPorId:idFilmEncontrado},function(arrayNotaMediaVotosJSON){
                                        var arrayNotaMediaVotos = JSON.parse(arrayNotaMediaVotosJSON);
                                        ////console.log("NOTA MEDIA Y VOTOS DE LA PELICULA");
                                        ////console.log(arrayNotaMediaVotos);



                                        //Consulta para obtener todas las criticas de una pelicula (PERO NO LA DEL USUARIO ACTUAL)
                                        $.post("../modelo/modelo.php",{idFilmCriticas:idFilmEncontrado,idUsuarioActualNo:sessionStorage.Id},function(criticasJSON){
                                            var arrayCriticas = JSON.parse(criticasJSON);
                                            var objetoCriticas = arrayCriticas;//ESTO SERAN VARIAS CRITICAS DE MUCHOS USUARIOS, ASI QUE NO SOLO PUEDO OBTENER LA PRIMERA CON [0]
                                            ////console.log("TODAS LAS CRITICAS DE LA PELICULA");
                                            ////console.log(objetoCriticas);

                                            //Si la pelicula no tiene criticas, oculto el titulo de las criticas
                                            if(objetoCriticas.length==0){
                                                $("#tituloCriticas").hide();
                                            }//Fin ocultar titulo criticas

                                            //Si la pelicula tiene alguna crítica
                                            if(objetoCriticas.length>=1){

                                            

                                                if(objetoCriticas.length<4){
                                                    var numeroCriticasMostrar = objetoCriticas.length;
                                                }else{
                                                    //Solo mostraré 4 criticas
                                                    var numeroCriticasMostrar = 4;
                                                }

                                                //Creo un array con el numero de criticas total de la pelicula
                                                var arrayCantidadCriticas = [];
                                                for(var z=0;z<objetoCriticas.length;z++){
                                                    arrayCantidadCriticas.push(z);
                                                }
                                                //Desordeno ese array
                                                arrayCantidadCriticas = arrayCantidadCriticas.sort(function(){return Math.random()-0.5})

                                                
                                                ////console.log(arrayCantidadCriticas);
                                                arrayCriticasMostrar=[];//Array que contiene las criticas que se mostraran

                                                //Creo un array con 4 criticas aleatorias
                                                for(var i = 0;i<numeroCriticasMostrar;i++){                                                
                                                    arrayCriticasMostrar.push(objetoCriticas[arrayCantidadCriticas[i]]);
                                                }//Fin array 4 criticas aleatorias

                                                ////console.log("CRITICAS QUE SE DEBEN MOSTRAR");
                                                ////console.log(arrayCriticasMostrar);

                                                

                                                var arrayIdsCriticasMostrar = [];
                                                var arrayTextosCriticasMostrar =[];
                                                var arrayHorasCriticasMostrar =[];
                                                for(var i=0;i<arrayCriticasMostrar.length;i++){
                                                    arrayIdsCriticasMostrar.push(arrayCriticasMostrar[i][0]);
                                                    arrayTextosCriticasMostrar.push(arrayCriticasMostrar[i][2]);
                                                    arrayHorasCriticasMostrar.push(formatearFechaHora(arrayCriticasMostrar[i][3]));
                                                }
                                                //Fin array ids
                                                

                                                ////console.log(arrayIdsCriticasMostrar);
                                                ////console.log(arrayTextosCriticasMostrar);
                                                ////console.log(arrayHorasCriticasMostrar);

                                                //TENGO QUE IMPRIMIR LAS CRITICAS QUE TIENE LA PELICULA
                                                //Antes intente hacer esto con un for, y no imprimía bien las criticas, en un futuro intentaré refactorizar esto:
                                                if(arrayCriticasMostrar.length==4){                                                   
                                                    $.post("../modelo/modelo.php",{idCadaUsuarioCriticaFilm:arrayIdsCriticasMostrar[0]},function(respuesta){
                                                        ////console.log(JSON.parse(respuesta)[0][0]+" - "+arrayTextosCriticasMostrar[0]+" - "+arrayHorasCriticasMostrar[0]);
                                                        $("#criticasDeUsuarios").append("<div class='contenedorCritica'><strong class='nicksUsuariosCriticas'><i class='fas fa-user iconosUsuariosCriticas'></i>"+JSON.parse(respuesta)[0][0]+"</strong><br><div class='textoCritica'><i class='criticaUsuarios'>"+'"'+arrayTextosCriticasMostrar[0]+'"'+"</i></div><div class='fechaCriticasUsuarios'>"+arrayHorasCriticasMostrar[0]+"</div></div><hr>");                                                       
                                                        clickEnNickIrPerfil();
                                                    });
                                                    $.post("../modelo/modelo.php",{idCadaUsuarioCriticaFilm:arrayIdsCriticasMostrar[1]},function(respuesta){
                                                        ////console.log(JSON.parse(respuesta)[0][0]+" - "+arrayTextosCriticasMostrar[1]+" - "+arrayHorasCriticasMostrar[1]);
                                                        $("#criticasDeUsuarios").append("<div class='contenedorCritica'><strong class='nicksUsuariosCriticas'><i class='fas fa-user iconosUsuariosCriticas'></i>"+JSON.parse(respuesta)[0][0]+"</strong><br><div class='textoCritica'><i class='criticaUsuarios'>"+'"'+arrayTextosCriticasMostrar[1]+'"'+"</i></div><div class='fechaCriticasUsuarios'>"+arrayHorasCriticasMostrar[1]+"</div></div><hr>");
                                                        clickEnNickIrPerfil();
                                                    });
                                                    $.post("../modelo/modelo.php",{idCadaUsuarioCriticaFilm:arrayIdsCriticasMostrar[2]},function(respuesta){
                                                        ////console.log(JSON.parse(respuesta)[0][0]+" - "+arrayTextosCriticasMostrar[2]+" - "+arrayHorasCriticasMostrar[2]);
                                                        $("#criticasDeUsuarios").append("<div class='contenedorCritica'><strong class='nicksUsuariosCriticas'><i class='fas fa-user iconosUsuariosCriticas'></i>"+JSON.parse(respuesta)[0][0]+"</strong><br><div class='textoCritica'><i class='criticaUsuarios'>"+'"'+arrayTextosCriticasMostrar[2]+'"'+"</i></div><div class='fechaCriticasUsuarios'>"+arrayHorasCriticasMostrar[2]+"</div></div><hr>");
                                                        clickEnNickIrPerfil();
                                                    });
                                                    $.post("../modelo/modelo.php",{idCadaUsuarioCriticaFilm:arrayIdsCriticasMostrar[3]},function(respuesta){
                                                        ////console.log(JSON.parse(respuesta)[0][0]+" - "+arrayTextosCriticasMostrar[3]+" - "+arrayHorasCriticasMostrar[3]);
                                                        $("#criticasDeUsuarios").append("<div class='contenedorCritica'><strong class='nicksUsuariosCriticas'><i class='fas fa-user iconosUsuariosCriticas'></i>"+JSON.parse(respuesta)[0][0]+"</strong><br><div class='textoCritica'><i class='criticaUsuarios'>"+'"'+arrayTextosCriticasMostrar[3]+'"'+"</i></div><div class='fechaCriticasUsuarios'>"+arrayHorasCriticasMostrar[3]+"</div></div><hr>");
                                                        clickEnNickIrPerfil();
                                                    });
                                                }else if(arrayCriticasMostrar.length==3){
                                                    $.post("../modelo/modelo.php",{idCadaUsuarioCriticaFilm:arrayIdsCriticasMostrar[0]},function(respuesta){
                                                        ////console.log(JSON.parse(respuesta)[0][0]+" - "+arrayTextosCriticasMostrar[0]+" - "+arrayHorasCriticasMostrar[0]);
                                                        $("#criticasDeUsuarios").append("<div class='contenedorCritica'><strong class='nicksUsuariosCriticas'><i class='fas fa-user iconosUsuariosCriticas'></i>"+JSON.parse(respuesta)[0][0]+"</strong><br><div class='textoCritica'><i class='criticaUsuarios'>"+'"'+arrayTextosCriticasMostrar[0]+'"'+"</i></div><div class='fechaCriticasUsuarios'>"+arrayHorasCriticasMostrar[0]+"</div></div><hr>");                                                       
                                                        clickEnNickIrPerfil();
                                                    });
                                                    $.post("../modelo/modelo.php",{idCadaUsuarioCriticaFilm:arrayIdsCriticasMostrar[1]},function(respuesta){
                                                        ////console.log(JSON.parse(respuesta)[0][0]+" - "+arrayTextosCriticasMostrar[1]+" - "+arrayHorasCriticasMostrar[1]);
                                                        $("#criticasDeUsuarios").append("<div class='contenedorCritica'><strong class='nicksUsuariosCriticas'><i class='fas fa-user iconosUsuariosCriticas'></i>"+JSON.parse(respuesta)[0][0]+"</strong><br><div class='textoCritica'><i class='criticaUsuarios'>"+'"'+arrayTextosCriticasMostrar[1]+'"'+"</i></div><div class='fechaCriticasUsuarios'>"+arrayHorasCriticasMostrar[1]+"</div></div><hr>");
                                                        clickEnNickIrPerfil();
                                                    });
                                                    $.post("../modelo/modelo.php",{idCadaUsuarioCriticaFilm:arrayIdsCriticasMostrar[2]},function(respuesta){
                                                        ////console.log(JSON.parse(respuesta)[0][0]+" - "+arrayTextosCriticasMostrar[2]+" - "+arrayHorasCriticasMostrar[2]);
                                                        $("#criticasDeUsuarios").append("<div class='contenedorCritica'><strong class='nicksUsuariosCriticas'><i class='fas fa-user iconosUsuariosCriticas'></i>"+JSON.parse(respuesta)[0][0]+"</strong><br><div class='textoCritica'><i class='criticaUsuarios'>"+'"'+arrayTextosCriticasMostrar[2]+'"'+"</i></div><div class='fechaCriticasUsuarios'>"+arrayHorasCriticasMostrar[2]+"</div></div><hr>");
                                                        clickEnNickIrPerfil();
                                                    });                                                  
                                                }else if(arrayCriticasMostrar.length==2){
                                                    $.post("../modelo/modelo.php",{idCadaUsuarioCriticaFilm:arrayIdsCriticasMostrar[0]},function(respuesta){
                                                        ////console.log(JSON.parse(respuesta)[0][0]+" - "+arrayTextosCriticasMostrar[0]+" - "+arrayHorasCriticasMostrar[0]);
                                                        $("#criticasDeUsuarios").append("<div class='contenedorCritica'><strong class='nicksUsuariosCriticas'><i class='fas fa-user iconosUsuariosCriticas'></i>"+JSON.parse(respuesta)[0][0]+"</strong><br><div class='textoCritica'><i class='criticaUsuarios'>"+'"'+arrayTextosCriticasMostrar[0]+'"'+"</i></div><div class='fechaCriticasUsuarios'>"+arrayHorasCriticasMostrar[0]+"</div></div><hr>");                                                       
                                                        clickEnNickIrPerfil();
                                                    });
                                                    $.post("../modelo/modelo.php",{idCadaUsuarioCriticaFilm:arrayIdsCriticasMostrar[1]},function(respuesta){
                                                        ////console.log(JSON.parse(respuesta)[0][0]+" - "+arrayTextosCriticasMostrar[1]+" - "+arrayHorasCriticasMostrar[1]);
                                                        $("#criticasDeUsuarios").append("<div class='contenedorCritica'><strong class='nicksUsuariosCriticas'><i class='fas fa-user iconosUsuariosCriticas'></i>"+JSON.parse(respuesta)[0][0]+"</strong><br><div class='textoCritica'><i class='criticaUsuarios'>"+'"'+arrayTextosCriticasMostrar[1]+'"'+"</i></div><div class='fechaCriticasUsuarios'>"+arrayHorasCriticasMostrar[1]+"</div></div><hr>");
                                                        clickEnNickIrPerfil();
                                                    });                                                   
                                                }else if(arrayCriticasMostrar.length==1){
                                                    $.post("../modelo/modelo.php",{idCadaUsuarioCriticaFilm:arrayIdsCriticasMostrar[0]},function(respuesta){
                                                        ////console.log(JSON.parse(respuesta)[0][0]+" - "+arrayTextosCriticasMostrar[0]+" - "+arrayHorasCriticasMostrar[0]);
                                                        $("#criticasDeUsuarios").append("<div class='contenedorCritica'><strong class='nicksUsuariosCriticas'><i class='fas fa-user iconosUsuariosCriticas'></i>"+JSON.parse(respuesta)[0][0]+"</strong><br><div class='textoCritica'><i class='criticaUsuarios'>"+'"'+arrayTextosCriticasMostrar[0]+'"'+"</i></div><div class='fechaCriticasUsuarios'>"+arrayHorasCriticasMostrar[0]+"</div></div><hr>");
                                                        clickEnNickIrPerfil();
                                                    });
                                                }
                                                //FIN IMPRIMIR LAS CRITICAS QUE TIENE LA PELICULA

                                              

                                        }//Fin si la pelicula tiene alguna crítica


                                        //Si el usuario tiene una critica en esta pelicula
                                        if(objetoCriticaUsuario){
                                            
                                            $("#tituloCriticaUsuario").html("<h5><strong>Tu crítica</strong></h5>");


                                            //OBTENGO LA FECHA DE LA CRITICA DEL USUARIO BIEN FORMATEADA:
                                            var fechaCriticaDelUsuario = objetoCriticaUsuario[3];
                                         

                                            var fechacriticausuario = formatearFechaHora(fechaCriticaDelUsuario);

                                            
                                            //Tengo que imprimir la crítica del usuario:
                                            
                                            $("#tuCritica").append("<div class='criticaDelUsuario'><div class='opcionesCritica'><i class='fas fa-edit editarCritica'></i><i class='fas fa-trash-alt borrarCritica'></i></div><i class='textoCritica'>"+'"'+objetoCriticaUsuario[2]+'"'+"</i><div class='fechaCriticaUsuario'>"+fechacriticausuario+"</div></div>")
                                            //$("#tuCritica").append("<br><div id='divBotonModificarCritica'><span id='modificaCritica'>Modifica tu critica</span></div>");

                                            //Si se hace click en modificar critica:
                                            $(".editarCritica").on("click",function(){
                                                //Sale un input de sweetalert2
                                               Swal.fire({
                                                    input:'textarea',
                                                    title:'Escribe tu nueva criticaa:',
                                                    inputPlaceholder:'Escribe la nueva crítica...',
                                                    inputAttributes:{
                                                        'arial-label':'Escribe la nueva crítica...'
                                                    },
                                                    showCancelButton:true

                                                }).then((result)=> {
                                                    
                                                    if(result.value){
                                                        //Si se escribe un texto y se le da a ok:
                                                        var nuevaCritica=result.value;
                                                       // //console.log(nuevaCritica);

                                                        //Evito insercion de script
                                                        regex= new RegExp( /<.*?>/g);                                                        
                                                        rex = regex.test(nuevaCritica);
                                                        

                                                        //Si detecta scripts muestro mensaje de error 
                                                        if(rex==true){
                                                            nuevaCritica="ERROR";
                                                              //Muestro mensaje de crítica eliminada
                                                              Swal.fire({
                                                                icon: 'error',
                                                                title:'ERROR',
                                                                showConfirmButton:false,
                                                                timer:1500
                                                            })//Fin mensaje critica eliminada
                                                        }else{                                                       

                                                            //La pongo a mayusculas 
                                                            nuevaCritica = nuevaCritica.charAt(0).toUpperCase()+nuevaCritica.slice(1);

                                                            //Necesito saber la hora de la modificacion:
                                                            var fechaactual = new Date().toISOString().slice(0,19).replace('T',' ');
                                                            var fechaActualFormateada = formatearFechaHora(fechaactual);
                                                        
                                                            //Inserto la fecha actual y la hora
                                                            $(".fechaCriticaUsuario").html(fechaActualFormateada);

                                                            //Se inserta en el html:
                                                            $(".textoCritica").html('<i>"'+result.value+'"</i>');

                                                            //Hacer peticion al modelo para modificar la critica del usuario actual:
                                                            $.post("../modelo/modelo.php",{criticaModificar:nuevaCritica,idFilmModificarCritica:idFilmEncontrado,idUsuarioModificarCritica:sessionStorage.Id},function(respuesta){
                                                                
                                                                //Si se cambia correctamente en la base de datos:
                                                                if(respuesta=="CRITICAMODIFICADA"){
                                                                    //Imprimo un mensaje correcto
                                                                    Swal.fire({
                                                                        icon:'success',
                                                                        title:'Critica modificada',
                                                                        showConfirmButton:false,
                                                                        timer:1500
                                                                    });
                                                                }
                                                            })//Fin peticion para modificar la critica actual
                                                        }//Fin deteccion de script
                                                    }//Fin si se escribe y se le da a ok
                                                })//Fin sweetalert2 
                                            })//Fin click en modificar critica


                                            //Si se hace click en borrar critica:
                                            $(".borrarCritica").on("click",function(){                                                
                                                //Muestro un mensaje de sweetalert2
                                                Swal.fire({
                                                    title:'Deseas eliminar tu crítica?',
                                                    icon:'warning',
                                                    showCancelButton:true,
                                                    confirmButtonColor:'#3085d6',
                                                    cancelButtonColor:'#d33',
                                                    confirmButtonText:'Si',
                                                    cancelButtonText:'No'
                                                }).then((result)=>{
                                                    //Si se hace click en aceptar
                                                    if(result.value){
                                                        //Hacer peticion al modelo para eliminar la critica del usuario:
                                                        $.post("../modelo/modelo.php",{idFilmBorrarCritica:idFilmEncontrado,idUsuarioBorrarCritica:sessionStorage.Id},function(respuestaBorrarCritica){
                                                            if(respuestaBorrarCritica=="CRITICABORRADA"){//Si se ha borrado la crítica:
                                                                $("#tuCritica").hide();
                                                                $("#tuPrimeraCritica").show();
                                                                //Muestro mensaje de crítica eliminada
                                                                Swal.fire({
                                                                    icon: 'success',
                                                                    title:'Critica eliminada',
                                                                    showConfirmButton:false,
                                                                    timer:1500
                                                                })//Fin mensaje critica eliminada
                                                            }//Fin si se ha borrado la critica
                                                        })//Fin peticion al modelo para borrar la critica                                                      
                                                    }//Fin si se hace click en aceptar
                                                })//Fin mensaje sweetalert2 
                                            });//Fin borrar critica

                                        }else{//Si el usuario no tiene ninguna critica
                                            $("#tuPrimeraCritica").show();
                                            
                                            //$("#tituloCriticaUsuario").html("<h5><strong>Escribe tu primera crítica en esta película:</strong></h5>");
                                        }//Fin si el usuario tiene critica o no

                                        //Al hacer click en el lapiz
                                        $(".botonEscribirCritica").on("click",function(){
                                             //Sale un input de sweetalert2
                                             Swal.fire({
                                                input:'textarea',
                                                title:'Escribe tu nueva critica:',
                                                inputPlaceholder:'Escribe la nueva crítica...',
                                                inputAttributes:{
                                                    'arial-label':'Escribe la nueva crítica...'
                                                },
                                                showCancelButton:true

                                            }).then((result)=> {
                                                if(result.value){
                                                    //Si se escribe un texto y se le da a ok:
                                                    var nuevaCritica=result.value;

                                                    //Evito insercion de script
                                                    regex= new RegExp( /<.*?>/g);                                                        
                                                    rex = regex.test(nuevaCritica);
                                                    

                                                    //Si detecta scripts muestro mensaje de error 
                                                    if(rex==true){
                                                        nuevaCritica="ERROR";
                                                            //Muestro mensaje de crítica eliminada
                                                            Swal.fire({
                                                            icon: 'error',
                                                            title:'ERROR',
                                                            showConfirmButton:false,
                                                            timer:1500
                                                        })//Fin mensaje critica eliminada
                                                    }else{ 

                                                        //La pongo a mayusculas
                                                        nuevaCritica = nuevaCritica.charAt(0).toUpperCase()+nuevaCritica.slice(1);

                                                        //Peticion al modelo para escribir una nueva critica
                                                        $.post("../modelo/modelo.php",{idFilmNuevaCritica:idFilmEncontrado,idUsuarioNuevaCritica:sessionStorage.Id,nuevaCritica:nuevaCritica},function(respuestaNuevaCritica){
                                                            ////console.log(respuestaNuevaCritica);
                                                            if(respuestaNuevaCritica=="CREADANUEVACRITICA"){
                                                                location.reload();                                     
                                                            }
                                                        })//Fin peticion modelo para escribir una nueva critica 
                                                    }//Fin evitar insercion scripts                                                 
                                                }//Fin si se escribe y se le da a ok
                                            })//Fin sweetalert2 
                                        })//Al hacer click en el lapiz


                                            var votoDelUsuario ="";
                                            
                                            if(objetoVotoUsuario==undefined){
                                                votoDelUsuario="no has votado";
                                            }else{
                                                votoDelUsuario =objetoVotoUsuario["nota"];
                                            }


                                            //Insertare la bandera del pais
                                            ////console.log(paisFilmEncontrado);
                                            //Consulta al modelo para saber el pais del film:
                                            $.post("../modelo/modelo.php",{idPais:paisFilmEncontrado},function(respuestaPais){                                               

                                                if(JSON.parse(respuestaPais)["status"]!="SINPAIS"){//Si se recibe un pais:
                                                    $("#idfotopais").html("<img src='../fotos/paises/"+JSON.parse(respuestaPais)[0][0]+"'/><span id='nombrePais'>"+JSON.parse(respuestaPais)[0][1]+"</span>");
                                                }else{//Si no se recibe informacion del pais, es decir, si no tenemos ese pais en la base de datos:
                                                    //Oculto la informacion del pais de la pelicula
                                                    $("#contenedorPais").hide();
                                                }
                                            });//Fin consulta para saber el pais del film


                                           

                                            //Inserto en el div de vue:
                                            var appFilm = new Vue({
                                                el:"#appFilm",
                                                data:{
                                                    tituloFilm:tituloFilmEncontrado,
                                                    notaDelUsuario:votoDelUsuario,
                                                    notaPelicula:arrayNotaMediaVotos[0].toFixed(1),
                                                    numeroVotosPelicula:arrayNotaMediaVotos[1],  
                                                    tipoPelicula: tipoFilmEncontrado, 
                                                    directorPelicula:directorFilmEncontrado,
                                                    fechaPelicula:fechaFilmEncontrado,
                                                    duracionPelicula:duracionFilmEncontrado,
                                                    guionPelicula:guionFilmEncontrado,
                                                    generoPelicula:generoFilmEncontrado,
                                                    repartoPelicula:repartoFilmEncontrado,
                                                    sinopsisPelicula:sinopsisFilmEncontrado                                                
                                                }
                                            });//FIN VUE


                                            //AÑADIR A FAVORITAS
                                            //COMPROBAR SI LA PELICULA ESTA EN FAVORITAS:
                                            $.post("../modelo/modelo.php",{idFilmComprobarFavorito:idFilmEncontrado,idUsuarioComprobarFavorito:sessionStorage.Id},function(respuestaFavoritoFilm){
                                                //Si la pelicula está en favoritas:
                                                if(respuestaFavoritoFilm==1){
                                                    //Elimino el color gris y pongo el amarillo
                                                    $("#btnFav").removeClass("estrellaFavoritaGris");
                                                    $("#btnFav").addClass("estrellaFavoritaAmarilla");
                                                }else{
                                                    //Si no esta en favoritas, elimino el amarillo y pongo el gris
                                                    $("#btnFav").removeClass("estrellaFavoritaAmarilla");
                                                    $("#btnFav").addClass("estrellaFavoritaGris");
                                                }
                                            })

                                            //Al hacer click en la estrella
                                            $("#btnFav").on("click",function(){
                                                //Si NO LA TENGO COMO FAVORITA_
                                                if($("#btnFav").hasClass("estrellaFavoritaGris")){
                                                    //Al hacer click pongo la estrella amarilla
                                                    $("#btnFav").removeClass("estrellaFavoritaGris");
                                                    $("#btnFav").addClass("estrellaFavoritaAmarilla");                                                    

                                                    //HACER PETICION AL POST PARA AÑADIR A FAVORITA
                                                    $.post("../modelo/modelo.php",{idFilmAgregarFavorito:idFilmEncontrado,idUsuarioAgregarFavorito:sessionStorage.Id}, function(respuestaAgregarFavoritos){                                                        
                                                        if(respuestaAgregarFavoritos=="AGREGADOAFAVORITOS"){
                                                            //Muestro mensaje de crítica eliminada
                                                            Swal.fire({
                                                                icon: 'success',
                                                                title:'Agregada a favoritas',
                                                                showConfirmButton:false,
                                                                timer:1200
                                                            })//Fin mensaje critica eliminada
                                                        }
                                                    });//FIN PETICION PARA AÑADIR A FAVORITA


                                                }else{//Al hacer click pongo la estrella gris
                                                    $("#btnFav").removeClass("estrellaFavoritaAmarilla");
                                                    $("#btnFav").addClass("estrellaFavoritaGris");
                                                    //HACER PETICION AL POST PARA AÑADIR A FAVORITA
                                                    $.post("../modelo/modelo.php",{idFilmEliminarFavorito:idFilmEncontrado,idUsuarioEliminarFavorito:sessionStorage.Id}, function(respuestaEliminarFavoritos){                                                        
                                                        if(respuestaEliminarFavoritos=="ELIMINADODEFAVORITOS"){
                                                            //Muestro mensaje de crítica eliminada
                                                            Swal.fire({
                                                                icon: 'success',
                                                                title:'Eliminada de favoritas',
                                                                showConfirmButton:false,
                                                                timer:1200
                                                            })//Fin mensaje critica eliminada
                                                        }
                                                    });//FIN PETICION PARA AÑADIR A FAVORITA

                                                };
                                                
                                            });//Fin click en la estrella



                                            //Cuando se hace click para votar
                                            $(".tuNota").on("click",function(){                                                
                                                $("#menuVotar").show();                                             
    

                                                //Si se pulsa en cualquier elemento
                                                $("#menuVotar div").on("click",function(){
                                                    var notaNueva =$(this).attr("id").split("nota")[1];

                                                    //Hacer peticion a modelo para modificar la nota del usuario en esta pelicula:
                                                    $.post("../modelo/modelo.php",{notaNuevaFilm:notaNueva,idFilmNotaNueva:idFilmEncontrado,idUsuarioNotaNueva:sessionStorage.Id},function(respuestaVoto){                                                            
                                                        //Si el voto es nuevo y se ha votado, hay que sumar un voto a los que hay
                                                        if(respuestaVoto=="VOTONUEVO"){
                                                            var votosExistentes= $(".numeroVotosFilmVue").html().split("<br>");
                                                            ////console.log(votosExistentes);
                                                            votosExistentesNumero = parseInt(votosExistentes)+1;
                                                            $(".numeroVotosFilmVue").html(votosExistentesNumero+"<br>votos");
                                                        }//Fin si el voto no es nuevo

                                                        //Volver a consultar la nota media de una pelicula y el numero de votos
                                                        $.post("../modelo/modelo.php",{obtenerNotaPeliculaPorId:idFilmEncontrado},function(arrayNotaMediaVotosJSON){                                                       
                                                            var arrayNotaMediaVotos = JSON.parse(arrayNotaMediaVotosJSON);
                                                            var nuevaNotaMedia = arrayNotaMediaVotos[0];
                                                            $(".notaPeliculaFilmVue").html(nuevaNotaMedia.toFixed(1));                                                            
                                                        });//Fin volver a calcular la media

                                                    });//Fin modificar nota

                                                    $(".tuNota").html(notaNueva);
                                                    $("#menuVotar").hide();
                                                })

                                                //Si se pulsa en cerrar votos:
                                                $("#cerrarVotos").on("click",function(){
                                                    $("#menuVotar").hide();
                                                })
                                            })//Fin click votar

                                            



                                            
                                            //Inserto la foto:                            
                                           //Inserto la foto:                            
                                            if(fotoFilmEncontrado.search("https://")!=-1){//Este fragmento tiene que estar, ya que si en la base de datos la imagen esta en internet, debo mostrarla:
                                                $("#portadaFilm").attr("src",fotoFilmEncontrado);
                                            }else{
                                                $("#portadaFilm").attr("src","../"+fotoFilmEncontrado);
                                            }

                                            
                                        

                                        });//Fin consulta para obtener todas las criticas de una pelicula 
                                       
                                    });//Fin consulta para obtener la nota media de una pelicula y el numero de votos                                    
                                });//Fin buscar critica del usuario
                            });//Fin buscar voto del usuario
                            //FIN CONSULTAS SOBRE INFORMACION DEL USUARIO LOGUEADO EN EL FILM------------------------------------------------------------------------------------


                            /*//Lo he metido dentro del ultimo post
                            //Inserto en el div de vue:
                            var appFilm = new Vue({
                                el:"#appFilm",
                                data:{
                                    tituloFilm:tituloFilmEncontrado
                                }
                            });//FIN VUE

                            //Inserto la foto:                            
                            $("#portadaFilm").attr("src","../"+fotoFilmEncontrado);
                            */
                          
                            




                        });//Fin consulta de informacion de pelicula por id

                        



            }else{//Si no existe el id:
                //$("#infoFilm").html("<h4><i class='fas fa-exclamation-triangle'></i> No se ha encontrado pelicula con el id: "+idBuscarInfo+"</h4>")
                $("#infoFilm").html("<h4><i class='fas fa-exclamation-triangle peliNoEncontrada'></i> No se ha encontrado pelicula</h4>")
            }//Fin si no existe el id

        });//Fin consulta comprobacion si el id existe o no      
    }//Fin si se intenta acceder solo a "film.php" sin pasarle ningun parámetro


    //Llamo la funcion para que funcione el boton de scroll en el index principal
    $(".subirFilm").on("click",function(){
        scrollTop();
    });//Fin boton subir



});//FIN DOCUMENT READY
//Funcion para el boton de scroll hacia arriba (Con anmacion, usar en el boton para subir):
function scrollTop(){
    $("html, body").animate({scrollTop:0},'fast');
    //$("html,body").scrollTop(0);
}//Fin funcion scroll up

//Funcion para obtener de la url el parametro id:
function obtenerIdURL(){    
    var stringUrl =window.location.href;
    var arrayUrl = stringUrl.split("/");
    var parametros =arrayUrl[arrayUrl.length-1]; 
    //console.log(parametros)   ;
    return parametros;
};//Fin funcion obtener de la url el parametro;

//Funcion que se le pasa una fecha en formato MYSQL (YYYY-MM-DD HH:MM:SS) y la devuelve en el formato DD/MM/YYYY HH:MM
function formatearFechaHora(fecha){
   //Formateo la fecha
   var arrayfecha = fecha.split(" ");
   var fechaFormateadaUsuario = arrayfecha[0].split("-");
   var horaFormateadausuario = arrayfecha[1].split(":");
   var fechaformateada =fechaFormateadaUsuario[2]+"/"+fechaFormateadaUsuario[1]+"/"+fechaFormateadaUsuario[0]+" "+horaFormateadausuario[0]+":"+horaFormateadausuario[1];  
    return fechaformateada;

}


function clickEnNickIrPerfil(){
    $(".nicksUsuariosCriticas").on("click",function(){                                                            
        var nickUsuario =$(this).text();
        /*window.location.replace("/vistas/perfil.php?nick="+nickUsuario);*/
        //window.location.href="perfil.php?nick="+nickUsuario;
        window.location.href="/perfil/"+nickUsuario;

    });
}