$(document).ready(function(){
    
    //console.log(sessionStorage);
    
	//Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);


    //Necesito saber el numero TOTAL de peliculas que existe en la base de datos:
    $.post("../modelo/modelo.php",{numeroTotalPeliculas:"true"},function(respuestaNumeroTotalPeliculas){
        //console.log(respuestaNumeroTotalPeliculas);
       
       
        //console.log(numeroPeliculas);

       
        //Peticion obtener todas las peliculas ordenadas por titulo
        $.post("../modelo/modelo.php",{obtenerTodasLasPeliculas:"true"},function(respuestaObtenerTodasLasPeliculas){
            //console.log(respuestaMostrarDiezPeliculas);
            var arrayTodasLasPeliculas = JSON.parse(respuestaObtenerTodasLasPeliculas);

            //ARRAY DE TODAS LAS PELICULAS:
            //console.log(arrayTodasLasPeliculas);

            if(arrayTodasLasPeliculas.length>0){
            for(var i=0;i<arrayTodasLasPeliculas.length;i++){
                var idFilmDiez = arrayTodasLasPeliculas[i][2];               
                var tituloFilmDiez = arrayTodasLasPeliculas[i][1];
                var fotoFilmDiez = arrayTodasLasPeliculas[i][0];

                //Añado las peliculas al div
                $("#listadoCompletoFilms").append(`
                <div class='contenedorFilmDiez idFilmTop`+idFilmDiez+`'>
                    <img class='fotoDiez' src='`+fotoFilmDiez+`'/>
                    <span class='tituloDiez'><i>`+tituloFilmDiez+`</i></span>                   
                </div>`);//Fin añadir peliculas
            }



            //Al hacer click en una pelicula voy a la pagina de esa película:
            irPaginaFilm();

        }else{
            $("#listadoCompletoFilms").append(`<div>No se han encontrado films.</div>`);
        }


    });//Fin saber numero total de peliculas que existe en la base de datos

});

    //Al pulsar en el boton de buscar film:
    $(".botonBuscarFilmAdmin").on("click",function(){
        //Realizo la busqueda del film utilizando la funcion
        buscarFilmAdmin();
    });//Fin al pulsar en el boton de buscar film:

   

    //Para que tambien realice la busqueda si se pulsa enter:
    $("#filmBuscarAdmin").keyup(function(e){
        if(e.keyCode==13){
            //Realizo la busqueda del film utilizando la funcion
            buscarFilmAdmin();
        }
    })//Fin para que realice la busqueda si se pulsa enter



});//Fin document ready

//Funcion para realizar la busqueda de film
function buscarFilmAdmin(){
       //Debo obtener lo que hay en el campo del buscador
       var textoBuscador = $(".campoTextoBuscadorFilm").val();
       //console.log(textoBuscador);

       //Vacio lo que hay anteriormente en el div de peliculas encontradas
       $("#listadoFilmsEncontradasBuscador").empty();


       //Oculto el listado predeterminado que contiene todas las peliculas
       $("#listadoCompletoFilms").hide();

       //Tengo que hacer una consulta para obtener las películas buscadas
       $.post("../modelo/modelo.php",{buscarPeliculasAdmin:"true",tituloBuscarFilmAdmin:textoBuscador},function(respuestaBuscarPeliculasAdmin){

           var arrayPeliculasEncontradasAdmin = JSON.parse(respuestaBuscarPeliculasAdmin)
           //console.log(arrayPeliculasEncontradasAdmin);


           //Recorro el array para mostrarlas en el div
           for(var i=0;i<arrayPeliculasEncontradasAdmin.length;i++){
               var idFilmDiez = arrayPeliculasEncontradasAdmin[i][2];               
               var tituloFilmDiez = arrayPeliculasEncontradasAdmin[i][1];
               var fotoFilmDiez = arrayPeliculasEncontradasAdmin[i][0];

               //Añado las peliculas al div
               $("#listadoFilmsEncontradasBuscador").append(`
               <div class='contenedorFilmDiez idFilmTop`+idFilmDiez+`'>
                   <img class='fotoDiez' src='`+fotoFilmDiez+`'/>
                   <span class='tituloDiez'><i>`+tituloFilmDiez+`</i></span>                   
               </div>`);//Fin añadir peliculas
           }//Fin recorro array para mostrar en div

            //Al hacer click en una pelicula voy a la pagina de esa película:
            irPaginaFilm();

       })//Fin consulta para obtener las películas buscadas

       //Muestro el listado de las peliculas encontradas
       $("#listadoFilmsEncontradasBuscador").show();

}//Fin funcion buscar film


//Funcion para enviar a la pagina de la pelicula al hacer click en una pelicula
function irPaginaFilm(){
    //Cuando hago click en el film, debo obtener el título:
    //$(".contenedorFilmDiez").on("click",function(){
    //    console.log($(this).children().text());
    //});

    //Cuando hago click en una pelicula
    $(".contenedorFilmDiez").on("click",function(){
        //Obtengo el id de ese film
        var idFilmTopTresPulsado = $(this).attr("class").split(" ")[1].split("idFilmTop")[1];			
        //Redirijo a la pagina del film:
        window.location.href="/filmAdmin/"+idFilmTopTresPulsado;
    });//Fin al hacer click en una de las tres peliculas	

}//Fin funcion para enviar a la pagina de la pelicula al hacer click en un film