$(document).ready(function(){
    
    //console.log(sessionStorage);

	//Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);


    //Necesito saber el numero TOTAL de peliculas que existe en la base de datos:
    $.post("../modelo/modelo.php",{numeroTotalUsuarios:"true"},function(respuestaNumeroTotalUsuarios){
        //console.log(respuestaNumeroTotalPeliculas);
        //console.log(respuestaNumeroTotalUsuarios);
       
        //console.log(numeroPeliculas);

        //Peticion para obtener todos los usuarios ordenados por nick
        $.post("../modelo/modelo.php",{obtenerTodosLosUsuarios:"true",idAdmin:sessionStorage.Id},function(respuestaObtenerUsuarios){
            //console.log(respuestaObtenerUsuarios);
            var arrayUsuarios = JSON.parse(respuestaObtenerUsuarios);
            //console.log(arrayUsuarios);

            if(arrayUsuarios.length>0){

            //Recorro el array para ir creando la lista con los usuarios
            for(var i=0;i<arrayUsuarios.length;i++){
                var nick = arrayUsuarios[i][0];
                var nombre = arrayUsuarios[i][1];
                var apellidos = arrayUsuarios[i][2];
                var cuentaActiva = arrayUsuarios[i][3];


                //Añado los usuarios al div
                $("#listadoCompletoUsuarios").append(`
                <div class='contenedorUsuario `+nick+` cuentaActiva`+cuentaActiva+`'>
                    <i class="fas fa-user-circle iconoCadaUsuarioLista"></i>
                    <span class='tituloUsuarios'><i>`+nick+`</i></span>
                    
                    <span class='nombreUsuarioLista'>`+nombre+`</i>                   
                    <span class='apellidoUsuarioLista'>`+apellidos+`</i>        
                </div>`);


            }//Fin recorrer array

            //Al hacer click en un usuario:
            irPaginaUsuario();

            }else{
                 //Añado los usuarios al div
                 $("#listadoCompletoUsuarios").append(`<div>No se han encontrado usuarios.</div>`);
            }

        });//Fin peticion al modelo para obtener los usuarios ordenados por nick



});

    //Al pulsar en el boton de buscar film:
    $(".botonBuscarUsuarioAdmin").on("click",function(){
        //Realizo la busqueda del film utilizando la funcion    
        buscarUsuarioAdmin();
    });//Fin al pulsar en el boton de buscar film:

   

    //Para que tambien realice la busqueda si se pulsa enter:
    $("#usuarioBuscarAdmin").keyup(function(e){
        if(e.keyCode==13){
            //Realizo la busqueda del film utilizando la funcion
            buscarUsuarioAdmin();        }
    })//Fin para que realice la busqueda si se pulsa enter



});//Fin document ready



//Funcion para realizar la busqueda de film
function buscarUsuarioAdmin(){
       //Debo obtener lo que hay en el campo del buscador
       var textoBuscador = $(".campoTextoBuscadorUsuario").val();
       //console.log(textoBuscador);

       //Vacio lo que hay anteriormente en el div de peliculas encontradas
       $("#listadoCompletoUsuarios").empty();


       //Oculto el listado predeterminado que contiene todas las peliculas
       //$("#listadoCompletoUsuarios").hide();

       //Tengo que hacer una consulta para obtener los usuarios buscados
       $.post("../modelo/modelo.php",{buscarUsuariosAdmin:"true",usuarioBuscarAdmin:textoBuscador},function(respuestaBuscarPeliculasAdmin){

            var arrayUsuariosEncontrados = JSON.parse(respuestaBuscarPeliculasAdmin)
            //console.log(arrayUsuariosEncontrados);

           //Recorro el array para mostrarlas en el div
           for(var i=0;i<arrayUsuariosEncontrados.length;i++){
               var nickEncontrados = arrayUsuariosEncontrados[i][0];               
               var nombreEncontrados = arrayUsuariosEncontrados[i][1];
               var apellidosEncontrados = arrayUsuariosEncontrados[i][2];
               var cuentaActivaEncontrados = arrayUsuariosEncontrados[i][3];

                //Añado los usuarios al div
                $("#listadoCompletoUsuarios").append(`
                <div class='contenedorUsuario `+nickEncontrados+` cuentaActiva`+cuentaActivaEncontrados+`'>
                    <i class="fas fa-user-circle iconoCadaUsuarioLista"></i>
                    <span class='tituloUsuarios'><i>`+nickEncontrados+`</i></span>
                    
                    <span class='nombreUsuarioLista'>`+nombreEncontrados+`</i>                   
                    <span class='apellidoUsuarioLista'>`+apellidosEncontrados+`</i>        
                </div>`);
           }//Fin recorro array para mostrar en div

            //Al hacer click en una pelicula voy a la pagina de esa película:
            irPaginaUsuario();

       })//Fin consulta para obtener los usuarios buscados


     

}//Fin funcion buscar film




//Funcion para enviar a la pagina de la pelicula al hacer click en una pelicula
function irPaginaUsuario(){
    
    //Cuando hago click en una pelicula
    $(".contenedorUsuario").on("click",function(){
        //Obtengo el nick del usuario
        var nickUsuarioPulsado = $(this).attr("class").split(" ")[1];			

        console.log(nickUsuarioPulsado);


        //Redirijo a la pagina del film (perfilUsuarioAdmin.php):
        window.location.href="/perfilUsuario/"+nickUsuarioPulsado;
    });//Fin al hacer click en una de las tres peliculas	

}//Fin funcion para enviar a la pagina de la pelicula al hacer click en un film