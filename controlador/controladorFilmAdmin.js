$(document).ready(function(){
    //console.log(sessionStorage);
	

    //Inserto en la cabecera el nombre del usuario
    $("#spanNickUsuario").text(sessionStorage.Nick);



    //Obtener de la url el id de la pelicula actual
    var idBuscarInfo = obtenerIdURL();
    //console.log(idBuscarInfo);
    ////console.log(idBuscarInfo);
    if(isNaN(parseInt(idBuscarInfo))){
        idBuscarInfo = -1;
    };


    //Si se intenta acceder solo a "film.php" sin pasarle ningun parámetro
    if(idBuscarInfo==undefined){
        $("#infoFilm").html("<h4><i class='fas fa-exclamation-triangle'></i> A esta página solo se puede acceder si se ha realizado la busqueda de una película <i class='fas fa-exclamation-triangle'></i></h4>")
    }else{
        


        //Tengo que comprobar si el id introducido existe o no:
        $.post("../modelo/modelo.php",{idBuscarInfoExiste:idBuscarInfo},function(numeroFilmsEncontradosJSON){
            var arrayNumeroFilms = JSON.parse(numeroFilmsEncontradosJSON);
            var numeroFilmsEncontrados = arrayNumeroFilms[0][0];
            
            //Si se ha encontrado una película con el ID que aparece en la URL
            if(numeroFilmsEncontrados==1){

                        //tengo que hacer una consulta al modelo para obtener toda la información de la pelicula a partir del id
                        $.post("../modelo/modelo.php",{idBuscarInfo:idBuscarInfo},function(infoFilmJSON){
                            var arrayInfoFilm = JSON.parse(infoFilmJSON);
                            var objetoFilm = arrayInfoFilm[0];
                            ////console.log("INFORMACION DE LA PELICULA");
                            ////console.log(objetoFilm);

                            //Información del film:
                            var idFilmEncontrado = objetoFilm["idfilm"];
                            var tituloFilmEncontrado = objetoFilm["titulo"];
                            var fechaFilmEncontrado = objetoFilm["fechaestreno"].split("-")[0];
                            var paisFilmEncontrado = objetoFilm["pais"];
                            var directorFilmEncontrado = objetoFilm["director"];
                            var generoFilmEncontrado = objetoFilm["genero"];
                            var sinopsisFilmEncontrado = objetoFilm["sinopsis"];
                            var tipoFilmEncontrado = objetoFilm["tipo"];
                            var fotoFilmEncontrado = objetoFilm["fotoportada"];
                            var duracionFilmEncontrado = objetoFilm["duracion"];
                            var guionFilmEncontrado = objetoFilm["guion"];
                            var repartoFilmEncontrado = objetoFilm["reparto"];      

                            //Insertare la bandera del pais
                            ////console.log(paisFilmEncontrado);
                            //Consulta al modelo para saber el pais del film:
                            $.post("../modelo/modelo.php",{idPais:paisFilmEncontrado},function(respuestaPais){                                               

                                if(JSON.parse(respuestaPais)["status"]!="SINPAIS"){//Si se recibe un pais:
                                    $("#idfotopais").html("<img src='../fotos/paises/"+JSON.parse(respuestaPais)[0][0]+"'/><span id='nombrePais'>"+JSON.parse(respuestaPais)[0][1]+"</span>");
                                }else{//Si no se recibe informacion del pais, es decir, si no tenemos ese pais en la base de datos:
                                    //Oculto la informacion del pais de la pelicula
                                    $("#contenedorPais").hide();
                                }
                            });//Fin consulta para saber el pais del film

                            

                            //Inserto en el div de vue:
                            var appFilm = new Vue({
                                el:"#appFilm",
                                data:{
                                    tituloFilm:tituloFilmEncontrado,                                   
                                    tipoPelicula: tipoFilmEncontrado, 
                                    directorPelicula:directorFilmEncontrado,
                                    fechaPelicula:fechaFilmEncontrado,
                                    duracionPelicula:duracionFilmEncontrado,
                                    guionPelicula:guionFilmEncontrado,
                                    generoPelicula:generoFilmEncontrado,
                                    repartoPelicula:repartoFilmEncontrado,
                                    sinopsisPelicula:sinopsisFilmEncontrado                                                
                                }
                            });//FIN VUE     
                            
                            //Inserto la foto:                            
                            if(fotoFilmEncontrado.search("https://")!=-1){//Este fragmento tiene que estar, ya que si en la base de datos la imagen esta en internet, debo mostrarla:
                                $("#portadaFilm").attr("src",fotoFilmEncontrado);
                            }else{
                                $("#portadaFilm").attr("src","../"+fotoFilmEncontrado);
                            }


                            //Al hacer click en editar el film
                            $(".editarFilm").on("click",function(){
                                //Muestro el boton ara editar el pais y su mensaje de información
                                $(".infoEditarPais").show();
                                $(".editarPais").show();
                                $(".upload").hide();

                                //Creo un array donde guardaré los datos de la película
                                var arrayDatos = [];

                                //Insertaré los datos que hay en la página en ese array
                                for(var i = 0; i<$(".editar").length;i++){
                                    //console.log($(".editar")[i].firstChild.data.trim());

                                    //Obtengo los datos y los meto en el array
                                    var datos = $(".editar")[i].firstChild.data.trim();
                                    arrayDatos.push(datos);                                   
                                }//Fin insertaré los datos que hay en la página en ese array


                                //console.log(arrayDatos);

                                //Recorro el array y voy transformando cada campo texto de la pagina en inputs con los datos que habia antes:
                                for(var i = 0 ; i<arrayDatos.length;i++){
                                    
                                    if(i<7){                                    
                                        $(".editada"+[i]).html("<input class='input"+i+"' value='"+arrayDatos[i]+"'></input>");
                                    }else if(i==7){
                                        $(".editada"+[7]).html("<textarea class='textarea modificar1'>"+arrayDatos[7]+"</textarea>");
                                    }else if(i==8){
                                        $(".editada"+[8]).html("<textarea class='textarea modificar2'>"+arrayDatos[8]+"</textarea>");
                                    }
                                }//Fin recorro el array y voy transformando cada campo texto de la pagina en inputs con los datos que habia antes

                                
                                //Oculto los botones de edición:
                                $(".cambiarFotoFilm").hide();
                                $(".editarFilm").hide();
                                $(".upload").hide();


                                //Muestro el boton de guardar cambios:
                                $(".guardar").show();
                                $(".cancelar").show();


                                //Si pulso en cancelar, no hago nada:
                                $(".cancelar").on("click",function(){
                                    location.reload();
                                });//Fin pulsar boton cancelar


                                //Si pulso en el boton "Guardar Cambios" tengo que hacer el efecto contrario, es decir, todo lo de los input pasarlo guardarlo en la base de datos y pasarlo a texto (Puedo hacerlo recargando pagina)
                                $(".guardar").on("click",function(){
                                   

                                    var arrayModificar = [];                                   

                                    for(var i=0;i<7;i++){
                                        //console.log($(".input"+i).val());
                                        arrayModificar.push($(".input"+i).val());
                                    }
                                    for(var i=1;i<=2;i++){
                                        arrayModificar.push($(".modificar"+i).val());
                                    }


                                    //console.log(arrayModificar);
                                    //[0] -> Titulo
                                    //[1] -> Tipo (pelicula)
                                    //[2] -> Director
                                    //[3] -> Año
                                    //[4] -> Duracion en Minutos
                                    //[5] -> Guion
                                    //[6] -> Genero
                                    //[7] -> Reparto
                                    //[8] -> Sinopsis



                                    //Consulta para guardar los datos del film en la base de datos
                                    $.post("../modelo/modelo.php",{arrayModificarFilm:arrayModificar,idFilmEncontradoAdmin:idFilmEncontrado,idAdmin:sessionStorage.Id},function(respuestaModificarFilm){
                                        console.log(respuestaModificarFilm);

                                        //Si se modifica el film en la base de datos
                                        if(respuestaModificarFilm=="FILMMODIFICADO"){

                                            //Muestro un mensaje de sweetalert y recargo la pagina:                                            
                                            Swal.fire({
                                                title:'Film modificado correctamente',
                                                icon:'success',
                                                showConfirmButton:false
                                            });
                                            setTimeout(recargarPagina,1700);
                                            //Funcion para recargar pagina usandola en el location reload anterior
                                            function recargarPagina(){
                                                location.reload();
                                            }
                                        }else{//En caso de que hayan errores al editar el film:
                                            Swal.fire({
                                                title:'Ha habido un error al editar el film',
                                                icon:'error',
                                                showConfirmButton:false
                                            });
                                        }

                                    })//Fin consulta para guardar los datos del film en la base de datos


                                })//Fin pulsar boton guardar
   



                            });//Al hacer click en editar el film
                            



                            //Al hacer click en boton cambiar foto:
                            $(".cambiarFotoFilm").on("click",function(){
                                $(".cambiarFotoFilm").hide();
                                $(".upload").show();
                            });//Fin hacer click boton cambiar 
                            
                            //LA SIGUIENTE FUNCION ES PARA CUANDO SELECCIONO LA IMAGEN
                            $("#image").change(function(){
                                //console.log("SELECCIONADA");
                                nombreseleccionado = this.files[0].name;
                                $(".upload").val("Subir archivo: "+ nombreseleccionado);

                            });//FIN CUANDO SELECCIONO LA IMAGEN


                            //Darle click a subir archivo seleccionado (Esto es una vez que le das click a "subir archivo nombrearchivo")
                            $(".upload").on("click",function(){
                                $(".upload").hide();
                                $(".cambiarFotoFilm").show();

                                var formData = new FormData();
                                var files = $("#image")[0].files[0];
                                //console.log(files.name);

                                var nombreArchivoFotoSubirFilmBD = files.name;

                                //AQUI TENGO QUE HACER LA CONSULTA PARA SUBIR EL ARCHIVO
                                formData.append('file',files);
                                $.ajax({
                                    url: '../modelo/modelo.php',
                                    type: 'post',
                                    data: formData,
                                    contentType:false,
                                    processData:false,
                                    success: function(response){
                                        //console.log(response);
                                    }
                                })//FIN SUBIDA DE ARCHIVO A SERVIDOR
                                
                                //Debo cambiarle el nombre de la foto al film en la base de datos
                                $.post("../modelo/modelo.php",{fotoSubirFilm:nombreArchivoFotoSubirFilmBD,idFilmModificarFotoAdmin:idFilmEncontrado,idAdmin:sessionStorage.Id},function(respuestaModificarFoto){
                                        if(respuestaModificarFoto=="FOTOMODIFICADACONEXITO"){
                                            //Muestro un mensaje de sweetalert y recargo la pagina:                                            
                                            Swal.fire({
                                                title:'Foto modificada correctamente',
                                                icon:'success',
                                                showConfirmButton:false
                                            });
                                            setTimeout(recargarPagina,1700);
                                            //Funcion para recargar pagina usandola en el location reload anterior
                                            function recargarPagina(){
                                                location.reload();
                                            }
                                        }
                                });//Fin cambiar nombre foto en el servidor.

                            });//Fin darle click a subir archivo seleccionado

                            //Cuando se pulsa en el botón para editar el país
                            $(".editarPais").on("click",function(){

                                Swal.fire({                                    
                                    html:'<div class="tablaPaises"></div>',
                                    width:'70%',
                                    showConfirmButton:true,
                                })

                                //$(".tablaPaises").html("prueba");
                                //../fotos/paises/us.png

                                //Consulta para obtener la lista de países
                                $.post("../modelo/modelo.php",{obtenerInfoPaisesFilms:"true"},function(respuestaPaisesFilm){

                                    var arrayPaises = JSON.parse(respuestaPaisesFilm);
                                    //console.log(arrayPaises);

                                    var arrayHTML = [];
                                    //Recorro los datos obtenidos de la consulta
                                    for(var i=0;i<arrayPaises.length;i++){
                                        var idpais = arrayPaises[i][0];
                                        var fotopais = arrayPaises[i][2];
                                        var nombrepais = arrayPaises[i][3];

                                        //Creo codigo HTML que insertaré en el mensaje
                                        var codigoHTML = 
                                        `<div class='cadaPais `+idpais+`'>
                                            <div>
                                                <span>
                                                    <img class='fotosPaisesEditarFilm' src="../fotos/paises/`+fotopais+`">
                                                </span>
                                                <span class='nombrepaiscastellano'>`+nombrepais+`</span>    
                                            </div>
                                            
                                            <div class='fotopais'></div>
                                        </div>`;

                                        
                                        arrayHTML.push(codigoHTML);
                                    }//Fin recorro los datos obtenidos de la consulta

                                    //Inserto el html
                                    $(".tablaPaises").html(arrayHTML)


                                    //Al hacer click en un pais de la lista
                                    $(".cadaPais").on("click",function(){
                                        //console.log(this.classList);

                                        //Obtengo el id del pais al que hice click:
                                        var idPaisSeleccionado = this.classList[1];
                                        //console.log(idPaisSeleccionado)

                                        

                                        //Tengo que hacer una consulta para cambiar el país del film:                                        
                                        $.post("../modelo/modelo.php",{idPaisNuevo:idPaisSeleccionado,idFilmModificarPaisAdmin:idFilmEncontrado,idAdmin:sessionStorage.Id},function(respuestaModificarPais){
                                                //console.log(respuestaModificarPais);

                                                if(respuestaModificarPais=="PAISMODIFICADOCONEXITO"){
                                                        //Muestro un mensaje de sweetalert y recargo la pagina:                                            
                                                        Swal.fire({
                                                            title:'País modificado correctamente',
                                                            icon:'success',
                                                            showConfirmButton:false
                                                        });
                                                        setTimeout(recargarPagina,1700);
                                                        //Funcion para recargar pagina usandola en el location reload anterior
                                                        function recargarPagina(){
                                                            location.reload();
                                                        }
                                                }
                                        });//Fin consulta para cambiar el país del film
                                        


                                    })//Fin al hacer click en un pais de la lista
    
                                });//Fin consulta para obtener la lista de países


                              




                            });//Fin cuando se pulsa en el botón para editar el país


                            //Cuando pulso en el boton de eliminar film
                            $(".eliminarFilmAdmin").on("click",function(){
                                Swal.fire({
                                    title:'Deseas eliminar este film?',                                    
                                    showCancelButton:true,
                                    confirmButtonText:'Eliminar',                                    
                                    cancelButtonText:'Cancelar',                                    
                                }).then((result)=>{
                                    if(result.value==true){
                                            //Debo hacer una petición al post para eliminar la pelicula
                                            $.post("../modelo/modelo.php",{eliminarFilmAdmin:"true",idFilmEliminar:idFilmEncontrado,idAdmin:sessionStorage.Id},function(respuestaFilmEliminadoAdmin){
                                                //console.log(respuestaFilmEliminadoAdmin);
                                                if(respuestaFilmEliminadoAdmin=="FILMELIMINADO"){
                                                    //Muestro un mensaje de sweetalert y recargo la pagina:                                            
                                                    Swal.fire({
                                                        title:'Film eliminado correctamente',
                                                        icon:'success',
                                                        showConfirmButton:false
                                                    });
                                                    setTimeout(recargarPagina,1700);
                                                    //Funcion para recargar pagina usandola en el location reload anterior
                                                    function recargarPagina(){
                                                        window.location.href="/listaFilmsAdmin";
                                                    }
                                                }
                                            })
                                    }
                                })
                            });//Fin pulsar boton eliminar film




                        });//Fin consulta de informacion de pelicula por id

                        



            }else{//Si no existe el id:
                //$("#infoFilm").html("<h4><i class='fas fa-exclamation-triangle'></i> No se ha encontrado pelicula con el id: "+idBuscarInfo+"</h4>")
                $("#infoFilm").html("<h4><i class='fas fa-exclamation-triangle peliNoEncontrada'></i> No se ha encontrado pelicula</h4>")
            }//Fin si no existe el id

        });//Fin consulta comprobacion si el id existe o no      
    }//Fin si se intenta acceder solo a "film.php" sin pasarle ningun parámetro


    //Llamo la funcion para que funcione el boton de scroll en el index principal
    $(".subirFilm").on("click",function(){
        scrollTop();
    });//Fin boton subir

   


});//FIN DOCUMENT READY



//Funcion para el boton de scroll hacia arriba (Con anmacion, usar en el boton para subir):
function scrollTop(){
    $("html, body").animate({scrollTop:0},'fast');
    //$("html,body").scrollTop(0);
}//Fin funcion scroll up

//Funcion para obtener de la url el parametro id:
function obtenerIdURL(){    
    var stringUrl =window.location.href;
    var arrayUrl = stringUrl.split("/");
    var parametros =arrayUrl[arrayUrl.length-1]; 
    //console.log(parametros)   ;
    return parametros;
};//Fin funcion obtener de la url el parametro;

//Funcion que se le pasa una fecha en formato MYSQL (YYYY-MM-DD HH:MM:SS) y la devuelve en el formato DD/MM/YYYY HH:MM
function formatearFechaHora(fecha){
   //Formateo la fecha
   var arrayfecha = fecha.split(" ");
   var fechaFormateadaUsuario = arrayfecha[0].split("-");
   var horaFormateadausuario = arrayfecha[1].split(":");
   var fechaformateada =fechaFormateadaUsuario[2]+"/"+fechaFormateadaUsuario[1]+"/"+fechaFormateadaUsuario[0]+" "+horaFormateadausuario[0]+":"+horaFormateadausuario[1];  
    return fechaformateada;

}
