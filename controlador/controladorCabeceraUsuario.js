 //Si se pulsa en cerrar sesión------------------------
 $("#cerrarsesion").on("click",function(){
     cerrarNavbar();
     Swal.fire({
         title: '¿Deseas cerrar la sesión?',
         icon: 'question',
         showCancelButton:true,
         confirmButtonColor:'#3085d6',
         cancelButtonColor:'#d33',
         confirmButtonText: 'Si',
         cancelButtonText: 'No'
     }).then((result)=>{			
         if(result.value){				
             Swal.fire({
                 title:'Sesión cerrada',
                 icon:'success',
                 showConfirmButton:false
             });
             setTimeout(cerrarSesion,1500);
         }
     })		
     function cerrarSesion(){
         sessionStorage.clear();
         window.location.replace("/modelo/logout.php");
     }		
 });//Fin cerrar la sesion----------------------------- 
 
 //Cuando se hace click en el icono "home":
 $("#home").on("click",function(){
     //window.location.replace("/");
     window.location.href="/principal";
 });//Fin hacer click en home.
 
 
 //Cuando se hace click en el icono de "estadisticas"
 $("#estadisticas").on("click",function(){
    //window.location.replace("/estadisticas.php");
    window.location.href="/estadisticas/"+sessionStorage.Nick;
 })//Fin al hacer click en estadisticas

 //Cuando se hace click en el icono del "perfil"
 $("#irPerfil").on("click",function(){
    //window.location.replace("/vistas/perfil.php?nick="+sessionStorage.Nick);
    //window.location.href="perfil.php?nick="+sessionStorage.Nick;
    window.location.href="/perfil/"+sessionStorage.Nick;
 })//Fin al hacer click en perfil

//Cuando se hace click en el icono de "listas"
$("#listafilms").on("click",function(){
    window.location.href="/listafilms";
})//Fin al hacer click en listas

//Cuando se hace click en el icono de "favoritas"
$("#favoritas").on("click",function(){
    window.location.href="/favoritas";
})//Fin al hacer click en favoritas

//Cuando se hace click en el icono de "Mis Votos"
$("#votos").on("click",function(){
    window.location.href="/votos";
})//Fin al hacer click en mis votos

//Cuando se hace click en el icono de "Mis Criticas"
$("#criticas").on("click",function(){
    window.location.href="/criticas";
})//Fin cuando se hace click en el icono de "Mis Criticas"

 //LO QUE REUTILICÉ DEL INDEX PUBLICO:
 
 //Cuando se hace click en el boton buscar(lupa) del buscador. 
 $("#botonBuscador").on("click",function(){    
    cerrarNavbar();
    var textoBuscar = $("#campoTextoBuscador").val();
    //console.log(textoBuscar);

    //Solo realizo busquedas si se introduce algo:
    if(textoBuscar.length>0){
        //Realizo la busqueda de la pelicula por su titulo por titulo:
        buscarPorTitulo(textoBuscar);
    }//Fin si se introduce algo
    //Vacio el campo de texto:
    $("#campoTextoBuscador").val("");
    //igualarStringsVue();
});//Fin hacer click en el boton buscar (lupa)


//Cuando se realiza una busqueda pulsando "Enter"
$("#campoTextoBuscador").keyup(function(evento){
    //Pulsar Enter
    if(evento.which==13){              
        cerrarNavbar();
        var textoBuscar = $("#campoTextoBuscador").val();
        //console.log(textoBuscar);

        //Solo realizo busquedas si se introduce algo:
        if(textoBuscar.length>0){
            //Realizo la busqueda de la pelicula por su titulo por titulo: (Noto una diferencia, pulsando "Enter" se ven las variables Vue {{}})
            buscarPorTitulo(textoBuscar);
        }//Fin si se introduce algo
        //Vacio el campo de texto:
        $("#campoTextoBuscador").val("");
        //igualarStringsVue();
    }//Fin pulsar Enter
    
})//Fin cuando se realiza busqueda pulsando "Enter"





//Cuando se hace click en el input del buscador
$("#campoTextoBuscador").on("click",function(){
        
        //Lo vacío:$("#campoTextoBuscador").val("");     
        $(this).val("");
        
        //Oculto las sugerencias anteriores al pulsar en la caja de texto
        //$("#menuDesplegablePeliculaBuscadaIndex").hide();
        //Si pulso en cualquier sitio del body, oculto las sugerencias
        $("body").on("click",function(){
            $("#menuDesplegablePeliculaBuscadaIndex").hide();            
        });

        //Cada vez que se pulsa una tecla debe ocurrir una sugerencia: 
        $("#campoTextoBuscador").keyup(function(){
            var nombrePeliculaBuscar =  $.trim($(this).val());
            
            //console.log(nombrePeliculaBuscar);

            //Si lo del campo de texto es menor a tres caracteres, oculto el campo.
            if(nombrePeliculaBuscar.length<2){
                $("#menuDesplegablePeliculaBuscadaIndex").hide();
            }

            //Si lo del campo de texto es mayor o igual a tres caracteres realizo la busqueda de sugerencias
            if(nombrePeliculaBuscar.length>=2){

                $.post("../../modelo/modelo.php",{nombrePeliculaSugerir:nombrePeliculaBuscar},function(peliculaSugeridaJSON){
                    //console.log(peliculaSugeridaJSON);
                    //Muestro el menu desplegable
                    $("#menuDesplegablePeliculaBuscadaIndex").show();
                    //Vacio el contenido del menu desplegable (Por si se realizó una busqueda previa)
                    $("#menuDesplegablePeliculaBuscadaIndex").html("");

                    //Creo un array de las peliculas sugeridas
                    var arrayPeliculasSugeridas = JSON.parse(peliculaSugeridaJSON);

                    //console.log(arrayPeliculasSugeridas);
                    //Si no hay coincidencias, se oculta el desplegable:
                    if(arrayPeliculasSugeridas.length==0){
                        $("#menuDesplegablePeliculaBuscadaIndex").hide();
                    }

                    //Por cada pelicula sugerida inserto un html en las sugerencias:
                    //Añado los titulos de las peliculas:
                    //<div class="dropdown-item"><img class="fotoBusqueda" src="fotos/thewire.jpg"/>The Wire</div>
                    for(var i = 0; i<arrayPeliculasSugeridas.length;i++){
                       
                       if(arrayPeliculasSugeridas[i]["fotoportada"].search("https://")!=-1){
                        $("#menuDesplegablePeliculaBuscadaIndex").append('<div class="dropdown-item id-'+arrayPeliculasSugeridas[i]["idfilm"]+'"><img class="fotoBusqueda" src="'+arrayPeliculasSugeridas[i]["fotoportada"]+'"/>'+arrayPeliculasSugeridas[i]["titulo"]+'</div><br>');
                       }else{
                            $("#menuDesplegablePeliculaBuscadaIndex").append('<div class="dropdown-item id-'+arrayPeliculasSugeridas[i]["idfilm"]+'"><img class="fotoBusqueda" src="../../'+arrayPeliculasSugeridas[i]["fotoportada"]+'"/>'+arrayPeliculasSugeridas[i]["titulo"]+'</div><br>');
                       }                     
                       
                }   



                    //Por cada elemento de las sugerencias:
                    $("#menuDesplegablePeliculaBuscadaIndex div").each(function(){
                        //Cuando pulso en el elemento
                        $(this).click(function(){     
                            
                            


                            $("#campoTextoBuscador").val("");
                         
                            //igualarStringsVue();//                         
                            /*Mostrar un div con información de esa pelicula*/
                            cerrarNavbar();
                            scrollTop();            
                         
                            //Oculto y vacio el menu desplegable 
                            //$("#menuDesplegablePeliculaBuscadaIndex").show();//Estaba en show(), no se si me habia equivocado.
                            $("#menuDesplegablePeliculaBuscadaIndex").hide(); 
                            $("#menuDesplegablePeliculaBuscadaIndex").html("");

                            //console.log($(this).text());
                            var tituloSeleccionadoMenu = $(this).text();

                            //console.log(tituloSeleccionadoMenu)

                            //Realizo la busqueda de la pelicula por su titulo por titulo:
                           buscarPorTitulo(tituloSeleccionadoMenu);    
                           
                            
                        });//Fin al pulsar elemento
                    });//Fin por cada elemento de las sugerencias:


                    
                    //$("#menuDesplegablePeliculaBuscadaIndex").append('<div class="dropdown-item">'+peliculaSugeridaJSON+'</div><br>');
                    
                });//Fin busqueda de sugerencias
            }//Fin si el campo es mayor o igual a 3

            //Vacio el menuDesplegable;
            $("#menuDesplegablePeliculaBuscadaIndex").html("");
            //Vacio la variable del nombre:
            nombrePeliculaBuscar="";
        })//Fin sugerencias
    

});//Fin click input buscador


//Funcion que se le pasa el titulo de una pelicula y realiza una consulta al modelo, luego la muestra en el div con Vue
function buscarPorTitulo(titulo){
    
    //Realizo una consulta por título
    $.post("../../modelo/modelo.php",{busquedaPorTitulo:titulo},function(peliculaBuscadaJSON){   
        
        //console.log(peliculaBuscadaJSON);
        //console.log(PELICULA);
        var arrayPeliculaEncontrada = JSON.parse(peliculaBuscadaJSON);        
        //console.log( arrayPeliculaEncontrada[0]["titulo"]);
        
   
          

            //Si se encuentra la pelicula:
            if(arrayPeliculaEncontrada["status"]!="error"){
                     //Consulta de la nota media de la pelicula seleccionada:
                    $.post("../../modelo/modelo.php",{obtenerNotaPeliculaPorId:arrayPeliculaEncontrada[0]["idfilm"]},function(notasPelicula){
                        //console.log(notaMediaPelicula);
                        
                           //Obtengo el JSON con la nota media y el numero de votos
                            var arrayNotasPelicula=JSON.parse(notasPelicula);

                            //console.log(arrayNotasPelicula);

                            var notaPelicula = arrayNotasPelicula[0].toFixed(1);
                            var votosPelicula = arrayNotasPelicula[1];



                       
                        

                        //Aqui creo las variables que obtendran del JSON la informacion de la pelicula (Es lo que le pasare a vue)
                        idEncontrar = arrayPeliculaEncontrada[0]["idfilm"];
                        
                        //window.location.replace("/vistas/film.php?id="+idEncontrar);
                        //window.location.href="film.php?id="+idEncontrar;
                        window.location.href="/film/"+idEncontrar;
                        

                        //tituloEncontrada = arrayPeliculaEncontrada[0]["titulo"];
                        //directorEncontrada = arrayPeliculaEncontrada[0]["director"];
                        //fechaEncontrada = arrayPeliculaEncontrada[0]["fechaestreno"];
                        ////fechaEncontrada=fechaEncontrada.split("-");
                        //fechaEncontrada=fechaEncontrada[0];
                        //duracionEncontrada = arrayPeliculaEncontrada[0]["duracion"];
                        //guionEncontrada = arrayPeliculaEncontrada[0]["guion"];
                        //generoEncontrada = arrayPeliculaEncontrada[0]["genero"];
                        //tipoEncontrada = arrayPeliculaEncontrada[0]["tipo"];
                        //repartoEncontrada = arrayPeliculaEncontrada[0]["reparto"];
                        //sinopsisEncontrada = arrayPeliculaEncontrada[0]["sinopsis"];

                        //portadaEncontrada = arrayPeliculaEncontrada[0]["fotoportada"];

                        //Le añado la foto
                        //$(".peliculaAMostrar").attr("src",portadaEncontrada);

                        //Utilizo Vue para mostrar información de la pelicula: //SI HAGO USO DE ESTO RECORDAR IGUALAR LOS STRINGS DE VUE DESPUES
                        
                        //var appPelicula = new Vue({
                           // el:"#appPelicula",
                           // data:{
                                //tituloPelicula:tituloEncontrada,
                                //directorPelicula:directorEncontrada,
                                //fechaPelicula:fechaEncontrada,
                                //duracionPelicula:duracionEncontrada,
                                //guionPelicula:guionEncontrada,
                                //generoPelicula:generoEncontrada,
                                //tipoPelicula:tipoEncontrada,
                              //  repartoPelicula:repartoEncontrada,
                            //    sinopsisPelicula:sinopsisEncontrada,
                          //      notaPelicula:notaPelicula,                                        
                        //        numeroVotosPelicula:votosPelicula
                        //    }
                        //})//FIN VUE
                        
                        //Muestro el div de VUE (Si hago esto antes de declarar las variables de Vue, por unos milisegundos se puede apreciar {{variables}} en el navegador)
                        //$("#appPelicula").show();                    
              
                        

                });//Fin consulta nota media 
                
                
        }else{//Si no se encuentra la pelicula:
                    //METER AQUI MEJOR UN DIV QUE INDIQUE QUE NO SE HA ENCONTRADO LA BUSQUEDA:
                    //window.location.replace("/vistas/filmNoEncotnrado.php");
                    //window.location.href="./filmNoEncotnrado.php";
                    
                    //window.location.href="film.php?id=-1";
                    window.location.href="/film/-1";

                    
                    //$("#appPelicula").hide();
                    //$("#peliculaNoEncontrada").show();
                    
                    //$("#peliculaNoEncontrada").html("<h4><i class='fas fa-search'></i><br><br>No se ha encontrado la película con el título '"+titulo+"'<br><br><button type='button' class='btn btn-lg colorAzul botonVolverContenidoPublico'><i class='fas fa-arrow-left'></i></button><br><br></h4>");
    
                    //Al darle al boton volver al contenido publico
                    //$(".botonVolverContenidoPublico").on("click",function(){
                        //Oculto las peliculas, y muestro informacion sobre la pelicula pulsada
                    //    cerrarNavbar();
                    //    scrollTop();
                    //    $("#peliculaNoEncontrada").hide();
                    //    $("#contenidoPublico").show();
                    //    $("#appPelicula").hide();

                        //NO HAGO location.reload();
                    //    //Vuelvo a igualar los strings de Vue (ESTO ES IMPORTANTE, EVITO HACER UN LOCATION RELOAD)                    
                    //   igualarStringsVue();
                    //});//Fin darle boton volver contenido publico
                               
        }//Fin if

      
});//Fin consulta por titulo


};//Fin funcion que se le pasa el titulo de una pelicula y realiza una consulta al modelo, luego la muestra en el div con Vue

//Funcion para el boton de scroll hacia arriba (Con anmacion, usar en el boton para subir):
function scrollTop(){
    $("html, body").animate({scrollTop:0},'fast');
    //$("html,body").scrollTop(0);
}//Fin funcion scroll up


//Funcion para el boton de scroll hacia arriba inmediato (Sin animación, util para "cambios de páginas" desde el navbar):
function scrollTopFast(){
    $("html,body").scrollTop(0);
}//Fin funcion scroll up


//Funcion que cierra la navbar (Cuando tiene el boton desplegar en pantallas pequeñas)
function cerrarNavbar(){ 
    $('.navbar-collapse').collapse('hide');
}//Fin funcion cerrar navbar
