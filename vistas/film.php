<!--INICIO DE SESION------------------------------------------------>
<?php
        //Inicio la sesion
        session_start();
        //Si no hay variables de session
    
        if(count($_SESSION)==0){//Si no hay una sesion iniciada:
            //Destruyo la sesion
            session_destroy();
            //Redirijo al index
            header("location:/");
        }else if(count($_SESSION)>0){//Si intenta acceder un administrador, se redirige a su index:
            if($_SESSION["admin"]=="1"){
                //header("location:/vistas/indexAdmin.php");
                header("location:/administracion");
            }
        }
?> 
<!--FIN INICIO DE SESION--------------------------------------------> 

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <!--<meta name="viewpoint" content="width=device-width, initial-scale=1, syrink-to-fit=no">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FilmRate</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!--FontAwesomCSS-->
    <script src="https://kit.fontawesome.com/bd632f581b.js" crossorigin="anonymous"></script>
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sh....-->
    <!--SWEETALERT2-->
    <link rel="stylesheet" href="../plugins/sweetalert2/sweetalert2.min.css">
 
    <!--Estilos-->
    <link rel="stylesheet" type="text/css" href="../css/estilos.css?v=1.1"/><!--PRUEBAS, lo dejare asi por un tiempo, luego lo cambio-->
    <!--<link rel="stylesheet" type="text/css" href="css/estilos.css"/>-->
    <!--<meta http-equiv="cache-control" content="no-cache"/>ESTO NO HA FUNCIONADO-->
    
</head>
<body>
    <!--Llamo a las librerias---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="../jquery/jquery.js"></script>
    <!--Popper-->   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <!--BOOTSTRAP-->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!--CDN de Bootstrap-->
    <!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    -->
    <!-- CDN de Vue -->
    <!--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>-->   
    <!-- <script src="../plugins/vue.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

    <!-- CDN de Axios -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
    <!--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>-->      
    <!--SWEETALERT2-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>-->
    <script src="../plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
     
    
    <!--LLAMADA AL CONTROLADOR (TIENE QUE IR DESPUES DE LAS LIBRERIAS)-->
    <script src="../controlador/controladorFilm.js"></script>
    
    
    <?php         
        require "./cabeceras/cabeceraUsuario.php";
        //El siguiente div estara oculto, tiene información del usuario que inicia sesion:
        //echo("<div id='infosesion' style='background-color:red;color:white;display:none'>{'id':'".$_SESSION["idusuario"]."','nick':'".$_SESSION["nick"]."','email':'".$_SESSION["email"]."','nombre':'".$_SESSION["nombre"]."','apellidos':'".$_SESSION["apellidos"]."','admin':'".$_SESSION["admin"]."'}</div>");
        //echo("<div id='infosesion' style='background-color:red;color:white;display:none'>".$_SESSION["idusuario"].",".$_SESSION["nick"].",".$_SESSION["email"].",".$_SESSION["nombre"].",".$_SESSION["apellidos"].",".$_SESSION["admin"]."</div>");
    ?>

        <div id="menuVotar">
            <i id="cerrarVotos" class="fa fa-close"></i>
            <div id="nota1">1:&nbsp<i class="fas fa-star"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i></div>            
            <div id="nota2">2:&nbsp<i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i></div>            
            <div id="nota3">3:&nbsp<i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i></div>            
            <div id="nota4">4:&nbsp<i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i></div>            
            <div id="nota5">5:&nbsp<i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i></div>            
            <div id="nota6">6:&nbsp<i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i></div>            
            <div id="nota7">7:&nbsp<i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i></div>            
            <div id="nota8">8:&nbsp<i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star estrellaOscura"></i><i class="fas fa-star estrellaOscura"></i></div>            
            <div id="nota9">9:&nbsp<i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star estrellaOscura"></i></div>            
            <div id="nota10">10:&nbsp<i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>            

        </div>

        <!--<div  id="inicioUsuario" class="container">-->
        <div  id="infoFilm"  class="container">
            <div id="appFilm">
                <h1 id="tituloPelicula">{{tituloFilm}} <i id="btnFav" class="fa fa-star estrellaFavoritaGris"></i></h1>
                <!--
                <div id="contenedor">
                    <div id="imagenFilm"><img id="portadaFilm"></img></div>

                    <div class="contenedorFilmVotos">
                        <div class="notaPeliculaFilmVue">{{notaPelicula}}</div><br>
                        <div class="numeroVotosFilmVue">{{numeroVotosPelicula}}<br>votos</div><br>
                        <div class="numeroVotosFilmVue">{{numeroVotosPelicula}}<br>votos</div>
                    </div>
                </div>
                -->


                <div id="contenedor">
                        <div id="uno"><img id="portadaFilm"></img></div>
                        <div id="dos">           
                            Nota media:                   
                            <div class="notaPeliculaFilmVue">{{notaPelicula}}</div>                            
                            Votos:
                            <div class="numeroVotosFilmVue">{{numeroVotosPelicula}}</div>                            
                            Tu nota:
                            <div class="tuNota">
                                
                                {{notaDelUsuario}} 
                            </div>                      
                        </div>
                </div>             


                <i><h5><strong class="tipoPeliculaVue">{{tipoPelicula}}</strong></h5></i>          
                <div id='contenidoInfoPelicula'>
                    <p id='contenedorPais'><strong>Pais: </strong><span id="idfotopais"></span></p>
                    <p><strong>Director/es: </strong> <span class="directorPeliculaVue">&nbsp;{{directorPelicula}}</span> </p>
                    <p><strong>Año: </strong> <span class="fechaPeliculaVue">&nbsp;{{fechaPelicula}}</span> </p>
                    <p><strong>Duración: </strong> <span class="duracionPeliculaVue">&nbsp;{{duracionPelicula}}</span> min </p>
                    <p><strong>Guión: </strong> <span class="guionPeliculaVue">&nbsp;{{guionPelicula}}</span></p>
                    <p><strong>Género: </strong> <span class="generoPeliculaVue">&nbsp;{{generoPelicula}}</span></p>
                    <p><strong>Reparto: </strong> <span class="repartoPeliculaVue">&nbsp;{{repartoPelicula}}</span></p>
                    <p><strong>Sinopsis: </strong> <span class="sinopsisPeliculaVue">&nbsp;{{sinopsisPelicula}}</span></p>
                </div>
           
            </div>
            
            <!--Criticas de los usuarios-->
            <h5 id='tituloCriticas'><strong ><br>Algunas de las críticas de esta película<br><br></strong></h5>            
            <div id="criticasDeUsuarios"></div>

            <!--Tu critica-->
            <div id='tuCritica'>                
                <h5 id='tituloCriticaUsuario'></h5>
            </div>
           
            <div id='tuPrimeraCritica'>
                <h5><strong>Escribe tu primera crítica<i class='fas fa-pencil-alt botonEscribirCritica'></i></strong></h5>

            </div>

            <br>
            <button type="button" class="btn btn-lg colorAzul subirFilm"><i class="fas fa-arrow-up"></i></button>           
        </div>

        



        <!--EJEMPLO-->
        <!--
        <div id="appPelicula" class="container text-dark shadow bg-light text-left">           
            
            <h1 class="tituloPeliculaVue">{{tituloPelicula}}</h1>
            <img class="peliculaAMostrar"/><div class="contenedorNotasVotos"><div class="notaPeliculaAMostrarVue">{{notaPelicula}}</div><br><div class="numeroVotosAMostrarVue">{{numeroVotosPelicula}}<br>votos</div></div>
            <br>
            <button type="button" class="btn btn-lg colorAzul votarPeliculaPublica">Votar Película</button>
            <br>
            <br>

            <i><h5><strong class="tipoPeliculaVue">{{tipoPelicula}}</strong></h5></i>          
            <p><strong>Director/es: </strong> <span class="directorPeliculaVue">&nbsp;{{directorPelicula}}</span> </p>
            <p><strong>Año: </strong> <span class="fechaPeliculaVue">&nbsp;{{fechaPelicula}}</span> </p>
            <p><strong>Duración: </strong> <span class="duracionPeliculaVue">&nbsp;{{duracionPelicula}}</span> min </p>
            <p><strong>Guión: </strong> <span class="guionPeliculaVue">&nbsp;{{guionPelicula}}</span></p>
            <p><strong>Género: </strong> <span class="generoPeliculaVue">&nbsp;{{generoPelicula}}</span></p>
            <p><strong>Reparto: </strong> <span class="repartoPeliculaVue">&nbsp;{{repartoPelicula}}</span></p>
            <p><strong>Sinopsis: </strong> <span class="sinopsisPeliculaVue">&nbsp;{{sinopsisPelicula}}</span></p>
            
            <button type="button" class="btn btn-lg colorAzul botonVolverContenidoPublico"><i class="fas fa-arrow-left"></i></button>           
            
            <button type="button" class="btn btn-lg colorAzul criticaPeliculaPublica">Escribe Una Crítica</button>
            <button type="button" class="btn btn-lg colorAzul subir"><i class="fas fa-arrow-up"></i></button>           
        </div>
        -->

    





        
    <?php
        require "./footer/footer.php";
    ?> 
   
</body>

</html>
