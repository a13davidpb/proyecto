<!--INICIO DE SESION------------------------------------------------>
<?php
        
        //Inicio la sesion
        session_start();
        //Si no hay variables de session
    
        if(count($_SESSION)==0){//Si no hay una sesion iniciada:
            //Destruyo la sesion
            session_destroy();
            //Redirijo al index
            header("location:/");
        }else if(count($_SESSION)>0){//Si intenta acceder un administrador, se redirige a su index:
            if($_SESSION["admin"]!="1"){
                //header("location:/vistas/indexAdmin.php");
                header("location:/");
            }
        }
?> 
<!--FIN INICIO DE SESION--------------------------------------------> 

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <!--<meta name="viewpoint" content="width=device-width, initial-scale=1, syrink-to-fit=no">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FilmRate</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!--FontAwesomCSS-->
    <script src="https://kit.fontawesome.com/bd632f581b.js" crossorigin="anonymous"></script>
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sh....-->
    <!--SWEETALERT2-->
    <link rel="stylesheet" href="../plugins/sweetalert2/sweetalert2.min.css">
 
    <!--Estilos-->
    <link rel="stylesheet" type="text/css" href="../css/estilos.css?v=1.1"/><!--PRUEBAS, lo dejare asi por un tiempo, luego lo cambio-->
    <!--<link rel="stylesheet" type="text/css" href="css/estilos.css"/>-->
    <!--<meta http-equiv="cache-control" content="no-cache"/>ESTO NO HA FUNCIONADO-->
    
        <!--LIBRERIA DE CHARTJS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>



</head>
<body>
    <!--Llamo a las librerias---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="../jquery/jquery.js"></script>
    <!--Popper-->   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <!--BOOTSTRAP-->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!--CDN de Bootstrap-->
    <!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    -->
    <!-- CDN de Vue -->
    <!--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>-->   
    <!-- <script src="../plugins/vue.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

    <!-- CDN de Axios -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
    <!--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>-->      
    <!--SWEETALERT2-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>-->
    <script src="../plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
     
    
    <!--LLAMADA AL CONTROLADOR (TIENE QUE IR DESPUES DE LAS LIBRERIAS)-->
    <script src="../controlador/controladorCrearUsuariosAdmin.js"></script>
    
    
    <?php         
        require "./cabeceras/cabeceraAdmin.php";
        //El siguiente div estara oculto, tiene información del usuario que inicia sesion:
        //echo("<div id='infosesion' style='background-color:red;color:white;display:none'>{'id':'".$_SESSION["idusuario"]."','nick':'".$_SESSION["nick"]."','email':'".$_SESSION["email"]."','nombre':'".$_SESSION["nombre"]."','apellidos':'".$_SESSION["apellidos"]."','admin':'".$_SESSION["admin"]."'}</div>");
        //echo("<div id='infosesion' style='background-color:red;color:white;display:none'>".$_SESSION["idusuario"].",".$_SESSION["nick"].",".$_SESSION["email"].",".$_SESSION["nombre"].",".$_SESSION["apellidos"].",".$_SESSION["admin"]."</div>");
    ?>

    
    <div  id="crearUsuarioAdmin" class="container">
            <h2 id='tituloListaUsuarios'>Crear Usuario</h2>   
           
            <div id="formularioCrearUsuario">
                <h5>Nick: </h5> 
                    <input id='inputNick' type="text"></input><br><br>
                <h5>Email: </h5> 
                    <input id='inputEmail' type="email"></input><br><br>
                <h5>Contraseña: </h5> 
                    <input id='inputPassword' type="password"></input><br><br>
                <h5>Nombre: </h5>
                    <input id='inputNombre' type="text"></input><br><br>
                <h5>Apellidos: </h5> 
                    <input id='inputApellidos' type="text"></input><br><br>
                <h5 class='tituloInputRadio'>Tipo de cuenta: </h5>
                <div class="inputRadio">
                    <input class="tipoCuenta" type="radio" id="usuario" name="tipoUsuario" value="0" checked="checked">
                    <label for="usuario">Usuario</label>
                    <input class="tipoCuenta" type="radio" id="administrador" name="tipoUsuario" value="1">
                    <label for="administrador">Administrador</label>
                    
                </div><br><br>
                <h5>Cuenta activada<input type="checkbox" id="cuentaActivada" name="activada" value="1"></h5>
                
            </div>
            <div class="botonCrearUsuario">Crear</div>


    
    </div>
        
    <?php
        require "./footer/footer.php";
    ?> 
   
</body>

</html>
