<?php

echo '

<footer id="sticky-footer" class="p-2 bg-dark text-white-50 footer">
    <div class="container text-center">
    <small>Sitio hecho por David Pérez (Proyecto DAW 2018-2020)<br>FilmRate es una página de recomendación de películas y series, sin ánimo de lucro para el proyecto de fin de curso.</small>
    <br><em class="lopd"><a href="https://www.boe.es/buscar/act.php?id=BOE-A-2018-16673">Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales (LOPDPGDD)</em>
    </div>
</footer>


';






?>