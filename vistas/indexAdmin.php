<!--INICIO DE SESION------------------------------------------------>
<?php
        //Inicio la sesion
        session_start();
        //Si no hay variables de session
        if(count($_SESSION)==0){//Si no hay una sesion iniciada:
            //Destruyo la sesion
            session_destroy();
            //Redirijo al index
            header("location:/");
        }else if(count($_SESSION)>0){//Si intenta acceder un administrador, se redirige a su index:
            if($_SESSION["admin"]!="1"){
                //header("location:/vistas/indexUsuario.php");
                header("location:/");
            }
        }
        
?> 
<!--FIN INICIO DE SESION--------------------------------------------> 

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <!--<meta name="viewpoint" content="width=device-width, initial-scale=1, syrink-to-fit=no">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FilmRate</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!--FontAwesomCSS-->
    <script src="https://kit.fontawesome.com/bd632f581b.js" crossorigin="anonymous"></script>
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sh....-->
    <!--SWEETALERT2-->
    <link rel="stylesheet" href="../plugins/sweetalert2/sweetalert2.min.css">
 
    <!--Estilos-->
    <link rel="stylesheet" type="text/css" href="../css/estilos.css?v=1.1"/><!--PRUEBAS, lo dejare asi por un tiempo, luego lo cambio-->
    <!--<link rel="stylesheet" type="text/css" href="css/estilos.css"/>-->
    <!--<meta http-equiv="cache-control" content="no-cache"/>ESTO NO HA FUNCIONADO-->
    
</head>
<body>
    <!--Llamo a las librerias---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="../jquery/jquery.js"></script>
    <!--Popper-->   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <!--BOOTSTRAP-->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!--CDN de Bootstrap-->
    <!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    -->
    <!-- CDN de Vue -->
    <!--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>-->   
    <!-- <script src="../plugins/vue.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

    <!-- CDN de Axios -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
    <!--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>-->      
    <!--SWEETALERT2-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>-->
    <script src="../plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
     
    
    <!--LLAMADA AL CONTROLADOR (TIENE QUE IR DESPUES DE LAS LIBRERIAS)-->
    <script src="../controlador/controladorIndexAdmin.js"></script>
    

    
    <?php         
        require "./cabeceras/cabeceraAdmin.php";
        //echo("<div id='infosesion' style='background-color:red;color:white;display:none'>".$_SESSION["idusuario"].",".$_SESSION["nick"].",".$_SESSION["email"].",".$_SESSION["nombre"].",".$_SESSION["apellidos"].",".$_SESSION["admin"]."</div>");
    ?>

    <div  id="inicioAdmin" class="container">
        <h3 class='mensajeBienvenidaAdmin'>Bienvenido Administrador</h3>

        <div class="opcionesAdmin">
            <!--Icono Administrar Films-->
            <div class="opcionAdmin filmsAdmin">
                <i class="fas fa-film claseOpcionAdmin"></i>
                <p class="textoOpcionAdmin">Lista Films</p>
            </div>
            <!--Icono Administrar Usuarios-->
            <div class="opcionAdmin usuariosAdmin">
                <i class="fas fa-user-friends claseOpcionAdmin"></i>
                <p class="textoOpcionAdmin">Lista Usuarios</p>
            </div>
            <!--Icono Ver Paises-->
            <!--<div class="opcionAdmin verEstadisticasAdmin">
                <i class="fas fa-chart-bar claseOpcionAdmin"></i>
                <p class="textoOpcionAdmin">Estadísticas</p>
            </div>-->
            <!--Icono Agregar Film-->
            <div class="opcionAdmin agregarFilmAdmin">
                <i class="fas  fa-film claseOpcionAdmin"></i>
                <i class="fas  fa-plus claseOpcionAdminPlus"></i>
                <p class="textoOpcionAdmin">Añadir Film</p>
            </div>
            <!--Icono Agregar Usuario-->
            <div class="opcionAdmin agregarUsuarioAdmin">
                <i class="fas fa-user claseOpcionAdmin"></i>
                <i class="fas  fa-plus claseOpcionAdminPlus"></i>
                <p class="textoOpcionAdmin">Añadir Usuario</p>
            </div>
            
        </div>

    </div>
    
        
    <?php
        require "./footer/footer.php";
    ?> 
   
</body>

</html>