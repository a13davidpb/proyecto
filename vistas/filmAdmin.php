<!--INICIO DE SESION------------------------------------------------>
<?php
        //Inicio la sesion
        session_start();
        //Si no hay variables de session
    
        if(count($_SESSION)==0){//Si no hay una sesion iniciada:
            //Destruyo la sesion
            session_destroy();
            //Redirijo al index
            header("location:/");
        }else if(count($_SESSION)>0){//Si intenta acceder un administrador, se redirige a su index:
            if($_SESSION["admin"]!="1"){
                //header("location:/vistas/indexAdmin.php");
                header("location:/");
            }
        }
?> 
<!--FIN INICIO DE SESION--------------------------------------------> 

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <!--<meta name="viewpoint" content="width=device-width, initial-scale=1, syrink-to-fit=no">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FilmRate</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!--FontAwesomCSS-->
    <script src="https://kit.fontawesome.com/bd632f581b.js" crossorigin="anonymous"></script>
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sh....-->
    <!--SWEETALERT2-->
    <link rel="stylesheet" href="../plugins/sweetalert2/sweetalert2.min.css">
 
    <!--Estilos-->
    <link rel="stylesheet" type="text/css" href="../css/estilos.css?v=1.1"/><!--PRUEBAS, lo dejare asi por un tiempo, luego lo cambio-->
    <!--<link rel="stylesheet" type="text/css" href="css/estilos.css"/>-->
    <!--<meta http-equiv="cache-control" content="no-cache"/>ESTO NO HA FUNCIONADO-->
    
</head>
<body>
    <!--Llamo a las librerias---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="../jquery/jquery.js"></script>
    <!--Popper-->   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <!--BOOTSTRAP-->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!--CDN de Bootstrap-->
    <!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    -->
    <!-- CDN de Vue -->
    <!--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>-->   
    <!-- <script src="../plugins/vue.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

    <!-- CDN de Axios -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
    <!--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>-->      
    <!--SWEETALERT2-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>-->
    <script src="../plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
     
    
    <!--LLAMADA AL CONTROLADOR (TIENE QUE IR DESPUES DE LAS LIBRERIAS)-->
    <script src="../controlador/controladorFilmAdmin.js"></script>
    
    
    <?php         
        require "./cabeceras/cabeceraAdmin.php";
        //El siguiente div estara oculto, tiene información del usuario que inicia sesion:
        //echo("<div id='infosesion' style='background-color:red;color:white;display:none'>{'id':'".$_SESSION["idusuario"]."','nick':'".$_SESSION["nick"]."','email':'".$_SESSION["email"]."','nombre':'".$_SESSION["nombre"]."','apellidos':'".$_SESSION["apellidos"]."','admin':'".$_SESSION["admin"]."'}</div>");
        //echo("<div id='infosesion' style='background-color:red;color:white;display:none'>".$_SESSION["idusuario"].",".$_SESSION["nick"].",".$_SESSION["email"].",".$_SESSION["nombre"].",".$_SESSION["apellidos"].",".$_SESSION["admin"]."</div>");
    ?>

        <!--<div  id="inicioUsuario" class="container">-->
        <div  id="infoFilm"  class="container">
            <div id="appFilm">
                <h1 id="tituloPelicula" class="editar editada0">{{tituloFilm}}<i class="fas fa-trash eliminarFilmAdmin"></i></h1>

                <div class="opcionesFilmAdmin">
                    <div class="editarFilm">Editar Film &nbsp </div>
                   
                    <div style="display:inline" class="form-group">
                        <label for="image" class="cambiarFotoFilm">Foto Portada &nbsp <i class="fas fa-camera"></i></label>
                        <input id="image" style="display:none" class="form-control-file" name="image" type="file">
                        <input style="display:none" type="button" class="btn btn-primary upload" value ="Subir archivo:">
                    </div>

                    <div class="guardar" style="display:none">Guardar Cambios &nbsp <i class="fas fa-check"></i></div><div class="cancelar" style="display:none">Cancelar &nbsp <i class="fas fa-ban"></i></div>
                </div>


                <div id="contenedor">
                        <div id="uno"><img id="portadaFilm"></img></div>                      
                </div>             


                <i><h5><strong class="tipoPeliculaVue editar editada1">{{tipoPelicula}}</strong></h5></i>          
                <div id='contenidoInfoPelicula'>
                    <p id='contenedorPais'><strong>Pais: </strong><span id="idfotopais"></span>  <span class="editarPais">Editar País  &nbsp <i class="fas fa-pencil-alt"></i></span><!-- <span class="infoEditarPais">*Para editar el país, es necesario saber el código del país.</span>--></p>
                    <p><strong>Director/es: </strong> <span class="directorPeliculaVue editar editada2">&nbsp;{{directorPelicula}}</span> </p>
                    <p><strong>Año: </strong> <span class="fechaPeliculaVue editar editada3">&nbsp;{{fechaPelicula}}</span> </p>
                    <p><strong>Duración: </strong> <span class="duracionPeliculaVue editar editada4">&nbsp;{{duracionPelicula}}</span> min </p>
                    <p><strong>Guión: </strong> <span class="guionPeliculaVue editar editada5">&nbsp;{{guionPelicula}}</span></p>
                    <p><strong>Género: </strong> <span class="generoPeliculaVue editar editada6">&nbsp;{{generoPelicula}}</span></p>
                    <p><strong>Reparto: </strong> <span class="repartoPeliculaVue editar editada7">&nbsp;{{repartoPelicula}}</span></p>
                    <p><strong>Sinopsis: </strong> <span class="sinopsisPeliculaVue editar editada8">&nbsp;{{sinopsisPelicula}}</span></p>
                </div>
           
            </div>
         

            <br>
            <button type="button" class="btn btn-lg colorAzul subirFilm"><i class="fas fa-arrow-up"></i></button>           
        </div>

        



        <!--EJEMPLO-->
        <!--
        <div id="appPelicula" class="container text-dark shadow bg-light text-left">           
            
            <h1 class="tituloPeliculaVue">{{tituloPelicula}}</h1>
            <img class="peliculaAMostrar"/><div class="contenedorNotasVotos"><div class="notaPeliculaAMostrarVue">{{notaPelicula}}</div><br><div class="numeroVotosAMostrarVue">{{numeroVotosPelicula}}<br>votos</div></div>
            <br>
            <button type="button" class="btn btn-lg colorAzul votarPeliculaPublica">Votar Película</button>
            <br>
            <br>

            <i><h5><strong class="tipoPeliculaVue">{{tipoPelicula}}</strong></h5></i>          
            <p><strong>Director/es: </strong> <span class="directorPeliculaVue">&nbsp;{{directorPelicula}}</span> </p>
            <p><strong>Año: </strong> <span class="fechaPeliculaVue">&nbsp;{{fechaPelicula}}</span> </p>
            <p><strong>Duración: </strong> <span class="duracionPeliculaVue">&nbsp;{{duracionPelicula}}</span> min </p>
            <p><strong>Guión: </strong> <span class="guionPeliculaVue">&nbsp;{{guionPelicula}}</span></p>
            <p><strong>Género: </strong> <span class="generoPeliculaVue">&nbsp;{{generoPelicula}}</span></p>
            <p><strong>Reparto: </strong> <span class="repartoPeliculaVue">&nbsp;{{repartoPelicula}}</span></p>
            <p><strong>Sinopsis: </strong> <span class="sinopsisPeliculaVue">&nbsp;{{sinopsisPelicula}}</span></p>
            
            <button type="button" class="btn btn-lg colorAzul botonVolverContenidoPublico"><i class="fas fa-arrow-left"></i></button>           
            
            <button type="button" class="btn btn-lg colorAzul criticaPeliculaPublica">Escribe Una Crítica</button>
            <button type="button" class="btn btn-lg colorAzul subir"><i class="fas fa-arrow-up"></i></button>           
        </div>
        -->

    





        
    <?php
        require "./footer/footer.php";
    ?> 
   
</body>

</html>
