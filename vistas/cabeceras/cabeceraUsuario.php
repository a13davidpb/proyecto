
<nav class="navbar shadow sticky-top navbar-expand-md navbar-dark bg-dark">
    <!--LOGO-->
    <div id="divMarca">
        <!-- <a id="marca" href="/vistas/indexUsuario.php" class="navbar-brand">F<span class="letraPequena">ilm</span>R<span class="letraPequena">ate</span></a>-->
        <a id="marca" href="/principal" class="navbar-brand">F<span class="letraPequena">ilm</span>R<span class="letraPequena">ate</span></a>
    </div>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    
    <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
        <div class="navbar-nav">
            <!--<a href="#" class="nav-item nav-link active">Home</a>-->
            <!--<a href="#" class="nav-item nav-link">Profile</a>-->
            <!--<div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Messages</a>
                <div class="dropdown-menu">
                    <a href="#" class="dropdown-item">Inbox</a>
                    <a href="#" class="dropdown-item">Sent</a>
                    <a href="#" class="dropdown-item">Drafts</a>
                </div>
            </div>-->
        </div>
        <!--<form class="form-inline">-->
        <!--<div class="mr-2 my-auto d-inline-block order-0">
            <div class="input-group">                    
                <input id="campoTextoBuscador" type="text" class="form-control" placeholder="Search">
                <div class="input-group-append">
                    <button id="botonBuscador" type="button" class="btn btn-secondary"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>-->


        <div class="mr-2 my-auto d-inline-block order-0">
            <div  id="buscador" class="input-group">
                <div class="dropdown">
                
                <input spellcheck="false" id="campoTextoBuscador" type="text" class="form-control border border-right-0 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" placeholder="">
                
                    <!--Desplegable-->
                        <div id="menuDesplegablePeliculaBuscadaIndex" class="dropdown-menu scrollable-menu" aria-labelledby="campoTextoBuscador">
                            <!--<div class="dropdown-item"><img class="fotoBusqueda" src="fotos/breakingbad.jpg"/>Breaking Bad</div>
                            <div class="dropdown-item"><img class="fotoBusqueda" src="fotos/thewire.jpg"/>The Wire</div>
                            <div class="dropdown-item"><img class="fotoBusqueda" src="fotos/donniedarko.jpg"/>Donnie Darko</div>-->
                        </div>

                    </div>


                <span class="input-group-append">
                    <button id="botonBuscador" class="btn btn-outline-light border border-left-0" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                
            </div>
            
            
        </div>
        


        <!--Opciones principales de la barra de navegacion-->
        <div id="opciones" class="navbar-nav">
        <a id="" title=" " class="nav-item nav-link text-light"><i></i></a>            
                
            <!--<a id="perfil" title="Perfil" class="nav-item nav-link text-light"><span id="spanNickUsuario"></span> <i  id='perfilUsuarioIcono' class="fas fa-user"></i></a>            -->
            

            <a id="irPerfil" title="Mi Perfil" class="nav-item nav-link text-light"><span id="spanNickUsuario"></span><i class="fas fa-user"></i></a></a>            
            <a  title="Inicio" class="nav-item nav-link text-light"><i id="home" class="fa fa-home"></i></a>            
            <a  title="Cerrar Sesión" class="nav-item nav-link text-light"><i id="cerrarsesion" class="fa fa-door-open"></i></a>            
            
        </div>
    </div>
</nav>

<!--BARRA DE OPCIONES -->
<!--<div id="barraLinks" class="container-fluid text-center">-->
<div id="barraLinksUsuario" class="container-fluid shadow">
<!--<div id="barraLinks" class="d-flex">-->
  <!--<a href="/"></a>-->
  <!--<div><a href="/"></a></div>-->
    <div class='iconoscabecera'>
        <div><i title="Peliculas" id="listafilms" class="fas fa-list"></i></div>
        <div><i title="Mis Favoritas" id="favoritas" class="fa fa-star"></i></div>
        <div><i title="Mis Votos"  id="votos" class="fa fa-vote-yea"></i></div>
        <div><i title="Mis Críticas" id="criticas" class="fa fa-comment-alt"></i></div>
        
        <!--<i class="fas fa-list-ul"></i>-->
        <div><i title='Estadísticas' id="estadisticas" class="far fa-chart-bar"></i></div>
    </div>
</div>

<script src="./../../controlador/controladorCabeceraUsuario.js"></script>

