<?php

require_once 'conexion.php';


/*EJEMPLO DE CONSULTA
$consultaPrueba = $pdo->prepare("SELECT * FROM FilmRate.usuarios");
$consultaPrueba->execute();
while($fila=$consultaPrueba->fetch()){
        $usuarios = $fila; //Esto se puede meter en un array
        echo print_r($usuarios);
}
*/

//CONSULTA PARA EL INDEX PUBLICO PARA MOSTRAR FOTOS DE PORTADA(controladorIndex.js)
if(isset($_POST["consultaPeliculasPaginaPrincipal"])){
    $consultaPrueba = $pdo->prepare("SELECT * FROM FilmRate.films");
    $consultaPrueba->execute();
    $arrayPeliculas=array();

    while($fila=$consultaPrueba->fetch()){
            array_push($arrayPeliculas,$fila); //Esto se puede meter en un array          
    }

    $arrayPeliculasJSON = json_encode($arrayPeliculas);
    echo($arrayPeliculasJSON);
}//FIN CONSULTA PARA EL INDEX PUBLICO PARA MOSTRAR FOTOS DE PORTADA(controladorIndex.js)

//CONSULTA QUE OBTIENE TODA LA INFORMACION DE UNA PELICULA A PARTIR DEL ID (controladorIndex.js)
if(isset($_POST["idPeliculaPublica"])){
        $idIntroducido = $_POST["idPeliculaPublica"];

        $consulta = $pdo->prepare("SELECT * FROM FilmRate.films where idfilm=$idIntroducido");
        $consulta->execute();
        $infoPelicula=array();
    
        while($fila=$consulta->fetch()){
                array_push($infoPelicula,$fila); //Esto se puede meter en un array          
        }
    
        $infoPeliculaJSON = json_encode($infoPelicula);
        //Devuelvo un JSON con informacion de la pelicula
        echo($infoPeliculaJSON);

}//CONSULTA QUE OBTIENE TODA LA INFORMACION DE UNA PELICULA PARA EL INDEX PUBLICO


//BUSQUEDA DE PELICULAS PAGINA PRINCIPAL (controladorIndex.js)
if(isset($_POST["nombrePeliculaSugerir"])){
        $tituloPeliculaBuscar = $_POST["nombrePeliculaSugerir"];
        $stmt=$pdo->prepare("select * from FilmRate.films where titulo like :busqueda");
        $stmt->bindValue(':busqueda','%'.$tituloPeliculaBuscar.'%');
        $stmt->execute();
        $arrayPeliculasSugeridas = array();

        while($fila=$stmt->fetch()){
                array_push($arrayPeliculasSugeridas,$fila);
        }
        $peliculasSugeridasJSON = json_encode($arrayPeliculasSugeridas);
        echo ($peliculasSugeridasJSON);
        
}//FIN BUSQUEDA DE PELICULAS PAGINA PRINCIPAL

//BUSQUEDA DE PELICULAS POR TITULO (controladorIndex.js)
if(isset($_POST["busquedaPorTitulo"])){
        $titulo = $_POST["busquedaPorTitulo"];
        
        
        $consulta = $pdo->prepare("SELECT * FROM FilmRate.films where titulo='$titulo'");
        $consulta->execute();
        $infoPelicula=array();
    
        while($fila=$consulta->fetch()){
                array_push($infoPelicula,$fila); //Esto se puede meter en un array          
        }
    

        if(count($infoPelicula)!=0){
                $infoPeliculaJSON = json_encode($infoPelicula);
                //Devuelvo un JSON con informacion de la pelicula
                echo($infoPeliculaJSON);
        }else{
                echo '{"status":"error"}';
        }       


}//FIN BUSQUEDA PELICULAS POR TITULO


//Funcion para encriptar
function encriptar($password,$vueltas = 10000){
        //Creo la semilla:
        $salt = substr(base64_encode(openssl_random_pseudo_bytes(17)),0,22);
        //Encripto y devuelvo el hash:
        return crypt((string) $password, '$6$'."rounds=$vueltas\$$salt\$");


}//Fin funcion encriptar



//CONSULTA PARA EL LOGIN (controladorIndex.js)
if(isset($_POST["cuentaLogin"])&&isset($_POST["passwordLogin"])){
        //echo(encriptar("abc123."));    
        try{
                //Declaro las variables
                $email =$_POST["cuentaLogin"];
                $password=$_POST["passwordLogin"];
                //Realizo la consulta (Si el nick o email introducido es de un usuario)
                $stmt=$pdo->prepare("SELECT idusuario,password,nick,email,nombre,apellidos,admin,hash,activa FROM FilmRate.usuarios where email='".$email."' OR nick='".$email."'");
                $stmt->execute();
                $arrayDatosUsuario =[];
                
                if($stmt->rowcount()==1){
                        $fila = $stmt->fetch();            
                        //Si el email/nick introducido es igual al email o nick del usuario:
                        if($email == $fila["nick"] || $email  ==$fila["email"]){
                                //Si la contraseña introducida es igual la contraseña del usuario:
                                if(hash_equals(crypt($password,$fila['password']),$fila['password'])==true){

                                        if($fila["activa"]!=0){
                                          
                                                //Creo la sesion
                                                session_start();
                                                //Creo las variables de sesion:
                                                $_SESSION["idusuario"]=$fila["idusuario"];
                                                $_SESSION["nick"]=$fila["nick"];
                                                $_SESSION["email"]=$fila["email"];
                                                //$_SESSION["password"]=$fila["password"];                             
                                                $_SESSION["nombre"]=$fila["nombre"];
                                                $_SESSION["apellidos"]=$fila["apellidos"];
                                                $_SESSION["admin"]=$fila["admin"];

                                                array_push($arrayDatosUsuario,$fila["idusuario"]);
                                                array_push($arrayDatosUsuario,$fila["nick"]);
                                                array_push($arrayDatosUsuario,$fila["email"]);
                                                array_push($arrayDatosUsuario,$fila["nombre"]);
                                                array_push($arrayDatosUsuario,$fila["apellidos"]);
                                                array_push($arrayDatosUsuario,$fila["admin"]);

                                                echo json_encode($arrayDatosUsuario);
                                                        /*
                                                //Si ese usuario es administrador:
                                                if($fila["admin"]==true){
                                                        //Redirijo al index de administradores:
                                                
                                                        
                                                        echo "indexAdmin.php";                                        
                                                        
                                                        


                                                }else if($fila["admin"]==false){
                                                        //Redirijo al index de usuarios:
                                                        echo "indexUsuario.php";
                                                }//Fin comprobacion administradores*/
                                        }else{
                                                echo("CUENTASINACTIVAR");
                                        }
                                }else{
                                        echo("ERRORLOGIN");
                                }
                        }else{
                                echo ("ERRORLOGIN");
                        }
                }else{
                        echo ("ERRORLOGIN");
                }
        
        }catch(PDOException $e){
                echo "Error en sentencia SQL ".$e->getMessage();
        }
}//FIN CONSULTA PARA EL LOGIN



//CONSULTA PARA OBTENER NOTA MEDIA DE UNA PELICULA POR ID (controladorIndex.js, controladorFilm.js)
if(isset($_POST["obtenerNotaPeliculaPorId"])){
        $id=$_POST["obtenerNotaPeliculaPorId"];

        $consultaPrueba = $pdo->prepare("SELECT nota FROM FilmRate.votos where filmid=$id");
        $consultaPrueba->execute();
        $arrayNotas=[];
        $notaMediaDePelicula=0;
        $numeroVotos=0;
        
        $arrayResultado = [];

        while($fila=$consultaPrueba->fetch()){
                if(count($fila)!=0){
                        array_push($arrayNotas,$fila["nota"]);                                               
                        $notaMediaDePelicula = array_sum($arrayNotas)/count($arrayNotas); 
                        $numeroVotos = count($arrayNotas);                         
                }                      
        }
        array_push($arrayResultado,$notaMediaDePelicula);
        array_push($arrayResultado,$numeroVotos);
        echo json_encode($arrayResultado);
}//CONSULTA PARA OBTENER NOTA MEDIA DE UNA PELICULA POR ID 




//COMPROBAR EXISTENCIA DE UN CORREO EN LA BASE DE DATOS (controladorIndex.php) (DEVUELVE 1 SI EXISTE Y 0 SI NO EXISTE)
if(isset($_POST["existeEsteCorreo"])){

        $correoComprobar = $_POST["existeEsteCorreo"];

        $consultaCorreo = $pdo->prepare("SELECT COUNT(email) FROM FilmRate.usuarios WHERE email='$correoComprobar'");
        $consultaCorreo ->execute();        
    
        while($fila=$consultaCorreo->fetch()){               
                echo($fila[0]);
        }
}//COMPROBAR EXISTENCIA DE UN CORREO EN BASE DE DATOS

//COMPROBAR EXISTENCIA DE UN Nick EN LA BASE DE DATOS (controladorIndex.php) (controladorPefil.php) (controladorPerfilUsuariosAdmin.js)(DEVUELVE 1 SI EXISTE Y 0 SI NO EXISTE)
if(isset($_POST["existeEsteNick"])){

        $nickComprobar = $_POST["existeEsteNick"];

        $consultaNick = $pdo->prepare("SELECT COUNT(nick) FROM FilmRate.usuarios WHERE nick='$nickComprobar'");
        $consultaNick ->execute(); 
        $array = []       ;
    
        while($fila=$consultaNick->fetch()){               
                echo($fila[0]);
                //array_push($array,$fila);
        }
        //echo json_encode($array);
}//COMPROBAR EXISTENCIA DE UN CORREO EN BASE DE DATOS


//REGISTRO DE UN NUEVO USUARIO (controladorIndex.php)
if(isset($_POST["registrarNombre"])&&isset($_POST["registrarApellidos"])&&isset($_POST["registrarNick"])&&isset($_POST["registrarEmail"])&&isset($_POST["registrarPassword"])){

        //DATOS QUE RECIBO:
        $nombre = ucwords($_POST["registrarNombre"]);
        $apellidos = ucwords($_POST["registrarApellidos"]);
        $nick = $_POST["registrarNick"];
        $email = $_POST["registrarEmail"];
        $password = encriptar($_POST["registrarPassword"]);

        //$email="usuario@usuario.com";
        //$nick="usuario";

        //Comprobaciones para ver si el email y nick son validos
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
                //Compruebo si existe el email en la base de datos.
                $consultaEmail = $pdo->prepare("SELECT COUNT(email) FROM FilmRate.usuarios WHERE email='$email'");
                $consultaEmail ->execute(); 
                while($filaEmail=$consultaEmail->fetch()){   
                        //Si no existe            
                        if($filaEmail[0]==0){
                                //Compruebo si existe el nick en la base de datos    
                                $consultaNick = $pdo->prepare("SELECT COUNT(nick) FROM FilmRate.usuarios WHERE nick='$nick'");
                                $consultaNick ->execute(); 
                                while($filaNick=$consultaNick->fetch()){   
                                        if($filaNick[0]==0){//Si no existe el nick
                                                //TODO BIEN, si el nick y el email no existen en la base de datos, tengo que registrar al usuario.
                                                $admin = 0;
                                                //try{
                                                //Lo comentado es para el envio de correos y verificacion de cuenta, me pondre con eso mas adelante
                                                $hash = md5(rand(0,1000));   
                                                $activa=0;                                                
                                                
                                                $crearUsuario = $pdo->prepare("INSERT INTO FilmRate.usuarios (nick,email,password,nombre,apellidos,admin,hash,activa) VALUES (?,?,?,?,?,?,?,?);");

                                                //$crearUsuario = $pdo->prepare("INSERT INTO FilmRate.usuarios (nick,email,password,nombre,apellidos,admin) VALUES (?,?,?,?,?,?);");
                                                $crearUsuario ->bindParam(1,$nick);
                                                $crearUsuario ->bindParam(2,$email);
                                                $crearUsuario ->bindParam(3,$password);
                                                $crearUsuario ->bindParam(4,$nombre);
                                                $crearUsuario ->bindParam(5,$apellidos);
                                                $crearUsuario ->bindParam(6,$admin);
                                                $crearUsuario ->bindParam(7,$hash);
                                                $crearUsuario ->bindParam(8,$activa);
                                                $crearUsuario -> execute();
                                                

                                                $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
                                                $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                                
                                                $titulo= 'Bienvenido a FilmRate';
                                                $mensaje= '
                                                <html>
                                                <head>
                                                  <title>Bienvenido a FilmRate '.$nombre.'</title>
                                                </head>
                                                <body>
                                                <p>Te enviamos el siguiente código de activación que se pedirá cuando accedas por primera vez con tu cuenta y contraseña a FilmRate.</p>
                                                
                                                Usuario: <strong>'.$nick.'</strong> <br>
                                                

                                                <br>
                                                Clave de para verificar la cuenta: <strong>'.$hash.'</strong>
                                                </body>
                                                </html>';
                                    
                                                mail($email,$titulo,$mensaje,$cabeceras);

                                                echo "USUARIOCREADO";
                                        }else{//Si existe el nick, envio mensaje de error:
                                                echo("Error del servidor al intentar crear el usuario con el nick introducido.");
                                        }
                                }//Fin comprobacion nick
                        }else{//Si existe, suelto un error
                             echo("Error del servidor al intentar crear el usuario con el email introducido.");
                        }
                }//Fin comprobacion correo                       
        }else{//Si no es valido:
                echo("Error del servidor al intentar crear el usuario con el email introducido.");
        }//Fin comprobaciones

}//FIN REGISTRO

//VALIDACION DE UNA CUENTA NUEVA DE UN USUARIO (controladorIndex.js)
if(isset($_POST["cuentaParaActivar"])&&isset($_POST["codigoActivacion"])){

        $cuentaActivar = $_POST["cuentaParaActivar"];
        $codigoActivar = $_POST["codigoActivacion"];

        
        $consultaNick = $pdo->prepare("SELECT hash FROM FilmRate.usuarios WHERE nick='$cuentaActivar' or email='$cuentaActivar'");
        $consultaNick ->execute();        

       
        while($fila=$consultaNick->fetch()){
                $resultado = $fila; //Esto se puede meter en un array
                if($resultado[0]===$codigoActivar){//FROM FilmRate.usuarios where email='".$email."' OR nick='".$email."'"
                        $activarCuenta = $pdo->prepare("UPDATE FilmRate.usuarios SET activa = 1 where email='".$cuentaActivar."' OR nick='".$cuentaActivar."'");
                        $activarCuenta ->execute();
                }else{
                        echo "CODIGOACTIVACIONERRONEO";
                }
        }

        

}//FIN VALIDACION DE UNA CUENTA NUEVA DE UN USUARIO 



//CONSULTA QUE OBTIENE TODA LA INFORMACION DE UNA PELICULA A PARTIR DEL ID (controladorFilm.js, controladorFilmAdmin.js)
if(isset($_POST["idBuscarInfo"])){
        $idIntroducido = $_POST["idBuscarInfo"];

        $consultaPeliculaIdFilm = $pdo->prepare("SELECT * FROM FilmRate.films where idfilm=$idIntroducido");
        $consultaPeliculaIdFilm->execute();
        $infoPelicula=array();
    
        while($fila=$consultaPeliculaIdFilm->fetch()){
                array_push($infoPelicula,$fila); //Esto se puede meter en un array          
        }
    
        $infoPeliculaJSON = json_encode($infoPelicula);
        //Devuelvo un JSON con informacion de la pelicula
        echo($infoPeliculaJSON);

}//CONSULTA QUE OBTIENE TODA LA INFORMACION DE UNA PELICULA PARA EL INDEX PUBLICO


//CONSULTA QUE OBTIENE LOS VOTOS DE UN USUARIO EN UNA PELICULA A PARTIR DE SU ID Y DEL ID DE LA PELICULA (controladorFilm.js)
//{idFilmVotos:idFilmEncontrado,idUsuarioVotos:sessionStorage.Id
if(isset($_POST["idFilmVotoUsuario"])&&isset($_POST["idUsuarioVotos"])){

        $idFilmVotos = $_POST["idFilmVotoUsuario"];
        $idUsuarioVotos =$_POST["idUsuarioVotos"];

        $consultaVotosPeliculaUsuario = $pdo->prepare("SELECT * FROM FilmRate.votos where filmid=$idFilmVotos AND usuarioid=$idUsuarioVotos;");
        $consultaVotosPeliculaUsuario->execute();
        $infoVotos = array();
        while($fila=$consultaVotosPeliculaUsuario->fetch()){
                array_push($infoVotos,$fila);
        }
        $infoVotosJSON = json_encode($infoVotos);
        echo($infoVotosJSON);
        
}//FIN CONSULTA QUE OBTIENE LOS VOTOS DE UN USUARIO EN UNA PELICULA A PARTIR DE SU ID Y DEL ID DE LA PELICULA



//CONSULTA QUE OBTIENE LAS CRITICAS DE UN USUARIO EN UNA PELICULA A PARTIR DE SU ID Y DEL ID DE LA PELICULA (controladorFilm.js)
//idFilmCriticas:idFilmEncontrado,idUsuarioCriticas:sessionStorage.Id
if(isset($_POST["idFilmCriticaUsuario"])&&isset($_POST["idUsuarioCriticas"])){

        $idFilmCriticas = $_POST["idFilmCriticaUsuario"];
        $idUsuarioCriticas =$_POST["idUsuarioCriticas"];

        $consultaCriticasPeliculaUsuario = $pdo->prepare("SELECT * FROM FilmRate.criticas where filmid=$idFilmCriticas AND usuarioid=$idUsuarioCriticas;");
        $consultaCriticasPeliculaUsuario->execute();
        $infoCriticas = array();
        while($fila=$consultaCriticasPeliculaUsuario->fetch()){                
                array_push($infoCriticas,$fila);
        }
        $infoCriticasJSON = json_encode($infoCriticas);
        echo($infoCriticasJSON);
        
}//FIN CONSULTA QUE OBTIENE LOS VOTOS DE UN USUARIO EN UNA PELICULA A PARTIR DE SU ID Y DEL ID DE LA PELICULA



//CONSULTA QUE OBTIENE TODAS LAS CRITICAS DE UN FILM A PARTIR DE SU ID (controladorFilm.js)
if(isset($_POST["idFilmVotos"])){

        $idFilmAllVotos = $_POST["idFilmVotos"];
        
        $consultaVotosIdFilm=$pdo->prepare("SELECT * FROM FilmRate.votos where filmid=$idFilmAllVotos");
        $consultaVotosIdFilm->execute();
        $arrayAllVotos=array();
        while($fila=$consultaVotosIdFilm->fetch()){
                array_push($arrayAllVotos,$fila);
        }
        $allVotosJSON = json_encode($arrayAllVotos);
        echo ($allVotosJSON);

}//FINCONSULTA QUE OBTIENE TODAS LAS CRITICAS DE UN FILM A PARTIR DE SU ID


//CONSULTA QUE OBTIENE TODAS LAS CRITICAS DE UN FILM A PARTIR DE SU ID (controladorFilm.js)
if(isset($_POST["idFilmCriticas"])&&$_POST["idUsuarioActualNo"]){

        $idFilmAllCriticas = $_POST["idFilmCriticas"];
        $idUsuarioActual =$_POST["idUsuarioActualNo"];
        
        $consultaCriticasIdFilm=$pdo->prepare("SELECT * FROM FilmRate.criticas where filmid=$idFilmAllCriticas and usuarioid!=$idUsuarioActual");
        $consultaCriticasIdFilm->execute();
        $arrayAllCriticas=array();
        while($fila=$consultaCriticasIdFilm->fetch()){
                array_push($arrayAllCriticas,$fila);
        }
        $allCriticasJSON = json_encode($arrayAllCriticas);
        echo ($allCriticasJSON);

}//FINCONSULTA QUE OBTIENE TODAS LAS CRITICAS DE UN FILM A PARTIR DE SU ID


//CONSULTA PARA SABER SI EXISTE EL ID DE UNA PELICULA (controladorFilm.js, controladorFilmAdmin.js)
if(isset($_POST["idBuscarInfoExiste"])){

        $idFilmExiste = $_POST["idBuscarInfoExiste"];
        $consultaFilmExisteId = $pdo->prepare("SELECT count(*) FROM FilmRate.films where idfilm=$idFilmExiste");
        $consultaFilmExisteId->execute();
        $arrayResultadoCuantosFilmsExisten = array();
        while($fila=$consultaFilmExisteId->fetch()){
                array_push($arrayResultadoCuantosFilmsExisten,$fila);
        }

        echo(json_encode($arrayResultadoCuantosFilmsExisten));
        


}//FIN CONSULTA PARA SABER SI EXISTE EL ID DE UNA PELICULA 

//CONSULTA PARA VOTAR (controladorFilm.js)
//{notaNuevaFilm:notaNueva,idFilmNotaNueva:idFilmEncontrado,idUsuarioNotaNueva:sessionStorage.Id}
if(isset($_POST["notaNuevaFilm"])&&isset($_POST["idFilmNotaNueva"])&&isset($_POST["idUsuarioNotaNueva"])){

        $filmid=$_POST["idFilmNotaNueva"];
        $usuarioid = $_POST["idUsuarioNotaNueva"];
        $nota = $_POST["notaNuevaFilm"];

        $consultaNumeroVotos = $pdo->prepare("SELECT count(*) FROM FilmRate.votos where filmid=$filmid AND usuarioid=$usuarioid");
        
        //Con esto me aseguro de que no se meten notas fuera de rango
        if($nota>=1&&$nota<=10){
                $consultaNumeroVotos->execute();     
                //Si el usuario ya ha votado alguna vez, tengo que actualizar su voto UPDATE
                if($consultaNumeroVotos->fetch()[0]==1){
                        $consultaVotar = $pdo->prepare("UPDATE FilmRate.votos SET nota=$nota where usuarioid=$usuarioid and filmid=$filmid");
                        $consultaVotar ->execute();

                }else{//Si no ha votado la pelicula hay que introducir el voto INSERT           
                        $consultaVotar = $pdo->prepare("INSERT INTO FilmRate.votos (usuarioid,filmid,nota) VALUES ($usuarioid,$filmid,$nota)");
                        $consultaVotar->execute();
                        echo("VOTONUEVO");
                };        
        }
}//FIN CONSULTA PARA VOTAR 

//CONSULTA SABER NICK DE UN USUARIO SEGUN ID (controladorFilm.js)
if(isset($_POST["idCadaUsuarioCriticaFilm"])){

        $idUsuario = $_POST["idCadaUsuarioCriticaFilm"];
        $consultaSaberNick = $pdo->prepare("SELECT nick FROM FilmRate.usuarios WHERE idusuario=$idUsuario");
        $consultaSaberNick->execute();
        //echo($consultaSaberNick->fetch()[0]."-".$consultaSaberNick->fetch()[2]);
        $arrayResultado=[];
        while($fila=$consultaSaberNick->fetch()){
                array_push($arrayResultado,$fila);
        }
        echo(json_encode($arrayResultado));

}//FIN CONSULTA SABER NICK DE UN USUARIO SEGUN ID


//CONSULTA MODIFICAR CRITICA USUARIO (controladorFilm.js)
//{criticaModificar:nuevaCritica,idFilmModificarCritica:idFilmEncontrado,idUsuarioModificarCritica:sessionStorage.Id}
if(isset($_POST["criticaModificar"])&&isset($_POST["idFilmModificarCritica"])&&isset($_POST["idUsuarioModificarCritica"])){
        $idFilmModificarCritica = $_POST["idFilmModificarCritica"];
        $idUsuarioModificarCritica=$_POST["idUsuarioModificarCritica"];
        
        $criticaNueva =strip_tags(htmlentities($_POST["criticaModificar"]));//Compruebo que lo introducido no es un script:
             
        
        $fechahoraactual = date("Y:m-d H:i:s");

        $consultaModificarCritica = $pdo->prepare("UPDATE FilmRate.criticas SET textocritica='$criticaNueva',fechacritica='$fechahoraactual' WHERE filmid=$idFilmModificarCritica AND usuarioid=$idUsuarioModificarCritica");
        $consultaModificarCritica->execute();
        echo("CRITICAMODIFICADA");


}//FIN CONSULTA MODIFICAR CRITICA USUARIO 


//CONSULTA PARA BORRAR CRITICA USUARIO (controladorFilm.js)
//{idFilmBorrarCritica:idFilmEncontrado,idUsuarioBorrarCritica:sessionStorage.Id}
if(isset($_POST["idFilmBorrarCritica"])&&isset($_POST["idUsuarioBorrarCritica"])){

        $idFilmBorrarCritica = $_POST["idFilmBorrarCritica"];
        $idUsuarioBorrarCritica = $_POST["idUsuarioBorrarCritica"];

        $consultaBorrarCritica = $pdo->prepare("DELETE FROM FilmRate.criticas WHERE filmid=$idFilmBorrarCritica AND usuarioid=$idUsuarioBorrarCritica");
        $consultaBorrarCritica -> execute();

        echo("CRITICABORRADA");



}//FIN CONSULTA PARA BORRAR CRITICA USUARIO 


//FUNCION PARA AÑADIR UNA NUEVA CRITICA (controladorFilm.js)
//{idFilmNuevaCritica:idFilmEncontrado,idUsuarioNuevaCritica:sessionStorage.Id,nuevaCritica:nuevaCritica}
if(isset($_POST["idFilmNuevaCritica"])&&isset($_POST["idUsuarioNuevaCritica"])&&isset($_POST["nuevaCritica"])){

        $idUsuarioNuevaCritica=$_POST["idUsuarioNuevaCritica"];
        $idFilmNuevaCritica=$_POST["idFilmNuevaCritica"];
        $nuevaCritica=strip_tags(htmlentities($_POST["nuevaCritica"]));

        $consultaNuevaCritica = $pdo->prepare("INSERT INTO FilmRate.criticas (usuarioid,filmid,textocritica)VALUES($idUsuarioNuevaCritica,$idFilmNuevaCritica,'$nuevaCritica')");
        $consultaNuevaCritica->execute();

        echo("CREADANUEVACRITICA");

}
//FIN FUNCION PARA AÑADIR UNA NUEVA CRITICA



//FUNCION PARA COMPROBAR SI LA PELICULA ESTA EN FAVORITOS Devuelve 1 o 0 (controladorFilm.js)
//{idFilmComprobarFavorito:idFilmEncontrado,idUsuarioComprobarFavorito:sessionStorage.Id}
if(isset($_POST["idFilmComprobarFavorito"])&&isset($_POST["idUsuarioComprobarFavorito"])){

        $idFilmFavorito = $_POST["idFilmComprobarFavorito"];
        $idUsuarioFavorito = $_POST["idUsuarioComprobarFavorito"];

        $consultaEsFavorito = $pdo->prepare("SELECT COUNT(*) FROM FilmRate.favoritas WHERE usuarioid=$idUsuarioFavorito AND filmid=$idFilmFavorito");
        $consultaEsFavorito -> execute();

        //Devuelve 1 si está en favoritas y 0 si no lo esta
        echo($consultaEsFavorito->fetch()[0]);

}//FIN FUNCION PARA COMPROBAR SI LA PELICULA ESTA EN FAVORITOS 



//FUNCION PARA AÑADIR UNA PELICULA A FAVORITAS (controladorFilm.js)
//{idFilmAgregarFavorito:idFilmEncontrado,idUsuarioAgregarFavorito:sessionStorage.id}
if(isset($_POST["idFilmAgregarFavorito"])&&isset($_POST["idUsuarioAgregarFavorito"])){
        session_start();     
        
        $idFilmAgregarFavorito = $_POST["idFilmAgregarFavorito"];
        $idUsuarioAgregarFavorito = $_POST["idUsuarioAgregarFavorito"];

        //Compruebo si el usuario recibido es igual al que tiene la sesion inciada
        if($_SESSION["idusuario"]==$idUsuarioAgregarFavorito){
                $consultaAgregarFilmFavorito = $pdo->prepare("INSERT INTO FilmRate.favoritas (usuarioid,filmid) VALUES ($idUsuarioAgregarFavorito,$idFilmAgregarFavorito)");
                $consultaAgregarFilmFavorito->execute();

                echo ("AGREGADOAFAVORITOS");        
        }

}//FIN FUNCION PARA AÑADIR UNA PELICULA A FAVORITAS 

//FUNCION PARA ELIMINAR UNA PELICULA DE FAVORITAS (controladorFilm.js)
//{idFilmAgregarFavorito:idFilmEncontrado,idUsuarioAgregarFavorito:sessionStorage.id}
if(isset($_POST["idFilmEliminarFavorito"])&&isset($_POST["idUsuarioEliminarFavorito"])){
        session_start();
        $idFilmEliminarFavorito = $_POST["idFilmEliminarFavorito"];
        $idUsuarioEliminarFavorito = $_POST["idUsuarioEliminarFavorito"];

        //Compruebo si el usuario recibido es igual al que tiene la sesion inciada
        if($_SESSION["idusuario"]==$idUsuarioEliminarFavorito){                      

                $consultaEliminarFilmFavorito = $pdo->prepare("DELETE FROM FilmRate.favoritas WHERE usuarioid=$idUsuarioEliminarFavorito AND filmid=$idFilmEliminarFavorito");
                $consultaEliminarFilmFavorito->execute();

                echo ("ELIMINADODEFAVORITOS");        
        }
}//FIN FUNCION PARA ELIMINAR UNA PELICULA DE FAVORITAS 

//FUNCION PARA DEVOLVER EL NOMRBE DE LA IMAGEN DE UN PAIS A TRAVES DEL ID DEL PAIS (controladorFilm.js)
if(isset($_POST["idPais"])){

        $idPais =$_POST["idPais"];

        $consultaFotoPais = $pdo->prepare("SELECT fotopais,nombrepaiscastellano FROM FilmRate.paises WHERE idpais='$idPais'");
        $consultaFotoPais ->execute();

        $arrayPais=[];

        while($fila=$consultaFotoPais->fetch()){
                array_push($arrayPais,$fila);
        }

        if(count($arrayPais)==1){
                echo json_encode($arrayPais);
        }else{
                echo ('{"status":"SINPAIS"}');
        }

        

}//FIN FUNCION PARA DEVOLVER EL NOMRBE DE LA IMAGEN DE UN PAIS A TRAVES DEL ID DEL PAIS


//CONSULTA PARA SABER LA NOTA MEDIA DE TODOS LOS VOTOS DE UN USUARIO (controladorPerfil.js)(controladorPerfilUsuariosAdmin.js)
if(isset($_POST["mediaUsuarioInfoPerfil"])){
        $nickUsuarioBuscarMedia = $_POST["mediaUsuarioInfoPerfil"];
        
        $consultaSaberMediaNick = $pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE usuarioid=(SELECT idusuario FROM FilmRate.usuarios where nick='$nickUsuarioBuscarMedia')");
        $consultaSaberMediaNick ->execute();

        echo($consultaSaberMediaNick->fetch()[0]);

}//FIN CONSULTA PARA SABER LA NOTA MEDIA DE TODOS LOS VOTOS DE UN USUARIO

//CONSULTA PARA SABER LA CANTIDAD DE VOTOS DE UN USUARIO (controladorPerfil.js)(controladorPerfilUsuariosAdmin.js)
if(isset($_POST["votosUsuarioInfoPerfil"])){
        $nickUsuarioBuscarVotos = $_POST["votosUsuarioInfoPerfil"];
        
        $consultaSaberVotosNick = $pdo->prepare("SELECT count(*) FROM FilmRate.votos WHERE usuarioid=(SELECT idusuario FROM FilmRate.usuarios where nick='$nickUsuarioBuscarVotos')");
        $consultaSaberVotosNick ->execute();

        echo($consultaSaberVotosNick->fetch()[0]);

}//FIN CONSULTA PARA SABER LA CANTIDAD DE VOTOS DE UN USUARIO


//CONSULTA PARA SABER LA CANTIDAD DE CRITICAS DE UN USUARIO (controladorPerfil.js)(controladorPerfilUsuariosAdmin.js)
if(isset($_POST["criticasUsuarioInfoPerfil"])){
        $nickUsuarioBuscarCriticas = $_POST["criticasUsuarioInfoPerfil"];
        
        $consultaSaberVotosCriticas = $pdo->prepare("SELECT count(*) FROM FilmRate.criticas WHERE usuarioid=(SELECT idusuario FROM FilmRate.usuarios where nick='$nickUsuarioBuscarCriticas')");
        $consultaSaberVotosCriticas ->execute();

        echo($consultaSaberVotosCriticas->fetch()[0]);

}//FIN CONSULTA PARA SABER LA CANTIDAD DE CRITICAS DE UN USUARIO


//CONSULTA PARA SABER LA CANTIDAD DE FAVORITAS DE UN USUARIO (controladorPerfil.js)(controladorPerfilUsuariosAdmin.js)
if(isset($_POST["favoritasUsuarioInfoPerfil"])){
        $nickUsuarioBuscarFavoritas = $_POST["favoritasUsuarioInfoPerfil"];
        
        $consultaSaberVotosFavoritas = $pdo->prepare("SELECT count(*) FROM FilmRate.favoritas WHERE usuarioid=(SELECT idusuario FROM FilmRate.usuarios where nick='$nickUsuarioBuscarFavoritas')");
        $consultaSaberVotosFavoritas ->execute();

        echo($consultaSaberVotosFavoritas->fetch()[0]);

}//FIN CONSULTA PARA SABER LA CANTIDAD DE FAVORITAS DE UN USUARIO

//CONSULTA PARA SABER LA CANTIDAD DE HORAS DE UN USUARIO (controladorPerfil.js)(controladorPerfilUsuariosAdmin.js)
if(isset($_POST["horasUsuarioInfoPerfil"])){
        //nick:
        $nickUsuarioBuscarHoras = $_POST["horasUsuarioInfoPerfil"];

        //Necesito saber el id de este nick:
        $saberIdNickHoras = $pdo -> prepare("SELECT idusuario FROM FilmRate.usuarios where nick='$nickUsuarioBuscarHoras'");
        $saberIdNickHoras->execute();
        $idBuscarHoras = $saberIdNickHoras->fetch()[0];

        //Hago la consulta
        //$consultaSaberHoras = $pdo->prepare("SELECT duracion FROM FilmRate.votos inner join FilmRate.films on FilmRate.votos.filmid= FilmRate.films.idfilm where usuarioid=$idBuscarHoras");
        $consultaSaberHoras = $pdo->prepare("select sum(duracion) from FilmRate.votos inner join FilmRate.films on FilmRate.votos.filmid=FilmRate.films.idfilm where usuarioid=$idBuscarHoras");
        $consultaSaberHoras -> execute();

        echo ($consultaSaberHoras->fetch()[0]);

        

}
//FIN CONSULTA PARA SABER LA CANTIDAD DE HORAS DE UN USUARIO








//CONSULTA PARA SABER LOS NOMBRES Y APELLIDOS DE UN NICK (controladorPerfil.js)
if(isset($_POST["saberNombreApellidosNick"])){

        $nickNombreApellidos = $_POST["saberNombreApellidosNick"];
        $consultaNombreApellidos =$pdo->prepare("SELECT nombre,apellidos FROM FilmRate.usuarios where nick='$nickNombreApellidos'");
        $consultaNombreApellidos->execute();
        $arrayNombreApellidos = [];
        while($fila=$consultaNombreApellidos->fetch()){
                array_push($arrayNombreApellidos,$fila);
        }
        
        echo json_encode($arrayNombreApellidos);
}
//FIN CONSULTA PARA SABER LOS NOMBRES Y APELLIDOS DE UN NICK

//CONSULTA PARA CAMBIAR EL NOMBRE DEL USUARIO (controladorPerfil.js)
//{nuevoNombreUsuario:nuevoNombre,nickUsuarioCambiarNombre:sessionStorage.Id}
if(isset($_POST["nuevoNombreUsuario"])&&isset($_POST["nickUsuarioCambiarNombre"])){
        session_start();
        $nickUsuario = $_SESSION["nick"];
        
        $nickUsuarioPeticion = $_POST["nickUsuarioCambiarNombre"];        
        $nuevoNombreUsuario =$_POST["nuevoNombreUsuario"];

        //Si el usuario que quiere cambiar el nick, es el mismo que tiene la session iniciada en el servidor:
        if($nickUsuario == $nickUsuarioPeticion ){
                //Si el nuevo nombre es mayor a 0:
                if(strlen($nuevoNombreUsuario)>0){
 
                        $consultaCambiarNombre = $pdo->prepare("UPDATE FilmRate.usuarios SET nombre='$nuevoNombreUsuario' WHERE nick='$nickUsuarioPeticion'");
                        $consultaCambiarNombre->execute();

                        //Cambio el nombre de la sesion:
                        $_SESSION["nombre"]=$nuevoNombreUsuario;

                        echo "NOMBRECAMBIADO";
                }//Fin si el nuevo nombre es mayor a 0              
        }//Fin comprobación usuario cliente/servidor
}//FIN CONSULTA PARA CAMBIAR EL NOMBRE DEL USUARIO 


//CONSULTA PARA CAMBIAR LOS APELLIDOS DEL USUARIO (controladorPerfil.js)
//,{nuevosApellidosUsuario:nuevoNombre,nickUsuarioCambiarApellidos:sessionStorage.Nick},
if(isset($_POST["nuevosApellidosUsuario"])&&isset($_POST["nickUsuarioCambiarApellidos"])){
        session_start();
        $nickUsuario = $_SESSION["nick"];
        
        $nickUsuarioPeticion = $_POST["nickUsuarioCambiarApellidos"];        
        $nuevosApellidosUsuario =$_POST["nuevosApellidosUsuario"];

        //Si el usuario que quiere cambiar el nick, es el mismo que tiene la session iniciada en el servidor:
        if($nickUsuario == $nickUsuarioPeticion ){                
                //Si el nuevo nombre es mayor a 0:
                if(strlen($nuevosApellidosUsuario)>0){
 
                        $consultaCambiarApellidos = $pdo->prepare("UPDATE FilmRate.usuarios SET apellidos='$nuevosApellidosUsuario'WHERE nick='$nickUsuarioPeticion'");
                        $consultaCambiarApellidos->execute();

                        //Cambio el nombre de la sesion:
                        $_SESSION["apellidos"]=$nuevosApellidosUsuario;

                        echo "APELLIDOSCAMBIADOS";
                }//Fin si el nuevo nombre es mayor a 0              
        }//Fin comprobación usuario cliente/servidor
}//FIN CONSULTA PARA CAMBIAR LOS APELLIDOS DEL USUARIO 


//CONSULTA PARA COMPROBAR SI LA CONTRASEÑA INTRODUCIDA ES LA CONTRASEÑA DEL USUARIO ACTUAL Devuelve 1 o 0 (controladorPerfil.js)
//{passwdUsuarioAutenticar:passwordpedida,nickUsuarioAutenticar:sessionStorage.Nick}
if(isset($_POST["passwdUsuarioAutenticar"])&&isset($_POST["nickUsuarioAutenticar"])){
        session_start();
        $nickUsuario=$_SESSION["nick"];

        $nickUsuarioPeticion = $_POST["nickUsuarioAutenticar"];
        $passwordIntroducida = $_POST["passwdUsuarioAutenticar"];
        //hash_equals(crypt($password,$fila['password']),$fila['password'])==true){
        if($nickUsuario == $nickUsuarioPeticion){

                $consultaObtenerPasswdUsuario = $pdo->prepare("SELECT password FROM FilmRate.usuarios WHERE nick='$nickUsuario'");
                $consultaObtenerPasswdUsuario->execute();
                $passwordBD = $consultaObtenerPasswdUsuario->fetch()[0];
                

                //Si la contraseña introducida es igual a la misma contraseña de ese usuario en la base de datos:
                if(hash_equals(crypt($passwordIntroducida,$passwordBD),$passwordBD)==true){
                        echo "1";
                }else{
                        echo "0";
                }//Fin si la contraseña introducida es igual a la misma contraseña de ese usuario en la base de datos:

        }
}//FIN CONSULTA PARA COMPROBAR SI LA CONTRASEÑA INTRODUCIDA ES LA CONTRASEÑA DEL USUARIO ACTUAL







//CONSULTA PARA QUE EL USUARIO PUEDA CAMBIAR SU CONTRASEÑA (controladorPerfil.js)
//{nuevaPasswordCambiar:pass1,nickUsuarioCambiarPassword:sessionStorage.Nick}
if(isset($_POST["nuevaPasswordCambiar"])&&isset($_POST["nickUsuarioCambiarPassword"])){
        session_start();
        $nickUsuario = $_SESSION["nick"];

        $nickUsuarioPeticion = $_POST["nickUsuarioCambiarPassword"];
        $nuevaPasswordUsuario = encriptar($_POST["nuevaPasswordCambiar"]);

        //Si el usuario que quiere cambiar el nick, es el mismo que tiene la session iniciada en el servidor:
        if($nickUsuario == $nickUsuarioPeticion ){ 
                //Si la contraseña es mayor a 8
                if(strlen($nuevaPasswordUsuario)>=8){
                        $consultaCambiarPassword = $pdo->prepare("UPDATE FilmRate.usuarios SET password='$nuevaPasswordUsuario'WHERE nick='$nickUsuarioPeticion'");
                        $consultaCambiarPassword->execute();

                        echo("PASSWORDCAMBIADA");
                }//Fin si la contraseña es mayor a 8
        }//Fin comprobacion usuario cliente/servidor        
}//FIN CONSULTA PARA QUE EL USUARIO PUEDA CAMBIAR SU CONTRASEÑA

//CONSULTA PARA QUE EL USUARIO ELIMINE SU CUENTA (controladorPerfil.js)
if(isset($_POST["idEliminarCuenta"])){
        session_start();
        $idUsuarioIniciadaSesionServidor = $_SESSION["idusuario"];
        $idUsuarioIniciadaSesionCliente =$_POST["idEliminarCuenta"];
        
        //Si el usuario que quiere cambiar el nick, es el mismo que tiene la session iniciada en el servidor:
        if($idUsuarioIniciadaSesionCliente==$idUsuarioIniciadaSesionServidor){
                $consultaEliminarCuenta = $pdo->prepare("DELETE FROM FilmRate.usuarios WHERE idusuario=$idUsuarioIniciadaSesionServidor");
                $consultaEliminarCuenta->execute();
                echo "CUENTAELIMINADA";

        }//Fin comprobacion usuario cliente/servidor        


}//FIN CONSULTA PARA QUE EL USUARIO ELIMINE SU CUENTA 


//CONSULTA PARA SABER EL ID DE UN USUARIO SEGUN SU NICK (controladorEstadisticas.js)
if(isset($_POST["nickUsuarioSaberId"])){
        $nickRecibido=$_POST["nickUsuarioSaberId"];

        //Saber si no es admin:
        $consultaEsAdmin =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE nick='$nickRecibido'");
        $consultaEsAdmin->execute();

        //Si el nick introducido es de un administrador, devolver error
        if($consultaEsAdmin->fetch()[0]==1||$consultaEsAdmin->fetch()[0]=="1"){
                echo ("ERROR");
        
        }else{
                $consultaIdUsuario = $pdo->prepare("SELECT idusuario FROM FilmRate.usuarios WHERE nick='$nickRecibido'");
                $consultaIdUsuario->execute();
                echo($consultaIdUsuario->fetch()[0]);
        }        

}//FIN CONSULTA PARA SABER EL ID DE UN USUARIO SEGUN SU NICK



//CONSULTA PARA SABER LOS CODIGOS DE LOS PAISES Y SUS NOMBRES (controladorEstadisticas.js)
if(isset($_POST["saberCodigosNombresPaises"])){

        $consultaSaberCodigoNombresPaises = $pdo->prepare("SELECT idpais,nombrepaiscastellano FROM FilmRate.paises");
        $consultaSaberCodigoNombresPaises->execute();

        $arrayCodigoNombre = [];

        while($fila=$consultaSaberCodigoNombresPaises->fetch()){
                array_push($arrayCodigoNombre,$fila);
        }

        echo json_encode($arrayCodigoNombre);

}//FIN CONSULTA PARA SABER LOS CODIGOS DE LOS PAISES Y SUS NOMBRES 


//CONSULTA PARA SABER LA CANTIDAD DE PELICULAS POR PAIS (controladorEstadisticas.js)
if(isset($_POST["saberCantidadPeliculasPorPais"])){

        $consultaSaberCantidadPeliculasPorPais = $pdo->prepare("SELECT count(*) as cantidad, pais FROM FilmRate.films GROUP BY pais ORDER BY cantidad DESC");
        $consultaSaberCantidadPeliculasPorPais->execute();

        $arrayCantidadPorPais = [];

        while($fila=$consultaSaberCantidadPeliculasPorPais->fetch()){
                array_push($arrayCantidadPorPais,$fila);
        }

        echo json_encode($arrayCantidadPorPais);


}//FIN CONSULTA PARA SABER LA CANTIDAD DE PELICULAS POR PAIS










//CONSULTA PARA SABER LAS ULTIMAS 10 NOTAS DE LOS USUARIOS Devuelve un array json con los ultimos 10 votos (controladorEstadisticas.js)
if(isset($_POST["saberDiezUltimasNotasUsuarios"])&&isset($_POST["idUsuarioIgnorar"])){
        
        $idUsuarioIgnorar = $_POST["idUsuarioIgnorar"];

        $consultaDiezUltimasNotasUsuarios=$pdo->prepare("SELECT nota,fechavoto FROM FilmRate.votos WHERE usuarioid!=$idUsuarioIgnorar ORDER BY fechavoto DESC");
        $consultaDiezUltimasNotasUsuarios->execute();

        $arrayVotos=[];

        while($fila=$consultaDiezUltimasNotasUsuarios->fetch()){
                array_push($arrayVotos,$fila);
        }
        
        if(count($arrayVotos)>=10){

                $arrayDiezVotosUsuarios=[];
                for($i=0;$i<10;$i++){
                        array_push($arrayDiezVotosUsuarios,$arrayVotos[$i]);
                }
        
                echo json_encode($arrayDiezVotosUsuarios);


                
        }else{

                $arrayDiezVotosUsuarios=[];
                for($i=0;$i<count($arrayVotos);$i++){
                        array_push($arrayDiezVotosUsuarios,$arrayVotos[$i]);
                }
        
                echo json_encode($arrayDiezVotosUsuarios);

        }

     
        
}//FIN CONSULTA PARA SABER LAS ULTIMAS 10 NOTAS DE LOS USUARIOS 


//CONSULTA PARA SABER LAS ULTIMAS 10 NOTAS DEL USUARIO Devuelve un array json con los ultimos 10 votos (controladorEstadisticas.js)
if(isset($_POST["saberDiezUltimasNotasUsuario"])&&isset($_POST["idUsuarioDiezNotas"])){
        
        $idUsuarioDiezNotas = $_POST["idUsuarioDiezNotas"];

        $consultaDiezUltimasNotasUsuario=$pdo->prepare("SELECT nota,fechavoto FROM FilmRate.votos WHERE usuarioid=$idUsuarioDiezNotas ORDER BY fechavoto DESC");
        $consultaDiezUltimasNotasUsuario->execute();

        $arrayVotos=[];

        while($fila=$consultaDiezUltimasNotasUsuario->fetch()){
                array_push($arrayVotos,$fila);
        }       

        if(count($arrayVotos)>=10){

                $arrayDiezVotosUsuario=[];
                for($i=0;$i<10;$i++){
                        array_push($arrayDiezVotosUsuario,$arrayVotos[$i]);
                }

                echo json_encode($arrayDiezVotosUsuario);

        }else{
                $arrayDiezVotosUsuario=[];
                for($i=0;$i<count($arrayVotos);$i++){
                        array_push($arrayDiezVotosUsuario,$arrayVotos[$i]);
                }

                echo json_encode($arrayDiezVotosUsuario);
        }
        
}//CONSULTA PARA SABER LAS ULTIMAS 10 NOTAS DEL USUARIO 

//CONSUILTA QUE ME DEVUELVE UN ARRAY CON LAS NOTAS MEDIAS GENERAL POR GENERO (controladorEstadisticas.js)
if(isset($_POST["notaMediaPorGenero"])){
        
        //ORDEN ARRAY POR GENEROS : [THRILLER, DRAMA, BELICO, MAFIA, CIENCIA FICCION, ACCION]
        $arrayNotasMediasGenerales=[];

        //THRILLER
        $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%Thriller%')");
        $consultaMediaThriller -> execute();
        $arrayNotasMediasGenerales[0]=round($consultaMediaThriller->fetch()[0],3);

        //DRAMA
        $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%Drama%')");
        $consultaMediaThriller -> execute();
        $arrayNotasMediasGenerales[1]=round($consultaMediaThriller->fetch()[0],3);

        //BELICO
        $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%Belico%')");
        $consultaMediaThriller -> execute();
        $arrayNotasMediasGenerales[2]=round($consultaMediaThriller->fetch()[0],3);

        //MAFIA
        $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%Mafia%')");
        $consultaMediaThriller -> execute();
        $arrayNotasMediasGenerales[3]=round($consultaMediaThriller->fetch()[0],3);


        //CIENCIA FICCION
        $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%ficcion%')");
        $consultaMediaThriller -> execute();
        $arrayNotasMediasGenerales[4]=round($consultaMediaThriller->fetch()[0],3);

        //ACCION
        $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%accion%')");
        $consultaMediaThriller -> execute();
        $arrayNotasMediasGenerales[5]=round($consultaMediaThriller->fetch()[0],3);
 

        echo json_encode($arrayNotasMediasGenerales);


}//FIN CONSUILTA QUE ME DEVUELVE UN ARRAY CON LAS NOTAS MEDIAS GENERAL POR GENERO

//CONSULTA QUE OBTIENE LAS NOTAS MEDIAS DE UN USUARIO POR EL GENERO (controladorEstadisticas.js)
//{notaMediaPorGeneroUsuario:"true",idUsuarioNotaMediaPorGenero:sessionStorage.Id}
if(isset($_POST["notaMediaPorGeneroUsuario"])&&isset($_POST["idUsuarioNotaMediaPorGenero"])){
        
        $idUsuarioNotasMediasGenero =$_POST["idUsuarioNotaMediaPorGenero"];

         //ORDEN ARRAY POR GENEROS : [THRILLER, DRAMA, BELICO, MAFIA, CIENCIA FICCION, ACCION]
         $arrayNotasMediasGenerales=[];

         //THRILLER
         $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%Thriller%') AND usuarioid=$idUsuarioNotasMediasGenero");
         $consultaMediaThriller -> execute();
         $arrayNotasMediasGenerales[0]=round($consultaMediaThriller->fetch()[0],3);
 
         //DRAMA
         $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%Drama%') AND usuarioid=$idUsuarioNotasMediasGenero");
         $consultaMediaThriller -> execute();
         $arrayNotasMediasGenerales[1]=round($consultaMediaThriller->fetch()[0],3);
 
         //BELICO
         $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%Belico%') AND usuarioid=$idUsuarioNotasMediasGenero");
         $consultaMediaThriller -> execute();
         $arrayNotasMediasGenerales[2]=round($consultaMediaThriller->fetch()[0],3);
 
         //MAFIA
         $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%Mafia%') AND usuarioid=$idUsuarioNotasMediasGenero");
         $consultaMediaThriller -> execute();
         $arrayNotasMediasGenerales[3]=round($consultaMediaThriller->fetch()[0],3);
 
 
         //CIENCIA FICCION
         $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%ficcion%') AND usuarioid=$idUsuarioNotasMediasGenero");
         $consultaMediaThriller -> execute();
         $arrayNotasMediasGenerales[4]=round($consultaMediaThriller->fetch()[0],3);
 
         //ACCION
         $consultaMediaThriller =$pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE filmid in (SELECT idfilm FROM FilmRate.films WHERE genero like '%accion%') AND usuarioid=$idUsuarioNotasMediasGenero");
         $consultaMediaThriller -> execute();
         $arrayNotasMediasGenerales[5]=round($consultaMediaThriller->fetch()[0],3);
  
 
         echo json_encode($arrayNotasMediasGenerales);

}//FIN CONSULTA QUE OBTIENE LAS NOTAS MEDIAS DE UN USUARIO POR EL GENERO

//CONSULTA PARA SABER LAS NOTAS MEDIAS GENERALES (controladorEstadisticas.js)
if(isset($_POST["notaMediaGeneralFilmRate"])){

        
        $consultaNotaMediaGeneral = $pdo->prepare("SELECT avg(nota) FROM FilmRate.votos");
        $consultaNotaMediaGeneral->execute();

        echo ($consultaNotaMediaGeneral->fetch()[0]);


}//FIN CONSULTA PARA SABER LAS NOTAS MEDIAS GENERALES


//CONSULTA PARA SABER LAS NOTAS MEDIAS GENERALES (controladorEstadisticas.js)
if(isset($_POST["nickUsuarioMediaGeneral"])){

        $nickUsuarioNotaMediaGeneral =$_POST["nickUsuarioMediaGeneral"];
        
        $consultaNotaMediaGeneralUsuario = $pdo->prepare("SELECT avg(nota) FROM FilmRate.votos WHERE usuarioid=(SELECT idusuario FROM FilmRate.usuarios where nick='$nickUsuarioNotaMediaGeneral')");
        $consultaNotaMediaGeneralUsuario->execute();

        echo ($consultaNotaMediaGeneralUsuario->fetch()[0]);


}//FIN CONSULTA PARA SABER LAS NOTAS MEDIAS GENERALES


//CONSULTA SABER LOS 5 USUARIOS CON MAS VOTOS, SUS NICKS, SUS MEDIAS Y SUS ID (controladorEstadisticas.js)
//select avg(nota),count(nota) as cantidadvotos, nick, idusuario from votos inner join usuarios on votos.usuarioid = usuarios.idusuario group by idusuario order by cantidadvotos desc limit 5;
if(isset($_POST["obtenerCincoUsuariosConMasVotos"])){

        //$consultaCincoUsuariosMasVotos = $pdo->prepare("select avg(nota),count(nota) as cantidadvotos, nick, idusuario from votos inner join usuarios on votos.usuarioid = usuarios.idusuario group by idusuario order by cantidadvotos desc limit 5");
        $consultaCincoUsuariosMasVotos=$pdo->prepare("SELECT avg(nota)as media, count(nota) as cantidadvotos,nick,idusuario FROM FilmRate.votos INNER JOIN FilmRate.usuarios on FilmRate.votos.usuarioid = FilmRate.usuarios.idusuario GROUP BY idusuario ORDER BY cantidadvotos DESC LIMIT 5");
        $consultaCincoUsuariosMasVotos->execute();

        $arrayResultadoCincoUsuariosMasVotos=[];

        while($fila=$consultaCincoUsuariosMasVotos->fetch()){
                array_push($arrayResultadoCincoUsuariosMasVotos,$fila);
        }

        echo json_encode($arrayResultadoCincoUsuariosMasVotos);



}//FIN CONSULTA SABER LOS 5 USUARIOS CON MAS VOTOS, SUS NICKS, SUS MEDIAS Y SUS ID

//CONSULTA PARA OBTENER EL TOP 3 DE LAS PELICULAS CON MEJOR NOTA (controladorIndexUsuario.js)
//select filmid,avg(nota) as notamedia,titulo,fotoportada FROM 'votos' inner join films on votos.filmid=films.idfilm GROUP BY filmid ORDER BY notamedia desc LIMIT 3;
if(isset($_POST["topTresMejorVotadas"])){
        
        $consultaTresMejorNotaMedia = $pdo->prepare("SELECT filmid,avg(nota) AS notamedia,titulo,fotoportada FROM FilmRate.votos INNER JOIN FilmRate.films ON FilmRate.votos.filmid=FilmRate.films.idfilm GROUP BY filmid ORDER BY notamedia DESC LIMIT 3;");
        $consultaTresMejorNotaMedia->execute();

        $arrayTresMejores=[];
        while($fila=$consultaTresMejorNotaMedia->fetch()){
                array_push($arrayTresMejores,$fila);

                
        }


        echo json_encode($arrayTresMejores);

}//FIN CONSULTA PARA OBTENER EL TOP 3 DE LAS PELICULAS CON MEJOR NOTA

//CONSULTA PARA OBTENER EL TOP 3 DE LAS PELICULAS CON PEOR NOTA (controladorIndexUsuario.js)
//select filmid,avg(nota) as notamedia,titulo,fotoportada FROM 'votos' inner join films on votos.filmid=films.idfilm GROUP BY filmid ORDER BY notamedia desc LIMIT 3;
if(isset($_POST["topTresPeorVotadas"])){
        
        $consultaTresMejorNotaMedia = $pdo->prepare("SELECT filmid,avg(nota) AS notamedia,titulo,fotoportada FROM FilmRate.votos INNER JOIN FilmRate.films ON FilmRate.votos.filmid=FilmRate.films.idfilm GROUP BY filmid ORDER BY notamedia ASC LIMIT 3;");
        $consultaTresMejorNotaMedia->execute();

        $arrayTresMejores=[];
        while($fila=$consultaTresMejorNotaMedia->fetch()){
                array_push($arrayTresMejores,$fila);
                
        }


        echo json_encode($arrayTresMejores);

}//FIN CONSULTA PARA OBTENER EL TOP 3 DE LAS PELICULAS CON PEOR NOTA


//CONSULTA PARA OBTENER 3 MEJORES PELICULAS DE UN GENERO: (controladorIndexUsuario.js)
//{topTresMejoresGenero:"true",genero:tipoTopMostrar}
if(isset($_POST["topTresMejoresGenero"])&&isset($_POST["genero"])){
        
        $genero = $_POST["genero"];

        $consultaTresMejoresGenero = $pdo->prepare("SELECT filmid,avg(nota) AS notamedia,titulo,fotoportada FROM FilmRate.votos INNER JOIN FilmRate.films ON FilmRate.votos.filmid=FilmRate.films.idfilm WHERE genero LIKE '%$genero%' GROUP BY filmid ORDER BY notamedia DESC LIMIT 3;");
        $consultaTresMejoresGenero->execute();

        $arrayTresMejoresGenero=[];
        while($fila=$consultaTresMejoresGenero->fetch()){
                array_push($arrayTresMejoresGenero,$fila);
        }

        echo json_encode($arrayTresMejoresGenero);


}//FIN CONSULTA PARA OBTENER 3 MEJORES PELICULAS DE UN GENERO


//CONSULTA PARA SABER EL NUMERO TOTAL DE PELICULAS DE LA BASE DE DATOS (controladorListaFilmsUsuario.js)
if(isset($_POST["numeroTotalPeliculas"])){
        $consultaNumeroTotalPeliculas = $pdo->prepare("SELECT count(*) FROM FilmRate.films");
        $consultaNumeroTotalPeliculas->execute();

        echo ($consultaNumeroTotalPeliculas->fetch()[0]);

}//FIN CONSULTA PARA SABER EL NUMERO TOTAL DE PELICULAS DE LA BASE DE DATOS

//CONSULTA PARA MOSTRAR DEVOLVER TODAS LAS PELICULAS (controladorListaFilmUsuario.js,controladorListaFilmsAdmin.js)
if(isset($_POST["obtenerTodasLasPeliculas"])){

        $consultaTodasLasPeliculas = $pdo->prepare("SELECT fotoportada,titulo,idfilm FROM FilmRate.films ORDER BY titulo ASC");
        $consultaTodasLasPeliculas->execute();

        $arrayTodasLasPeliculas=[];
        while($fila=$consultaTodasLasPeliculas->fetch()){
                array_push($arrayTodasLasPeliculas,$fila);
                
        }


        echo json_encode($arrayTodasLasPeliculas);

}//FIN CONSULTA PARA MOSTRAR 10 PELICULAS 


//CONSULTA PARA OBTENER EL ID DE LAS PELICULAS FAVORITAS DE UN USUARIO (controladorListaFavoritasUsuario.js):
if(isset($_POST["obtenerFavoritasUsuario"])&&isset($_POST["idUsuarioFavoritas"])){
        $idUsuarioFavoritas = $_POST["idUsuarioFavoritas"];

        //$consultaFavoritasUsuario = $pdo->prepare("SELECT titulo,filmid FROM FilmRate.favoritas INNER JOIN FilmRate.films ON FilmRate.favoritas.filmid=films.idfilm WHERE usuarioid = '$idUsuarioFavoritas'");
        $consultaFavoritasUsuario = $pdo->prepare("SELECT filmid,titulo FROM FilmRate.favoritas INNER JOIN FilmRate.films ON FilmRate.favoritas.filmid=films.idfilm WHERE usuarioid = '$idUsuarioFavoritas' ORDER BY titulo ASC");
        $consultaFavoritasUsuario->execute();

        $arrayFavoritasUsuario=[];
        while($fila=$consultaFavoritasUsuario->fetch()){
                array_push($arrayFavoritasUsuario,$fila);
        }

        echo json_encode($arrayFavoritasUsuario);
        
}//FIN OBTENER ID PELICULAS FAVORITAS DEL USUARIO


//CONSULTA PARA OBTENER LA INFORMACIÓN COMPLETA DE CADA PELICULA FAVORITA DEL USUARIO (controladorListaFavoritasUsuario.js):
if(isset($_POST["obtenerInfoFavorita"])&&isset($_POST["idFavoritaObtenerInfo"])){
        
        $idPeliculaFavoritaUsuario = $_POST["idFavoritaObtenerInfo"];

        $consultaInformacionPeliculaFavorita = $pdo->prepare("SELECT titulo,fotoportada,idfilm FROM FilmRate.films WHERE idfilm = '$idPeliculaFavoritaUsuario'");
        $consultaInformacionPeliculaFavorita->execute();

        $arrayInfoFavoritaUsuario = [];
        while($fila=$consultaInformacionPeliculaFavorita->fetch()){
                array_push($arrayInfoFavoritaUsuario,$fila);
        }

        echo json_encode($arrayInfoFavoritaUsuario);

}//FIN //CONSULTA PARA OBTENER LA INFORMACIÓN COMPLETA DE CADA PELICULA FAVORITA DEL USUARIO



//CONSULTA PARA ELIMINAR UNA PELICULA FAVORITA DE UN USUARIO (controladorListaFavoritasUsuario.js):
if(isset($_POST["eliminarFavoritaListaFavoritas"])&&isset($_POST["idUsuarioFavoritasEliminar"])&&isset($_POST["idPeliculaEliminarFavorita"])){
        session_start();
        $idFilmEliminarFavorito = $_POST["idPeliculaEliminarFavorita"];
        $idUsuarioEliminarFavorito = $_POST["idUsuarioFavoritasEliminar"];

        echo $idFilmEliminarFavorito;

        //Compruebo si el usuario recibido es igual al que tiene la sesion inciada
        if($_SESSION["idusuario"]==$idUsuarioEliminarFavorito){                      

               $consultaEliminarFilmFavorito = $pdo->prepare("DELETE FROM FilmRate.favoritas WHERE usuarioid=$idUsuarioEliminarFavorito AND filmid=$idFilmEliminarFavorito");
               $consultaEliminarFilmFavorito->execute();

               echo ("ELIMINADODEFAVORITOS");        
        }
}//FIN CONSULTA PARA ELIMINAR UNA PELICULA FAVORITA DE UN USUARIO


//CONSULTA PARA OBTENER LAS PELICULAS QUE HAN SIDO VOTADAS POR EL USUARIO (controladorVotosUsuario.js):
if(isset($_POST["obtenerVotosUsuario"])&&isset($_POST["idUsuarioVotos"])){
        
        $idUsuarioConsulta = $_POST["idUsuarioVotos"];

        $consultaVotosUsuario = $pdo->prepare("SELECT filmid,nota,titulo,fotoportada FROM FilmRate.votos INNER JOIN FilmRate.films ON FilmRate.votos.filmid=FilmRate.films.idfilm WHERE usuarioid ='$idUsuarioConsulta' ORDER BY titulo ASC");
        $consultaVotosUsuario->execute();

        $arrayVotosUsuario = [];

        while($fila=$consultaVotosUsuario->fetch()){
                array_push($arrayVotosUsuario,$fila);
        }

        echo json_encode($arrayVotosUsuario);



}//FIN CONSULTA PARA OBTENER LAS PELICULAS QUE HAN SIDO VOTADAS POR EL USUARIO


//CONSULTA PARA OBTENER LAS PELICULAS QUE HAN SIDO VOTADAS POR EL USUARIO DE FORMA DESCENDENTE (controladorVotosUsuario.js):
if(isset($_POST["obtenerVotosDESCUsuario"])&&isset($_POST["idUsuarioVotos"])){
        
        $idUsuarioConsulta = $_POST["idUsuarioVotos"];

        $consultaVotosDESCUsuario = $pdo->prepare("SELECT filmid,nota,titulo,fotoportada FROM FilmRate.votos INNER JOIN FilmRate.films ON FilmRate.votos.filmid=FilmRate.films.idfilm WHERE usuarioid ='$idUsuarioConsulta' ORDER BY titulo DESC");
        $consultaVotosDESCUsuario->execute();

        $arrayVotosDESCUsuario = [];

        while($fila=$consultaVotosDESCUsuario->fetch()){
                array_push($arrayVotosDESCUsuario,$fila);
        }

        echo json_encode($arrayVotosDESCUsuario);

}//FIN CONSULTA PARA OBTENER LAS PELICULAS QUE HAN SIDO VOTADAS POR EL USUARIO DE FORMA DESCENDENTE

//CONSULTA PARA OBTENER LAS PELICULAS QUE HAN SIDO VOTADAS POR EL USUARIO DE FORMA DESCENDENTE POR NOTA (controladorVotosUsuario.js):
if(isset($_POST["obtenerVotosNotaDESCUsuario"])&&isset($_POST["idUsuarioVotos"])){
        
        $idUsuarioConsulta = $_POST["idUsuarioVotos"];

        $consultaVotosNotaDESCUsuario = $pdo->prepare("SELECT filmid,nota,titulo,fotoportada FROM FilmRate.votos INNER JOIN FilmRate.films ON FilmRate.votos.filmid=FilmRate.films.idfilm WHERE usuarioid ='$idUsuarioConsulta' ORDER BY nota DESC");
        $consultaVotosNotaDESCUsuario->execute();

        $arrayVotosNotaDESCUsuario = [];

        while($fila=$consultaVotosNotaDESCUsuario->fetch()){
                array_push($arrayVotosNotaDESCUsuario,$fila);
        }

        echo json_encode($arrayVotosNotaDESCUsuario);

}//FIN CONSULTA PARA OBTENER LAS PELICULAS QUE HAN SIDO VOTADAS POR EL USUARIO DE FORMA DESCENDENTE POR NOTA

//CONSULTA PARA OBTENER LAS PELICULAS QUE HAN SIDO VOTADAS POR EL USUARIO DE FORMA ASCENDENTE POR NOTA (controladorVotosUsuario.js):
if(isset($_POST["obtenerVotosNotaASCUsuario"])&&isset($_POST["idUsuarioVotos"])){
        
        $idUsuarioConsulta = $_POST["idUsuarioVotos"];

        $consultaVotosNotaASCUsuario = $pdo->prepare("SELECT filmid,nota,titulo,fotoportada FROM FilmRate.votos INNER JOIN FilmRate.films ON FilmRate.votos.filmid=FilmRate.films.idfilm WHERE usuarioid ='$idUsuarioConsulta' ORDER BY nota ASC");
        $consultaVotosNotaASCUsuario->execute();

        $arrayVotosNotaASCUsuario = [];

        while($fila=$consultaVotosNotaASCUsuario->fetch()){
                array_push($arrayVotosNotaASCUsuario,$fila);
        }

        echo json_encode($arrayVotosNotaASCUsuario);

}//FIN CONSULTA PARA OBTENER LAS PELICULAS QUE HAN SIDO VOTADAS POR EL USUARIO DE FORMA ASCENDENTE POR NOTA








//CONSULTA PARA OBTENER LAS CRITICAS DE UN USUARIO (controladorCriticasUsuario.js):
if(isset($_POST["obtenerCriticasUsuario"])&&isset($_POST["idUsuarioListaCriticas"])){

        $idUsuarioListaCriticas = $_POST["idUsuarioListaCriticas"];

        $consultaCriticasUsuario = $pdo->prepare("SELECT filmid,titulo,fotoportada,textocritica,fechacritica FROM FilmRate.criticas INNER JOIN FilmRate.films ON FilmRate.criticas.filmid=FilmRate.films.idfilm WHERE usuarioid='$idUsuarioListaCriticas' ORDER BY titulo ASC");
        $consultaCriticasUsuario->execute();


        $arrayCriticasUsuario = [];
        while($fila=$consultaCriticasUsuario->fetch()){
                array_push($arrayCriticasUsuario,$fila);
        }

        echo json_encode($arrayCriticasUsuario);


}//FIN CONSULTA PARA OBTENER LAS CRITICAS DE UN USUARIO

//CONSULTA PARA MODIFICAR LOS DATOS DE UN FILM COMO ADMINISTRADOR (controladorFilmAdmin.js)
if(isset($_POST["arrayModificarFilm"])&&isset($_POST["idFilmEncontradoAdmin"])&&isset($_POST["idAdmin"])){
        
        session_start();
        $idAdmin = $_POST["idAdmin"];
        $idFilmModificar = $_POST["idFilmEncontradoAdmin"];
        $arrayDatosFilm = $_POST["arrayModificarFilm"];
        //[0] -> Titulo
        //[1] -> Tipo (pelicula)
        //[2] -> Director
        //[3] -> Año
        //[4] -> Duracion en Minutos
        //[5] -> Guion
        //[6] -> Genero
        //[7] -> Reparto
        //[8] -> Sinopsis

        $titulo = $arrayDatosFilm[0];
        $tipo = $arrayDatosFilm[1];
        $director = $arrayDatosFilm[2];
        $fechaestreno =  $arrayDatosFilm[3];
        $duracion = $arrayDatosFilm[4];
        $guion =  $arrayDatosFilm[5];
        $genero = $arrayDatosFilm[6];
        $reparto =  $arrayDatosFilm[7];        
        $sinopsis =  $arrayDatosFilm[8];

        
        //Saber si no es admin:
        $consultaEsAdministrador =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministrador->execute();

        //Si el nick introducido es de un administrador, devolver error
        if($consultaEsAdministrador->fetch()[0]==1||$consultaEsAdministrador->fetch()[0]=="1"){
                //echo ("ES ADMIN");
                //Compruebo si el usuario recibido es igual al que tiene la sesion inciada
                if($_SESSION["idusuario"]==$idAdmin){    
                        
                        //$consultaModificarFilm = $pdo->prepare("INSERT INTO FilmRate.films ('titulo','tipo','director','fechaestreno','duracion','guion','genero','reparto','sinopsis') VALUES ('$titulo','$tipo','$director','$fechaestreno','$duracion','$guion','$genero','$reparto','$sinopsis') WHERE idfilm='$idFilmModificar'");
                        $consultaModificarFilm = $pdo->prepare("UPDATE FilmRate.films SET titulo='$titulo',tipo='$tipo',director='$director',fechaestreno='$fechaestreno-01-01',duracion='$duracion',guion='$guion',genero='$genero',reparto='$reparto',sinopsis='$sinopsis'  WHERE idfilm='$idFilmModificar'");
                        $consultaModificarFilm->execute();
                        echo "FILMMODIFICADO";

                        
                }//Fin si el usuario recibido es igual al de la sesion iniciada
        }else{
                echo ("Error: ES NECESARIO SER ADMINISTRADOR");
        }        





}//FIN CONSULTA PARA MODIFICAR LOS DATOS DE UN FILM COMO ADMINISTRADOR

//CONSULTA SUBIR FOTO AL EDITAR EL FILM COMO ADMINISTRADOR (controladorFilmAdmin.js)
if(isset($_FILES["file"])){
        if(($_FILES["file"]["type"]=="image/png")||($_FILES["file"]["type"]=="image/jpg")||($_FILES["file"]["type"]=="image/jpeg")){
                if(move_uploaded_file($_FILES["file"]["tmp_name"], "/var/www/html/fotos/".$_FILES["file"]["name"])){
                        echo "SUBIDA";
                }

        }




        
}//FIN CONSULTA SUBIR FOTO AL EDITAR EL FILM COMO ADMINISTRADOR

//CONSULTA CAMBIAR DIRECCION DE FOTO DE FILM EN BD (controladorFilmAdmin.js)
if(isset($_POST["fotoSubirFilm"])&&isset($_POST["idFilmModificarFotoAdmin"])&&isset($_POST["idAdmin"])){
        session_start();
        $idAdmin = $_POST["idAdmin"];
        //Saber si no es admin:
        $consultaEsAdministradorModificarFoto =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorModificarFoto->execute();

        $fotoportada=$_POST["fotoSubirFilm"];
        $idFilmModificarFoto = $_POST["idFilmModificarFotoAdmin"];

        //Si el nick introducido es de un administrador, devolver error
        if($consultaEsAdministradorModificarFoto->fetch()[0]==1||$consultaEsAdministradorModificarFoto->fetch()[0]=="1"){
                $consultaModificarFotoFilm = $pdo->prepare("UPDATE FilmRate.films SET fotoportada='fotos/$fotoportada' WHERE idfilm='$idFilmModificarFoto'");
                $consultaModificarFotoFilm->execute();
                echo "FOTOMODIFICADACONEXITO";
        }

}//FIN CONSULTA CAMBIAR DIRECCION DE FOTO DE FILM EN BD


//CONSULTA PARA OBTENER LA INFORMACION DE LOS PAISES DE LA BASE DE DATOS (controladorFilmAdmin.js,controladorCrearFilmsAdmin.js)
if(isset($_POST["obtenerInfoPaisesFilms"])){

        $consultaPaisesEditarFilm = $pdo->prepare("SELECT * FROM FilmRate.paises ORDER BY nombrepaiscastellano ASC");
        $consultaPaisesEditarFilm -> execute();
       

        $arrayPaisesEditarFilm = [];
        while($fila=$consultaPaisesEditarFilm->fetch()){
                array_push($arrayPaisesEditarFilm,$fila);
        }

        echo json_encode($arrayPaisesEditarFilm);

}//FIN CONSULTA PARA OBTENER LA INFORMACION DE LOS PAISES DE LA BASE DE DATOS


//CONSULTA PARA MODIFICAR EL PAIS DEL FILM ACTUAL (controladorFilmAdmin.js)
if(isset($_POST["idPaisNuevo"])&&isset($_POST["idFilmModificarPaisAdmin"])&&isset($_POST["idAdmin"])){
        
        session_start();
        $idAdmin = $_POST["idAdmin"];
        //Saber si no es admin:
        $consultaEsAdministradorModificarFoto =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorModificarFoto->execute();

        $idNuevoPais = $_POST["idPaisNuevo"];
        $idFilmModificarPais = $_POST["idFilmModificarPaisAdmin"];

        //Si el nick introducido es de un administrador, devolver error
        if($consultaEsAdministradorModificarFoto->fetch()[0]==1||$consultaEsAdministradorModificarFoto->fetch()[0]=="1"){
                $consultaModificarFotoFilm = $pdo->prepare("UPDATE FilmRate.films SET pais='$idNuevoPais' WHERE idfilm='$idFilmModificarPais'");
                $consultaModificarFotoFilm->execute();
                echo "PAISMODIFICADOCONEXITO";
        }



}//FIN CONSULTA PARA MODIFICAR EL PAIS DEL FILM ACTUAL

//CONSULTA PARA ELIMINAR UN FILM COMO ADMINISTRADOR (controladorFilmAdmin.js)
//{eliminarFilmAdmin:"true",idFilmEliminar:idFilmEncontrado,idAdmin:sessionStorage.Id}
if(isset($_POST["eliminarFilmAdmin"])&&isset($_POST["idFilmEliminar"])&&isset($_POST["idAdmin"])){
        $idAdmin = $_POST["idAdmin"];
        $idFilmEliminar = $_POST["idFilmEliminar"];
 
        session_start();
        
        //Saber si no es admin:
        $consultaEsAdministradorModificarFoto =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorModificarFoto->execute();        

        //Si el nick introducido es de un administrador, devolver error
        if($consultaEsAdministradorModificarFoto->fetch()[0]==1||$consultaEsAdministradorModificarFoto->fetch()[0]=="1"){
                $consultaEliminarFilm = $pdo->prepare("DELETE FROM FilmRate.films WHERE idfilm = $idFilmEliminar");
                $consultaEliminarFilm ->execute();
                echo "FILMELIMINADO";
        }

        
}
//FIN CONSULTA PARA ELIMINAR UN FILM COMO ADMINISTRADOR

//CONSULTA PARA BUSCAR PELICULAS COMO ADMINISTRADOR (controladorListaFilmsAdmin.js)
if(isset($_POST["buscarPeliculasAdmin"])&&isset($_POST["tituloBuscarFilmAdmin"])){

        $tituloFilmBuscar = $_POST["tituloBuscarFilmAdmin"];
        
                
        $consultaBuscarFilmsAdmin =$pdo->prepare("SELECT fotoportada,titulo,idfilm FROM FilmRate.films WHERE titulo like '%$tituloFilmBuscar%'");
        $consultaBuscarFilmsAdmin -> execute();

        $arrayPeliculasEncontradasAdmin = [];
        while($fila=$consultaBuscarFilmsAdmin->fetch()){
                array_push($arrayPeliculasEncontradasAdmin,$fila);
        }

        echo json_encode($arrayPeliculasEncontradasAdmin);


}//FIN CONSULTA PARA BUSCAR PELICULAS COMO ADMINISTRADOR

//CONSULTA PARA SABER EL NUMERO TOTAL DE USUARIOS DE LA BASE DE DATOS (controladorListaUsuariosAdmin.js)
if(isset($_POST["numeroTotalUsuarios"])){
        $consultaNumeroTotalUsuarios = $pdo->prepare("SELECT count(*) FROM FilmRate.usuarios");
        $consultaNumeroTotalUsuarios->execute();

        echo ($consultaNumeroTotalUsuarios->fetch()[0]);

}//FIN CONSULTA PARA SABER EL NUMERO TOTAL DE USUARIOS DE LA BASE DE DATOS


//CONSULTA PARA OBTENER LA INFORMACIÓN DE LOS USUARIOS COMO ADMINISTRADOR (controladorListaUsuariosAdmin.js)
if(isset($_POST["obtenerTodosLosUsuarios"])&&isset($_POST["idAdmin"])){

        session_start();
        $idAdmin = $_POST["idAdmin"];
        //Saber si no es admin:
        $consultaEsAdministradorModificarFoto =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorModificarFoto->execute();

        //Si el nick introducido es de un administrador
        if($consultaEsAdministradorModificarFoto->fetch()[0]==1||$consultaEsAdministradorModificarFoto->fetch()[0]=="1"){
                $consultaObtenerUsuarios = $pdo->prepare("SELECT nick,nombre,apellidos,activa FROM FilmRate.usuarios WHERE idusuario!='$idAdmin' ORDER BY nick ASC");
                $consultaObtenerUsuarios->execute();
                $arrayUsuarios = [];
                while($fila=$consultaObtenerUsuarios->fetch()){
                        array_push($arrayUsuarios,$fila);
                }
                echo json_encode($arrayUsuarios);
        }

}//FIN CONSULTA PARA OBTENER LA INFORMACIÓN DE LOS USUARIOS COMO ADMINISTRADOR




//CONSULTA PARA OBTENER TODA LA INFORMACIÓN DE UN USUARIO A PARTIR DE SU NICK (controladorPerfilUsuariosAdmin.js)
if(isset($_POST["nickUsuarioBuscarInfoAdmin"])&&isset($_POST["idAdmin"])){
        session_start();
        $idAdmin = $_POST["idAdmin"];
        //Saber si no es admin:
        $consultaEsAdministradorModificarFoto =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorModificarFoto->execute();

        $nickUsuarioBuscarInfo = $_POST["nickUsuarioBuscarInfoAdmin"];


        //Si el nick introducido es de un administrador
        if($consultaEsAdministradorModificarFoto->fetch()[0]==1||$consultaEsAdministradorModificarFoto->fetch()[0]=="1"){
                //$consultaObtenerInfoUsuarioAdmin = $pdo->prepare("SELECT FilmRate.usuarios.idusuario,'nick','email','nombre','apellidos','admin','hash','activa' FROM FilmRate.usuarios WHERE nick='$nickUsuarioBuscarInfo'");
                $consultaObtenerInfoUsuarioAdmin = $pdo->prepare("SELECT FilmRate.usuarios.idusuario,FilmRate.usuarios.nick,FilmRate.usuarios.email,FilmRate.usuarios.nombre,FilmRate.usuarios.apellidos,FilmRate.usuarios.admin,FilmRate.usuarios.hash,FilmRate.usuarios.activa FROM FilmRate.usuarios WHERE nick='$nickUsuarioBuscarInfo'");
                //$consultaObtenerInfoUsuarioAdmin = $pdo->prepare("SELECT * FROM FilmRate.usuarios WHERE nick='$nickUsuarioBuscarInfo'");
                $consultaObtenerInfoUsuarioAdmin -> execute();
                $arrayInformacionUsuario = [];
                while($fila=$consultaObtenerInfoUsuarioAdmin->fetch()){
                        array_push($arrayInformacionUsuario,$fila);
                }
                

                echo json_encode($arrayInformacionUsuario);


        }
}//FIN CONSULTA PARA OBTENER TODA LA INFORMACIÓN DE UN USUARIO A PARTIR DE SU NICK 


//CONSULTA PARA MODIFICAR LOS DATOS DEL USUARIO COMO ADMINISTRADOR (controladorPerfilUsuariosAdmin.js)
if(isset($_POST["arrayDatosModificarUsuario"])&&isset($_POST["idAdmin"])&&isset($_POST["idUsuarioAModificar"])){
        //echo $_POST["arrayDatosModificarUsuario"][1];

        $idUsuarioAModificar= $_POST["idUsuarioAModificar"];//Este es el nick del usuario que estamos modificando

        //Datos modificados que tendremos que introducir en la base de datos
        $nickModificado = $_POST["arrayDatosModificarUsuario"][0]; //Comprobar que no hay uno igual
        $emailModificado = $_POST["arrayDatosModificarUsuario"][1]; //Comprobar que no hay uno igual
        $nombreModificado = $_POST["arrayDatosModificarUsuario"][2];
        $apellidosModificado = $_POST["arrayDatosModificarUsuario"][3];
        $tipoModificado = $_POST["arrayDatosModificarUsuario"][4];


        session_start();
        $idAdmin = $_POST["idAdmin"];
        //Saber si no es admin:
        $consultaEsAdministradorModificarFoto =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorModificarFoto->execute();      

        //Si el nick introducido es de un administrador
        if($consultaEsAdministradorModificarFoto->fetch()[0]==1||$consultaEsAdministradorModificarFoto->fetch()[0]=="1"){
                //Tengo que comprobar si el nick y el correo ya están introducidos en la base de datos
                $consultaComprobarNick = $pdo->prepare("SELECT nick FROM FilmRate.usuarios WHERE nick='$nickModificado' AND idusuario!='$idUsuarioAModificar'");
                $consultaComprobarNick->execute();
                //SI SE ENCUENTRA NICK
                if($consultaComprobarNick->rowcount()==1){
                        //Cuando el nick existe en la base de datos devuelvo lo siguiente
                        echo "NICKEXISTENTE";
                }else{//Cuando el nick no existe en la base de datos puedo modificarlo
                        //SI SE ENCUENTRA EL CORREO:
                        $consultaComprobarEmail = $pdo->prepare("SELECT email FROM FilmRate.usuarios WHERE email='$emailModificado' AND idusuario!='$idUsuarioAModificar'");
                        $consultaComprobarEmail->execute();
                        if($consultaComprobarEmail->rowcount()==1){
                                //Cuando el nick existe en la base de datos devuelvo lo siguiente
                                echo "EMAILEXISTENTE";
                        }else{//Cuando no se repiten los datos importantes en la base de datos debo actualizar el usuario:
                                

                                $consultaModificacionUsuario = $pdo->prepare("UPDATE FilmRate.usuarios SET nick='$nickModificado',email='$emailModificado',nombre='$nombreModificado',apellidos='$apellidosModificado',admin='$tipoModificado' WHERE idusuario='$idUsuarioAModificar'");
                                $consultaModificacionUsuario -> execute();
                                
                                echo "USUARIOMODIFICADO";
                        }//fin si los datos no se repiten
                }//Fin cuando el nick no existe

        }//Fin si el que modifica es administrador


}
//FIN CONSULTA PARA MODIFICAR LOS DATOS DEL USUARIO COMO ADMINISTRADOR

//CONSULTA PARA ACTIVAR UN USUARIO QUE TIENE LA CUENTA DESACTIVADA (controladorPerfilUsuariosAdmin.js)
if(isset($_POST["activarCuenta"])&&isset($_POST["idAdmin"])&&isset($_POST["idCuentaActivar"])){
        
        session_start();
        $idAdmin = $_POST["idAdmin"];
        //Saber si no es admin:
        $consultaEsAdministradorModificarFoto =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorModificarFoto->execute();      

        $idUsuarioActivar = $_POST["idCuentaActivar"];

        //Si el nick introducido es de un administrador
        if($consultaEsAdministradorModificarFoto->fetch()[0]==1||$consultaEsAdministradorModificarFoto->fetch()[0]=="1"){

                $consultaActivarCuenta = $pdo->prepare("UPDATE FilmRate.usuarios SET activa='1' WHERE idusuario='$idUsuarioActivar'");
                $consultaActivarCuenta -> execute();

                echo "CUENTAACTIVADA";
        }
}
//FIN CONSULTA PARA ACTIVAR UN USUARIO QUE TIENE LA CUENTA DESACTIVADA

//CONSULTA PARA QUE EL ADMIN CAMBIE LA CONTRASEÑA DE UN USUARIO (controladorPerfilUsuariosAdmin.js)
if(isset($_POST["administradorCambiaPassword"])&&isset($_POST["idAdmin"])&&isset($_POST["idUsuarioCambiarPassword"])&&isset($_POST["nuevaPasswordUsuario"])){
         
        session_start();
        $idAdmin = $_POST["idAdmin"];
        //Saber si no es admin:
        $consultaEsAdministradorModificarFoto =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorModificarFoto->execute();      

        $idUsuarioCambiarPasswd = $_POST["idUsuarioCambiarPassword"];
        $nuevaPasswordUsuario = $_POST["nuevaPasswordUsuario"];
        $nuevaPasswordEnctriptada = encriptar($nuevaPasswordUsuario);

        //Si el nick introducido es de un administrador
        if($consultaEsAdministradorModificarFoto->fetch()[0]==1||$consultaEsAdministradorModificarFoto->fetch()[0]=="1"){
                $consultaCambiarPasswordAdmin = $pdo->prepare("UPDATE FilmRate.usuarios SET password='$nuevaPasswordEnctriptada' WHERE idusuario='$idUsuarioCambiarPasswd'");
                $consultaCambiarPasswordAdmin ->execute();

                echo "PASSWORDUSUARIOCAMBIADA";
        }
}
//FIN CONSULTA PARA QUE EL ADMIN CAMBIE LA CONTRASEÑA DE UN USUARIO 

//CONSULTA PARA ELIMINAR UN USUARIO (controladorPerfilUsuariosAdmin.js)
if(isset($_POST["idUsuarioEliminar"])&&isset($_POST["idAdmin"])){
        session_start();
        $idAdmin = $_POST["idAdmin"];
        //Saber si no es admin:
        $consultaEsAdministradorModificarFoto =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorModificarFoto->execute();  

        $idUsuarioEliminar = $_POST["idUsuarioEliminar"];

        //Si el nick introducido es de un administrador
        if($consultaEsAdministradorModificarFoto->fetch()[0]==1||$consultaEsAdministradorModificarFoto->fetch()[0]=="1"){
                $consultaAdminEliminarCuenta = $pdo->prepare("DELETE FROM FilmRate.usuarios WHERE idusuario='$idUsuarioEliminar'");
                $consultaAdminEliminarCuenta ->execute();
                echo "USUARIOELIMINADO";
        }
}//FIN CONSULTA PARA ELIMINAR UN USUARIO


//CONSULTA PARA BUSCAR USUARIOS COMO ADMINISTRADOR (controladorListaFilmsAdmin.js)
if(isset($_POST["buscarUsuariosAdmin"])&&isset($_POST["usuarioBuscarAdmin"])){

        $usuarioBuscar = $_POST["usuarioBuscarAdmin"];
        
                
        $consultaBuscarUsuariosAdmin =$pdo->prepare("SELECT nick,nombre,apellidos,activa FROM FilmRate.usuarios WHERE nick like '%$usuarioBuscar%' OR nombre like '%$usuarioBuscar%' OR apellidos like '%$usuarioBuscar%' ORDER BY nombre ASC");
        $consultaBuscarUsuariosAdmin -> execute();

        $arrayUsuariosEncontradosAdmin = [];
        while($fila=$consultaBuscarUsuariosAdmin->fetch()){
                array_push($arrayUsuariosEncontradosAdmin,$fila);
        }

        echo json_encode($arrayUsuariosEncontradosAdmin);


}//FIN CONSULTA PARA BUSCAR USUARIOS COMO ADMINISTRADOR


//CONSULTA PARA QUE EL ADMINISTRADOR CREE USUARIOS (controladorCrearUsuariosAdmin.js)
if(isset($_POST["crearUsuarioAdmin"])&&isset($_POST["crearNick"])&&isset($_POST["crearEmail"])&&isset($_POST["crearPassword"])&&isset($_POST["crearNombre"])&&isset($_POST["crearApellidos"])&&isset($_POST["tipoUsuario"])&&isset($_POST["cuentaActivada"])&&isset($_POST["idAdmin"])){
        
        //Creo las variables
        $nick=$_POST["crearNick"];
        $email=$_POST["crearEmail"];
        $password=encriptar($_POST["crearPassword"]);
        $nombre=ucwords($_POST["crearNombre"]);
        $apellidos=ucwords($_POST["crearApellidos"]);
        $tipoUsuario=$_POST["tipoUsuario"];
        $cuentaActivada=$_POST["cuentaActivada"];
        $hash = md5(rand(0,1000));

        //echo $email;
        
        //Compruebo que es administrador el que va a realizar la consulta
        session_start();
        $idAdmin = $_POST["idAdmin"];
        //Saber si no es admin:
        $consultaEsAdministradorCrearUsuario =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorCrearUsuario->execute();  

        //Si el nick introducido es de un administrador
        if($consultaEsAdministradorCrearUsuario->fetch()[0]==1||$consultaEsAdministradorCrearUsuario->fetch()[0]=="1"){
                //Tengo que comprobar la existencia del nick en la base de datos
                //Tengo que comprobar si el nick y el correo ya están introducidos en la base de datos
                $consultaComprobarNick = $pdo->prepare("SELECT nick FROM FilmRate.usuarios WHERE nick='$nick'");
                $consultaComprobarNick->execute();
                //SI SE ENCUENTRA NICK
                if($consultaComprobarNick->rowcount()==1){
                        //Cuando el nick existe en la base de datos devuelvo lo siguiente
                        echo "NICKEXISTENTE";
                }else{//Cuando el nick no existe en la base de datos puedo modificarlo
                        //SI SE ENCUENTRA EL CORREO:
                        $consultaComprobarEmail = $pdo->prepare("SELECT email FROM FilmRate.usuarios WHERE email='$email'");
                        $consultaComprobarEmail->execute();
                        if($consultaComprobarEmail->rowcount()==1){
                                //Cuando el nick existe en la base de datos devuelvo lo siguiente
                                echo "EMAILEXISTENTE";
                        }else{//Cuando no se repiten los datos importantes en la base de datos debo actualizar el usuario:
                                
                                //$consultaCrearUsuarioAdmin = $pdo->prepare("INSERT INTO FilmRate.usuarios ('nick','email','password','nombre','apellidos','admin','hash','activa') VALUES ($nick,$email,$password,$nombre,$apellidos,$tipoUsuario,$hash,$cuentaActiva)");
                                $consultaCrearUsuarioAdmin=$pdo->prepare("INSERT INTO FilmRate.usuarios (`nick`, `email`, `password`, `nombre`, `apellidos`, `admin`, `hash`, `activa`) VALUES ('$nick','$email','$password','$nombre','$apellidos','$tipoUsuario','$hash','$cuentaActivada')");
                                $consultaCrearUsuarioAdmin->execute();
                                
                                
                                echo "USUARIOCREADO";
                        }//fin si los datos no se repiten
                }//Fin cuando el nick no existe

        }//Fin comprobar si es administrador



}//FIN CONSULTA PARA QUE EL ADMINISTRADOR CREE USUARIOS



//CONSULTA SUBIR FOTO AL EDITAR EL FILM COMO ADMINISTRADOR (controladorCrearFilmsAdmin.js)
if(isset($_POST["nombrefoto"])&&isset($_FILES["fotos"])){  
        //echo  print_r($_FILES["fotos"]);
        //echo $_POST["nombrefoto"];

        $nombreArchivoGuardar = $_POST["nombrefoto"];


        //Tengo que hacer una comprobación para ver si no se duplica la foto:
        $consultaFotoDuplicada = $pdo->prepare("SELECT COUNT(*) FROM FilmRate.films WHERE fotoportada like '%$nombreArchivoGuardar'");
        $consultaFotoDuplicada ->execute();

        //COMPRUEBO SI NO HAY OTRA FOTO CON EL MISMO NOMBRE
        if($consultaFotoDuplicada->fetch()[0]<1){
                //LA SUBO TAL CUAL
                if(($_FILES["fotos"]["type"]=="image/png")||($_FILES["fotos"]["type"]=="image/jpg")||($_FILES["fotos"]["type"]=="image/jpeg")){
                        if(move_uploaded_file($_FILES["fotos"]["tmp_name"], "/var/www/html/fotos/".$nombreArchivoGuardar)){
                                echo $nombreArchivoGuardar;
                        }
        
                }
        }else{//SI YA EXISTE UNA FOTO ASI LE CAMBIO EL NOMBRE A LA IMAGEN Y LA SUBO CON OTRO NOMBRE

                //Le añadire un 1
                $array = explode(".",$nombreArchivoGuardar);
                $nombreArchivoGuardarNuevo = $array[0]."1.".$array[1];
                
                //LA VUELVO A SUBIR
                if(($_FILES["fotos"]["type"]=="image/png")||($_FILES["fotos"]["type"]=="image/jpg")||($_FILES["fotos"]["type"]=="image/jpeg")){
                        if(move_uploaded_file($_FILES["fotos"]["tmp_name"], "/var/www/html/fotos/".$nombreArchivoGuardarNuevo)){
                                echo $nombreArchivoGuardarNuevo;
                        }        
                }               

        }
                
        
}//FIN CONSULTA SUBIR FOTO AL EDITAR EL FILM COMO ADMINISTRADOR


//CONSULTA PARA CREAR UN FILM NUEVO EN LA BASE DE DATOS (controladorCrearFilmsAdmin)
if(isset($_POST["tipo"])&&isset($_POST["idAdmin"])&&isset($_POST["tituloFinal"])&&isset($_POST["fecha"])&&isset($_POST["pais"])&&isset($_POST["director"])&&isset($_POST["genero"])&&isset($_POST["sinopsis"])&&isset($_POST["fotoFilmFinal"])&&isset($_POST["duracion"])&&isset($_POST["guion"])&&isset($_POST["reparto"])){
        //Creo las variables con los datos:
        $titulo = str_replace("'"," ",trim($_POST["tituloFinal"]));
        $fecha = str_replace("'"," ",trim($_POST["fecha"]."-01-01"));
        $pais = str_replace("'"," ",trim($_POST["pais"]));
        $director = str_replace("'"," ",trim($_POST["director"]));
        $genero = str_replace("'"," ",trim($_POST["genero"]));
        $sinopsis = str_replace("'"," ",trim($_POST["sinopsis"]));
        $foto = str_replace("'"," ",trim($_POST["fotoFilmFinal"]));
        $duracion = str_replace("'"," ",trim($_POST["duracion"]));
        $guion = str_replace("'"," ",trim($_POST["guion"]));
        $reparto = str_replace("'"," ",trim($_POST["reparto"]));
        $tipo = str_replace("'"," ",trim($_POST["tipo"]));



        //Compruebo que es administrador el que va a realizar la consulta
        session_start();
        $idAdmin = $_POST["idAdmin"];
        //Saber si no es admin:
        $consultaEsAdministradorCrearUsuario =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
        $consultaEsAdministradorCrearUsuario->execute();  

        //Si el nick introducido es de un administrador
        if($consultaEsAdministradorCrearUsuario->fetch()[0]==1||$consultaEsAdministradorCrearUsuario->fetch()[0]=="1"){
                $consultaNuevaPelicula = $pdo->prepare("INSERT INTO `films`(`titulo`, `fechaestreno`, `pais`, `director`, `genero`, `sinopsis`, `tipo`, `fotoportada`, `duracion`, `guion`, `reparto`) VALUES ('$titulo','$fecha','$pais','$director','$genero','$sinopsis','$tipo','$foto','$duracion','$guion','$reparto')");
                $consultaNuevaPelicula->execute();

                echo "PELICULAAGREGADA";
                
        }

}
//FIN CONSULTA PARA CREAR UN FILM NUEVO EN LA BASE DE DATOS (controladorCrearFilmsAdmin)





//CONSULTAS PARA AGREGAR UN FILM UTILIAZANDO LA API (controladorCrearFilmsAdmin.js)

if(isset($_POST["tituloNuevoFilmApi"])){
        $tituloBuscar = $_POST["tituloNuevoFilmApi"];
        $ch = curl_init();

        //curl_setopt($ch,CURLOPT_URL,"https://api-filmaffinity.herokuapp.com/api/pelicula/470268"); //ÂRA BUSCAR POR ID DE FILMAFFINITY
        curl_setopt($ch,CURLOPT_URL,"https://api-filmaffinity.herokuapp.com/api/busqueda/".$tituloBuscar);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $res = curl_exec($ch);

        echo $res;//ESTO ES UN JSON
}

//director:director,reparto:reparto,duracion:duracion,sinopsis:sinopsis,fecha:fecha,genero:genero,guion:guion,titulo:titulo,tipo:tipo,pais:pais,agregadaApi:"true"
if(isset($_POST["idAdmin"])&&isset($_POST["foto"])&&isset($_POST["director"])&&isset($_POST["reparto"])&&isset($_POST["duracion"])&&isset($_POST["sinopsis"])&&isset($_POST["fecha"])&&isset($_POST["genero"])&&isset($_POST["guion"])&&isset($_POST["titulo"])&&isset($_POST["tipo"])&&isset($_POST["pais"])&&isset($_POST["agregadaApi"])){
        $titulo = $_POST["titulo"];
        $fechaestreno = $_POST["fecha"]."-01-01";
        $pais = $_POST["pais"];
        $director = $_POST["director"];
        $genero = $_POST["genero"];
        $guion = $_POST["guion"];
        $sinopsis = $_POST["sinopsis"];
        $fotoportada = $_POST["foto"];
        $duracion = $_POST["duracion"];
        $reparto = $_POST["reparto"];
        $tipo = $_POST["tipo"];
        //echo $titulo;
         //Compruebo que es administrador el que va a realizar la consulta
         session_start();
         $idAdmin = $_POST["idAdmin"];
         //Saber si no es admin:
         $consultaEsAdministradorCrearUsuario =$pdo->prepare("SELECT admin FROM FilmRate.usuarios WHERE idusuario='$idAdmin'");
         $consultaEsAdministradorCrearUsuario->execute();  
 
         //Si el nick introducido es de un administrador
         if($consultaEsAdministradorCrearUsuario->fetch()[0]==1||$consultaEsAdministradorCrearUsuario->fetch()[0]=="1"){

                $consultaNuevaPelicula = $pdo->prepare("INSERT INTO `films`(`titulo`, `fechaestreno`, `pais`, `director`, `genero`, `sinopsis`, `tipo`, `fotoportada`, `duracion`, `guion`, `reparto`) VALUES ('$titulo','$fechaestreno','$pais','$director','$genero','$sinopsis','$tipo','$fotoportada','$duracion','$guion','$reparto')");
                $consultaNuevaPelicula->execute();
                echo "PELICULAAGREGADAAPI";
         }

}



//FIN CONSULTAS PARA AGREGAR UN FILM UTILIAZANDO LA API (controladorCrearFilmsAdmin.js)


//CONSULTA PARA COMPROBAR SI EL FILM QUE SE VA A AGREGAR YA EXISTE EN LA BASE DE DATOS O NO (controladorCrearFilmsAdmin.js)
//{consultaExisteFilm:"true",tituloFilmComprobarExiste:tituloAgregar}
if(isset($_POST["consultaExisteFilm"])&&isset($_POST["tituloFilmComprobarExiste"])){

        $tituloFilmComprobarExiste = $_POST["tituloFilmComprobarExiste"];

        $consultaComprobarExisteFilm = $pdo->prepare("SELECT count(*) FROM FilmRate.films WHERE titulo like '%$tituloFilmComprobarExiste%'");
        $consultaComprobarExisteFilm -> execute();

        while($fila=$consultaComprobarExisteFilm->fetch()){               
                echo($fila[0]);
        }

}
//FIN CONSULTA PARA COMPROBAR SI EL FILM QUE SE VA A AGREGAR YA EXISTE EN LA BASE DE DATOS O NO




//API : https://filmaffinity-unofficial.herokuapp.com //UTILIZAR ESTO CUANDO NECESITE LA API (VER PARTE DE JAVASCRIPT EN controladorIndexAdmin.js AL FINAL)
/*
if(isset($_POST["pruebaEnviar"])){
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,"https://filmaffinity-unofficial.herokuapp.com/api/movie/504889?lang=ES");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $res = curl_exec($ch);

        echo $res;//ESTO ES UN JSON

}
*/


?>